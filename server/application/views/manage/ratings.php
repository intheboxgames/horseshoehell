
<h3>Ratings List</h3>
<br/>
<!-- Can't add ratings, these are set 
<button id="add-element-button" class="uk-button uk-button-primary">Add Rating</button>
-->
<hr/>
<table id="rating_list" class="display">
    <thead>
        <tr>
            <th>Rating</th>
            <th>Points</th>
            <th>Number of Routes</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rating_list as $rating) { ?>
            <tr>
                <th><?php echo $rating->name; ?></th>
                <th><?php echo $rating->points; ?></th>
                <th><?php echo $rating->route_count; ?></th>
                <th><button class="edit-element-button" data-element="<?php echo $rating->id; ?>"><i class="uk-icon-edit"></i></button></th>
            </tr>
        <?php } ?>
    </tbody>
</table>

<!-- Can't add ratings, these are set 
<div id="rating-add-modal" class="uk-modal">
    <div class="uk-modal-dialog">
        <a href="" class="uk-modal-close uk-close"></a>
        <h1>Add New Rating</h1>
        
        <form action='<?php echo base_url("/manage/ratings/add")?>' method='post' accept-charset='utf-8' class="uk-form uk-form-horizontal">
            <div class="uk-form-row">
                <label class="uk-form-label" for="name">Rating Name</label>
                <input type="text" name="name" id="name" class="uk-form-control " placeholder="Rating Name" required autofocus data-uk-tooltip="{delay:1000}" title="The name of this rating">
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="points">Points</label>
                <input type="number" name="points" id="points" class="uk-form-control " placeholder="Points" required autofocus data-uk-tooltip="{delay:1000}" title="Points awarded for climbing a route of this rating">
            </div>
            <hr/>
            <div class="uk-form-row uk-clear-fix">
                <div class="uk-form-controls uk-float-right">                    
                    <div id='add-cancel-button' class="uk-button" >Cancel</div>
                    <button id='add-submit-button' class="uk-button uk-button-primary" type="submit" name="submit" value="Submit">Add Rating</button>
                </div>
            </div>
        </form>
    </div>
</div>
-->

<div id="rating-edit-modal" class="uk-modal">
    <div class="uk-modal-dialog">
        <a href="" class="uk-modal-close uk-close"></a>
        <h1 id="edit-rating-title">Edit Rating - </h1>
        <h3 id="edit-rating-subtitle">This will not affect current scores but will affect any new laps added after the change.</h3>
        
        <form id='edit-form' action='<?php echo base_url("/manage/ratings/edit")?>' method='post' accept-charset='utf-8' class="uk-form uk-form-horizontal">
            <input type="hidden" name="id" id="id" >
            <div class="uk-form-row">
                <label class="uk-form-label" for="name">Rating Name</label>
                <input type="text" name="name" id="name" class="uk-form-control " placeholder="rating Name" required autofocus data-uk-tooltip="{delay:1000}" title="The name of this rating" disabled>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="points">Points</label>
                <input type="number" name="points" id="points" class="uk-form-control " placeholder="Points" required autofocus data-uk-tooltip="{delay:1000}" title="Points awarded for climbing a route of this rating">
            </div>
            <hr/>
            <div class="uk-form-row uk-clear-fix">
                <div class="uk-form-controls uk-float-right">                    
                    <div id='edit-cancel-button' class="uk-button" >Cancel</div>
                    <button id='edit-submit-button' class="uk-button uk-button-primary" type="submit" name="submit" value="Submit">Save Rating</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $.fn.DataTable.ext.order['route-rating'] = function (r) {
        for(var i in ratings) {
            if(ratings[i].name == r) {
                return i;
            }
        }
        return ratings.length;
    };


    var ratings = [];
    <?php foreach($rating_list as $rating) { 
        echo 'ratings['.$rating->id.'] = {id:"'.$rating->id.'", name:"'.$rating->name.'", points:"'.$rating->points.'"};'.PHP_EOL;
    } ?>
    $(document).ready( function () {

        var table = $('#rating_list').DataTable( {
            "columns": [
                { "orderDataType": "route-rating" },
                null,
                null,
                null,
            ]
        });

        $('.edit-element-button').on('click', function(e) {
            var rating = ratings[$(e.currentTarget).data("element")];
            $('#edit-rating-title').html("Edit Rating - " + rating.name);

            $('#edit-form #id').val(rating.id);
            $('#edit-form #name').val(rating.name);
            $('#edit-form #points').val(rating.points).change();

            $.UIkit.modal("#rating-edit-modal").show();
        });
        table.on('draw', function() {
            $('.edit-element-button').on('click', function(e) {
                var rating = ratings[$(e.currentTarget).data("element")];
                $('#edit-rating-title').html("Edit Rating - " + rating.name);

                $('#edit-form #id').val(rating.id);
                $('#edit-form #name').val(rating.name);
                $('#edit-form #points').val(rating.points).change();

                $.UIkit.modal("#rating-edit-modal").show();
            });
        });
        $('#edit-cancel-button').on('click', function(e) {
            $.UIkit.modal("#rating-edit-modal").hide();
        });
        /*$('#add-element-button').on('click', function(e) {
            $.UIkit.modal("#rating-add-modal").show();
        });
        $('#add-cancel-button').on('click', function(e) {
            $.UIkit.modal("#rating-add-modal").hide();
        });*/


    } );
</script>
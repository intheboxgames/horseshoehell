<div class="uk-grid" data-uk-grid-margin="">
    <div class="uk-width-medium-1-3">
        <a href="<?php echo base_url('/manage/ratings'); ?>" class="uk-panel uk-panel-box" style="min-height: 8.5em;">
            <h3 class="uk-panel-title">Ratings</h3>
            Add or update ratings and the scores associated with each rating.
        </a>
    </div>
    <div class="uk-width-medium-1-3">
        <a href="<?php echo base_url('/manage/routes'); ?>" class="uk-panel uk-panel-box" style="min-height: 8.5em;">
            <h3 class="uk-panel-title">Routes</h3>
            Add new routes to the competition or update existing routes.
        </a>
    </div>
    <div class="uk-width-medium-1-3">
        <a href="<?php echo base_url('/manage/walls'); ?>" class="uk-panel uk-panel-box" style="min-height: 8.5em;">
            <h3 class="uk-panel-title">Walls</h3>
            Edit the walls and areas of the ranch.
        </a>
    </div>
    <div class="uk-width-medium-1-3">
        <a href="<?php echo base_url('/manage/teams'); ?>" class="uk-panel uk-panel-box" style="min-height: 8.5em;">
            <h3 class="uk-panel-title">Teams</h3>
            Add or change teams and team members.
        </a>
    </div>
</div>
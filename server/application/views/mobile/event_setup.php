<div id='event_setup-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>Great, thanks for registering. Now let's set up a practice event.</div>
    <form class='form-wrapper'>
        <div class='form-row'>
            <div class='label'>Event Length</div>
            <div class='value'><input type='number' id='length' required min='4' max='48'></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Event Start Date</div>
            <div class='value'><input type='date' id='start_date' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Event Start Time</div>
            <div class='value'><input type='time' id='start_time' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Public Event?</div>
            <div class='value'>
                <select id='is_public'>
                    <option value='yes'>Yes</option>
                    <option value='no'>No</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Team Name</div>
            <div class='value'><input type='text' id='team_name' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Partner's First Name</div>
            <div class='value'><input type='text' id='partner_first_name' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Partner's Last Name</div>
            <div class='value'><input type='text' id='partner_last_name' required></input></div>
        </div>
        <div class='form-row button-group'>
            <div id='event_setup-back' class='button button-medium'>Go Back</div>
            <div id='event_setup-submit' class='button button-medium'>Submit</div>
        </div>
    </form>
</div>

<div id='app_settings-wrapper' class="popup-wrapper view slide-right">
    <form class='form-wrapper'>
        <div class='form-row'>
            <div class='label'>App Theme</div>
            <div class='sub-label'>Changes the appearance of the app. Dark mode is good for using at night.</div>
            <div class='value'>
                <select id='theme'>
                    <option value='light'>Light</option>
                    <option value='dark'>Dark</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Auto Lock Routes</div>
            <div class='sub-label'>If enabled, routes will automatically lock after marking your second lap.</div>
            <div class='value'>
                <select id='lock'>
                    <option value='on'>Enabled</option>
                    <option value='off'>Disabled</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Goals Progress Bar</div>
            <div class='sub-label'>Determines what the Progress Bar at the top of page shows.</div>
            <div class='value'>
                <select id='progress'>
                    <option value='goals'>Goals Progress</option>
                    <option value='score'>Only Score Progress</option>
                    <option value='laps'>Only Laps Progress</option>
                    <option value='text'>Score and Lap Text</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Allow Post Event Changes</div>
            <div class='sub-label'>Determines if scores can be adjusted after the end of a practice event.</div>
            <div class='value'>
                <select id='changeScores'>
                    <option value='yes'>Yes</option>
                    <option value='no'>No</option>
                </select>
            </div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div class='label'>Low Power Mode</div>
            <div class='sub-label'>If enabled, this app uses less battery life but is much slower.</div>
            <div class='value'>
                <select id='power'>
                    <option value='on'>Enabled</option>
                    <option value='off'>Disabled</option>
                </select>
            </div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row button-group'>
            <div id='app_settings-cancel' class='button button-medium'>Cancel</div>
            <div id='app_settings-submit' class='button button-medium'>Save</div>
        </div>
    </form>
</div>

<div id='welcome-wrapper' class="popup-wrapper view slide-right">
    <div class='title'>Welcome to the 24 Hours of Horseshoe Hell Scoring App!</div>
    <div class='subtitle'>Please enter your username and password, name and birthday, or select create account.</div>
    <div class='form-wrapper'>
        <div class='form-row'>
            <div class='label'>Username</div>
            <div class='value'><input id='username'></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Password</div>
            <div class='value'><input type='password' id='password'></input></div>
        </div>
        <div class='form-row divider'></div>
    	<div class='form-row'>
    		<div class='label'>First Name</div>
    		<div class='value'><input id='first_name'></input></div>
    	</div>
    	<div class='form-row'>
    		<div class='label'>Last Name</div>
    		<div class='value'><input id='last_name'></input></div>
    	</div>
        <div class='form-row'>
            <div class='label'>Birthday</div>
            <div class='value'><input type='date' id='birth_date' required></input></div>
        </div>
        <div class='form-row divider'></div>
    	<div class='form-row'>
    		<div id='welcome-submit' class='button button-large'>Submit</div>
    	</div>
        <div class='form-row'>
            <div id='welcome-new' class='button button-large'>New User</div>
        </div>
    </div>
</div>

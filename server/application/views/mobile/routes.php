

<div id='routes-wrapper' class="view fade main-content" style='z-index:1'>
    <div class='scroll-wrapper'>
        <div class='area-wrapper'>
            <div class='area-title'>
                North Forty - Lavender Eye Area
            </div>
            <div class='area-more-button'>
                <i class='fa fa-chevron-right'></i>
            </div>
            <div class='area-routes'></div>
        </div>
        <div class='route-wrapper'>
            <div class='lap lap1'>
                <i class='fa fa-square-o'></i>
            </div>
            <div class='lap lap2'>
                <i class='fa fa-square-o'></i>
            </div>
            <div class='route-info'>
                <div class='name'>Lavender Eye *</div>
                <div class='rating'>5.12a</div>
                <div class='style'>Sport</div>
                <div class='points'>390 points</div>
                <div class='page'>p50 route #38</div>
            </div>
            <div class='route-more-button'>
                <i class='fa fa-chevron-down'></i>
            </div>
            <div class='route-more'>
                <div class='lap-time lap1'>
                    <span class='label'>First lap at: </span>
                    <div class='form-icon'>
                        <i class='fa fa-calendar'></i>
                        <input type='datetime-local'/>
                    </div>
                </div>
                <div class='lap-time lap2'>
                    <span class='label'>Second lap at: </span>
                    <div class='form-icon'>
                        <i class='fa fa-calendar'></i>
                        <input type='datetime-local'/>
                    </div>
                </div>
                <div class='route-lock'>
                    <span class='label'>Route lock OFF</span>
                    <i class='fa fa-toggle-off'></i>
                </div>
            </div>
        </div>
    </div>
</div>
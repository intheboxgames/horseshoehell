<div id='create-user-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>We couldn't find any registered climbers by that name. If you want to register for a practice run you can fill in the fields below to get started. Or you can press back to search again.</div>
    <form class='form-wrapper'>
    	<div class='form-row'>
    		<div class='label'>First Name</div>
    		<div class='value'><input type='text' id='first_name' required></input></div>
    	</div>
    	<div class='form-row'>
    		<div class='label'>Last Name</div>
    		<div class='value'><input type='text' id='last_name' required></input></div>
    	</div>
        <div class='form-row'>
            <div class='label'>Birthday</div>
            <div class='value'><input type='date' id='birth_date' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Category</div>
            <div class='value'>
                <select id='category' required>
                    <option value='rec'>Recreational (5.9- and below)</option>
                    <option value='int' selected>Intermediate (5.10d and below)</option>
                    <option value='adv'>Advanced (5.12a and below)</option>
                    <option value='eli'>Elite (All routes)</option>
                </select>
            </div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div class='label'>Username</div>
            <div class='value'><input type='text' id='username' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Password</div>
            <div class='sub-label'>A password is not required but will keep others from logging into your account</div>
            <div class='value'><input type='password' id='password'></input></div>
        </div>
    	<div class='form-row button-group'>
            <div id='create-user-back' class='button button-medium'>Go Back</div>
    		<div id='create-user-submit' class='button button-medium'>Submit</div>
    	</div>
    </form>
</div>

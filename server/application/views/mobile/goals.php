
<div id='goals-wrapper' class="view fade main-content">
    <form class='form-wrapper'>
        <div class='form-row button-group'>
            <div class='button button-medium tab-button selected' data-tab="tab-personal-goals">Personal</div>
            <div class='button button-medium tab-button' data-tab="tab-team-goals">Team</div>
        </div>
        <div id="tab-personal-goals" class='form-tab selected' style='margin:0 .45em 0 .5em'>
            <table>
                <tr>
                    <th style="width: 45%;border-bottom: 1px solid #999;padding: .5em 0;"></th>
                    <th style="width: 15%;border-bottom: 1px solid #999;padding: .5em 0;">Goal</th>
                    <th style="width: 15%;border-bottom: 1px solid #999;">Current</th>
                    <th style="width: 15%;border-bottom: 1px solid #999;">Progress</th>
                </tr>
            </table>
            <div class='form-row'>
                <div id='need-button' class='button button-large' style='font-size:1em'>What do I need to do to meet these goals?</div>
            </div>
        </div>
        <div id="tab-team-goals" class='form-tab' style='margin:0 .45em 0 .5em'>
            <div style='margin-top:3em; width: 80%; display:inline-block;'>Team goals are currently not supported, please look for this feature soon.</div>
        </div>
    </form>
</div>
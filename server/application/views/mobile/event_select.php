<div id='event_select-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>You have several events to choose from. Please pick the event you want to view.<br/>Don't see an event you want? Create a new one at the bottom of the page.</div>
    <form class='form-wrapper'>
        <div id='event-current-official' class='event-select-container'>
            <div class='event-select-header'>Current Official Events</div>
        </div>
        <div id='event-current-practice' class='event-select-container'>
            <div class='event-select-header'>Current Practice Events</div>
        </div>
        <div id='event-future-official' class='event-select-container'>
            <div class='event-select-header'>Official Events Starting Soon</div>
        </div>
        <div id='event-future-practice' class='event-select-container'>
            <div class='event-select-header'>Practice Events Starting Soon</div>
        </div>
        <div id='event-past-official' class='event-select-container'>
            <div class='event-select-header'>Past Official Events</div>
        </div>
        <div id='event-past-practice' class='event-select-container'>
            <div class='event-select-header'>Past Practice Events</div>
        </div>
    	<div class='event-summary'>
    		<div class='event-name'>First Name Last Name</div>
            <div class='event-date'>10-30-1990</div>
    	</div>
        <div class='form-row'>
            <div id='event_select-submit' class='button button-large'>Select Event</div>
        </div>
        <div class='form-row'>
            <div id='event_select-new' class='button button-large'>New Event</div>
        </div>
        <div class='form-row'>
            <div id='event_select-logout' class='button button-large'>Logout</div>
        </div>
    </form>
</div>

<div id='settings-wrapper' class="view fade main-content">
    <form class='form-wrapper'>
        <div class='form-row'>
            <div id='settings-app' class='button button-large'>App Settings</div>
        </div>
        <div class='form-row'>
            <div id='settings-goals' class='button button-large'>Event Goals</div>
        </div>
        <div class='form-row'>
            <div id='settings-routes' class='button button-large'>Route Filter</div>
        </div>
        <div class='form-row'>
            <div id='settings-event' class='button button-large'>Event Settings</div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div id='settings-export' class='button button-large'>Export Data</div>
        </div>
        <div class='form-row'>
            <div id='settings-import' class='button button-large'>Import Data</div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div id='settings-help' class='button button-large'>Help</div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div id='settings-select' class='button button-large'>Event Select</div>
        </div>
        <div class='form-row'>
            <div id='settings-logout' class='button button-large'>Logout</div>
        </div>
        <div style='height: 12em'></div>
    </form>
</div>

<div id='filter_settings-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>You can select as many, or none, of the following filters so only the routes you want on your route list will be there.</div>
    <form class='form-wrapper'>
        <div class='form-row'>
            <div class='label'>Areas</div>
            <div class='value'>
                <div class='multiselect-button-all'>Select All</div>
                <div class='multiselect-button-none'>Remove All</div>
                <div class='multiselect-container' id='area-select'>
                    <div class='multiselect-group'>
                        <div class='multiselect-group-header'>The West Side</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='1'>Crackhouse Alley</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='2'>Confederate Cracks</div>
                    </div>
                    <div class='multiselect-group'>
                        <div class='multiselect-group-header'>North West Crags</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='3'>Ren and Stimpy</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='4'>Prophecy Wall</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='5'>Titanic Boulder</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='6'>Doomsday</div>
                    </div>
                    <div class='multiselect-group'>
                        <div class='multiselect-group-header'>The North Forty</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='7'>Cooridor Area</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='8'>Lavender Eye Area</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='9'>Circus Wall</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='10'>Crimp Scampi Area</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='11'>Groovy Area</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='12'>Kindergarten Boulder</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='13'>The Land Beyond</div>
                    </div>
                    <div class='multiselect-group'>
                        <div class='multiselect-group-header'>The East Side</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='14'>The Goat Cave</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='15'>Mullet Buttress</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='16'>Land of the Lost</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='17'>Middle East</div>
                    </div>
                    <div class='multiselect-group'>
                        <div class='multiselect-group-header'>Far East Crags</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='18'>Magoo Rock</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='19'>Roman Wall</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='20'>Cliffs of Insanity</div>
                        <div class='multiselect-input'><input type='checkbox' name='area-select' value='21'>The Far East</div>
                    </div>
                </div>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Lowest Rating</div>
            <div class='value'>
                <select id='lowest-rating'>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Highest Rating</div>
            <div class='value'>
                <select id='highest-rating'>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Route Style</div>
            <div class='value'>
                <select id='route-style'>
                    <option value='both' selected>Sport and Trad</option>
                    <option value='sport'>Sport Only</option>
                    <option value='trad'>Trad Only</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Route Height</div>
            <div class='value'>
                <select id='route-height'>
                    <option value='all' selected>All</option>
                    <option value='tall'>Tall Routes Only</option>
                    <option value='short'>Short Routes Only</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Routes in Filter: </div>
            <div class='value' id='route-count'></div>
        </div>
        <div class='form-row button-group'>
            <div id='filter_settings-cancel' class='button button-medium'>Cancel</div>
            <div id='filter_settings-submit' class='button button-medium'>Save</div>
        </div>
    </form>
</div>

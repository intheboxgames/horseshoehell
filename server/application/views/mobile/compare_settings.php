<div id='compare_settings-wrapper' class="view fade main-content">
    <div class='title'></div>
    <div class='subtitle'>Please be aware that all climber data and ranking is un-official and may change drastically from what you see here.</div>
    <form class='form-wrapper'>
        <div class='form-row'>
            Compare scores with:
        </div>
        <div class='form-row'>
            <div class='label'>Climber</div>
            <div class='value'><input id='climber_search' style='border: 2px solid #3776fb;'></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Team</div>
            <div class='value'><input id='team_search' style='border: 2px solid #3776fb;'></input></div>
        </div>
        <div class='form-row'>
            <div id='compare_settings-compare' class='button button-large'>Compare Scores</div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div id='compare_settings-rankings' class='button button-large'>Event Rankings</div>
        </div>
        <div class='form-row'>
            <div id='compare_settings-routes' class='button button-large'>Route Statistics</div>
        </div>
        <div class='form-row'>
            <div id='compare_settings-event' class='button button-large'>Event Statistics</div>
        </div>
        <div style='height: 12em'></div>
    </form>
</div>

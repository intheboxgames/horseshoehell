
<div id='progress-wrapper' class="view fade main-content">
    <form class='form-wrapper'>
        <div class='form-row'>
            <div class='label'>Graph Type</div>
            <div class='value'>
                <select id='graph-type' style='border: 2px solid #3776fb; margin-bottom:1em;'>
                    <option value='total_score' selected>Total Score Over Time</option>
                    <option value='total_routes'>Total Laps Over Time</option>
                    <option value='total_height'>Total Height Over Time</option>
                    <option value='total_rating'>Average Rating Over Time</option>
                    <option value='total_trad'>Total Trad Laps Over Time</option>
                    <option value='total_points_per_route'>Average Point Per Route Over Time</option>
                    <option value='score'>Score Per Hour</option>
                    <option value='routes'>Laps Per Hour</option>
                    <option value='height'>Height Per Hour</option>
                    <option value='rating'>Average Rating Per Hour</option>
                    <option value='trad'>Trad Laps Per Hour</option>
                    <option value='points_per_route'>Average Point Per Route Pre Hour</option>
                </select>
            </div>
        </div>
    </form>
    <div id="score-container" style="min-width: 310px; height: 300px; margin: 0 auto 1em"></div>
    <div style="font-size:1.1em;font-weight:bold">Score Details</div>
    <table id="route_list" class="display">
        <thead>
            <tr>
                <th>Lap</th>
                <th>Name</th>
                <th>Rating</th>
                <th>Score</th>
                <th>Total Score</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
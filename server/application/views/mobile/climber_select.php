<div id='climber_select-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>It looks like there are several climbers with that name. Please select your account from the list below.</div>
    <form class='form-wrapper'>
    	<div class='form-row climber-summary'>
    		<div class='climber-name'>First Name Last Name</div>
            <div class='climber-username'></div>
            <div class='climber-date'>Created: 10-30-1990</div>
    	</div>
        <div class='form-row button-group'>
            <div id='climber_select-back' class='button button-medium'>Go Back</div>
            <div id='climber_select-submit' class='button button-medium'>Submit</div>
        </div>
    </form>
</div>


<div class='event-header view slide-top' style='z-index: 50'>
    <div class='header-column'>
        <div class='climber-name'>Climber Name</div>
        <div class='climber-category'>Div</div>
        <div class='team-name'>Team Name</div>
    </div>
    <div class='header-column'>
        <div class='countdown'>14:35:25</div>
    </div>
    <div class='goal-progress'>
        <div class='goal-progress-bar' style="width:43%"></div>
        <div class='goal-progress-text'>Goal Progress: 43%</div>
    </div>
</div>
<div class='event-footer view slide-bottom' style='z-index: 50'>
    <div class='footer-column' id='routes-button'>
        <i class='icon fa fa-check'></i>
        <div class='title'>Routes</div>
    </div>
    <div class='footer-column' id='progress-button'>
        <i class='icon fa fa-line-chart'></i>
        <div class='title'>Progress</div>
    </div>
    <div class='footer-column' id='goals-button'>
        <i class='icon fa fa-trophy'></i>
        <div class='title'>Goals</div>
    </div>
    <div class='footer-column' id='compare-button'>
        <i class='icon fa fa-users'></i>
        <div class='title'>Compare</div>
    </div>
    <div class='footer-column' id='settings-button'>
        <i class='icon fa fa-cogs'></i>
        <div class='title'>Settings</div>
    </div>
</div>
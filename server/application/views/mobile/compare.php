

<div class='event-header view slide-top'>
    <div class='header-column'>
        <div class='climber-name'>Climber Name</div>
        <div class='climber-category'>Div</div>
        <div class='team-name'>Team Name</div>
    </div>
    <div class='header-column'>
        <div class='countdown'>01:35:25</div>
    </div>
    <div class='goal-progress'>
        <div class='goal-progress-bar' style="width:91%"></div>
        <div class='goal-progress-text'>Goal Progress: 91%</div>
    </div>
</div>
<div id='progress-wrapper' class="view fade main-content">
    <form class='form-wrapper'>
        <h3>Comparing scores with Luke Stuff</h3>
        <div class='form-row'>
            <div class='label'>Graph Type</div>
            <div class='value'>
                <select id='category' style='border: 1px solid blue;'>
                    <option value='0'>5.7</option>
                    <option value='1' selected>Total Score Over Time</option>
                    <option value='2'>5.9</option>
                    <option value='3'>5.10</option>
                </select>
            </div>
        </div>
    </form>
    <div id="score-container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
</div>
<div class='event-footer view slide-bottom'>
    <div class='footer-column'>
        <i class='icon fa fa-check'></i>
        <div class='title'>Routes</div>
    </div>
    <div class='footer-column'>
        <i class='icon fa fa-line-chart'></i>
        <div class='title'>Progress</div>
    </div>
    <div class='footer-column'>
        <i class='icon fa fa-trophy'></i>
        <div class='title'>Goals</div>
    </div>
    <div class='footer-column active'>
        <i class='icon fa fa-users'></i>
        <div class='title'>Compare</div>
    </div>
    <div class='footer-column'>
        <i class='icon fa fa-cogs'></i>
        <div class='title'>Settings</div>
    </div>
</div>

<script>

$(document).ready(function() {
    show_chart('total_score');
});
</script>
<div id='register-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>We couldn't find any registered climbers by that name. If you want to register for a practice run you can fill in the fields below to get started. Or you can press back to search again.</div>
    <form class='form-wrapper'>
    	<div class='form-row'>
    		<div class='label'>First Name</div>
    		<div class='value'><input type='text' id='first_name' required></input></div>
    	</div>
    	<div class='form-row'>
    		<div class='label'>Last Name</div>
    		<div class='value'><input type='text' id='last_name' required></input></div>
    	</div>
        <div class='form-row'>
            <div class='label'>Birthday</div>
            <div class='value'><input type='date' id='birth_date' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Category</div>
            <div class='value'>
                <select id='category' required>
                    <option value='rec'>Recreational (5.9- and below)</option>
                    <option value='int' selected>Intermediate (5.10d and below)</option>
                    <option value='adv'>Advanced (5.12a and below)</option>
                    <option value='eli'>Elite (All routes)</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Event Type</div>
            <div class='value'>
                <select id='event_type' required>
                    <option value='24' selected>24 Hours</option>
                    <option value='12'>12 Hours</option>
                </select>
            </div>
        </div>
        <div class='form-row'>
            <div class='label'>Event Start Date</div>
            <div class='value'><input type='date' id='start_date' required></input></div>
        </div>
        <div class='form-row'>
            <div class='label'>Event Start Time</div>
            <div class='value'><input type='time' id='start_time' required></input></div>
        </div>
    	<div class='form-row'>
    		<div id='register-submit' class='button button-large'>Submit</div>
    	</div>
        <div class='form-row'>
            <div id='register-back' class='button button-large'>Go Back</div>
        </div>
    </form>
</div>

<div id='goals_settings-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>Here you can set your goals for this event. Goals will be tracked with a progress bar to make it easy to see how close you are. You can enter numbers in all or none of the categories below. No one else can see your goals.</div>
    <form class='form-wrapper'>
        <div class='form-row button-group'>
            <div class='button button-medium tab-button selected' data-tab="tab-personal-goals">Personal</div>
            <div class='button button-medium tab-button' data-tab="tab-team-goals">Team</div>
        </div>
            <div id="tab-personal-goals" class='form-tab selected'>
                <div class='form-row horizontal'>
                    <div class='label'>Total Score</div>
                    <div class='value'><input type='number' id='personal-score' min="0"></input></div>
                </div>
                <div class='form-row horizontal'>
                    <div class='label'>Total Laps</div>
                    <div class='value'><input type='number' id='personal-laps' min="0"></input></div>
                </div>
                <div class='form-row horizontal'>
                    <div class='label'>Total Height</div>
                    <div class='value'><input type='number' id='personal-height' min="0"></input></div>
                </div>
                <div class='form-row horizontal'>
                    <div class='label'>Average Rating</div>
                    <div class='value'>
                        <select id='personal-rating'>
                            <option value=''>None</option>
                        </select>
                    </div>
                </div>
                <div class='form-row horizontal'>
                    <div class='label'>Average Laps Per Hour</div>
                    <div class='value'><input type='number' id='personal-laps-per-hour' min="0"></input></div>
                </div>
                <div class='form-row horizontal'>
                    <div class='label'>Average Score Per Hour</div>
                    <div class='value'><input type='number' id='personal-score-per-hour' min="0"></input></div>
                </div>
            </div>
        <div id="tab-team-goals" class='form-tab'>
            <div class='form-row horizontal'>
                <div class='label'>Total Score</div>
                <div class='value'><input type='number' id='team-score' min="0"></input></div>
            </div>
            <div class='form-row horizontal'>
                <div class='label'>Total Laps</div>
                <div class='value'><input type='number' id='team-laps' min="0"></input></div>
            </div>
            <div class='form-row horizontal'>
                <div class='label'>Total Height</div>
                <div class='value'><input type='number' id='team-height' min="0"></input></div>
            </div>
            <div class='form-row horizontal'>
                <div class='label'>Average Rating</div>
                <div class='value'>
                    <select id='team-rating'>
                        <option value=''>None</option>
                    </select>
                </div>
            </div>
            <div class='form-row horizontal'>
                <div class='label'>Average Laps Per Hour</div>
                <div class='value'><input type='number' id='team-laps-per-hour' min="0"></input></div>
            </div>
            <div class='form-row horizontal'>
                <div class='label'>Average Score Per Hour</div>
                <div class='value'><input type='number' id='team-score-per-hour' min="0"></input></div>
            </div>
        </div>
        <div class='form-row button-group'>
            <div id="goals_settings-cancel" class='button button-medium'>Cancel</div>
            <div id="goals_settings-submit" class='button button-medium'>Save</div>
        </div>
    </form>
</div>

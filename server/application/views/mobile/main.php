<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
		<meta title="Horseshoe Hell Scoring App">
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
	    <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">

		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="<?php echo base_url('/static/js/jquery.dataTables.min.js') ?>"></script>
		<script src="<?php echo base_url('/static/js/select2.js') ?>"></script>
		<script src="<?php echo base_url('/static/js/mobile/base.js') ?>"></script>
		<script src="<?php echo base_url('/static/js/mobile/data.js') ?>"></script>
		<script src="<?php echo base_url('/static/js/mobile/main.js') ?>"></script>
		<script src="<?php echo base_url('/static/js/highcharts.js') ?>"></script>

		<link rel="stylesheet" href="<?php echo base_url('/static/css/select2.css') ?>"/>
		<link rel="stylesheet" href="<?php echo base_url('/static/css/mobile.css') ?>"/>
		<link rel="stylesheet" href="<?php echo base_url('/static/css/jquery.dataTables.min.css') ?>"/>
		<link rel="stylesheet" href="<?php echo base_url('/static/css/font-awesome.min.css') ?>"/>

	</head>
	<body>
		<div id="background-img"></div>
		<div id="message-header">
			<div class="message"></div>
		</div>
		<div id="loading-overlay"><i class="fa fa-spinner fa-spin"></i></div>
		<div id="overlay"></div>
		<div id="container" class="">
			<div id='content-wrapper'>
			</div>
		</div> <!-- End of 'container'-->
	</body>
</html>
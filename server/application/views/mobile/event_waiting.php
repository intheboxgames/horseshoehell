<div id='event_waiting-wrapper' class="popup-wrapper view slide-right">
    <div class='title'></div>
    <div class='subtitle'>This event hasn't begun yet. You can prepare for the event with the settings below.</div>
    <form class='form-wrapper'>
        <div class='form-row countdown-wrapper'>
            <div class='subtitle'>Event begins in:</div>
            <div class='countdown'></div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row team-info'>
            <div class='team'><div class='label'>Team</div><div class='value'></div></div>
            <div class='partner'><div class='label'>Partner</div><div class='value'></div></div>
            <div class='category'><div class='label'>Category</div><div class='value'></div></div>
        </div>
        <div class='form-row event-info'>
            <div class='length' style='margin-top:1em'><div class='label'>Event Length</div><div class='value'></div></div>
            <div class='climbers'><div class='label'>Event Participants</div><div class='value'></div></div>
        </div>
        <div class='form-row divider'></div>
        <div class='form-row'>
            <div id='event_waiting-goals' class='button button-large'>Event Goals</div>
        </div>
        <div class='form-row'>
            <div id='event_waiting-routes' class='button button-large'>Route Filter</div>
        </div>
        <div class='form-row'>
            <div id='event_waiting-settings' class='button button-large'>Event Settings</div>
        </div>
        <div class='form-row button-group'>
            <div id='event_waiting-back' class='button button-medium'>Back</div>
            <div id='event_waiting-logout' class='button button-medium'>Logout</div>
        </div>
    </form>
</div>

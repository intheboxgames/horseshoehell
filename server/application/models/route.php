<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."models/base_model.php";

/*
 *  Model Class for Routes
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff   Creation
 */

class Route extends Base_model {

    public function __construct(){
        $this->table_name = 'route';
        parent::__construct();
    }

    /**
     * Gets the number of routes available at each wall
     */
    public function get_route_count_by_wall() {
    	$sql = 'SELECT wall, COUNT(*) as routes from '.$this->table_name.' GROUP BY wall';
        $result = $this->db->query($sql);
        return $result->result();
    }

    /**
     * Gets the number of routes available at each rating
     */
    public function get_route_count_by_rating() {
        $sql = 'SELECT rating, COUNT(*) as routes from '.$this->table_name.' GROUP BY rating';
        $result = $this->db->query($sql);
        return $result->result();
    }

    /**
     * Gets all routes but filters a data to only what needs to be sent to the client (this is to fit with FixedPin's contract so route data doesn't get sent)
     */
    public function get_all_client() {
        $sql = 'SELECT id, number, name, wall, rating, trad, height, page from '.$this->table_name;
        $result = $this->db->query($sql);
        return $result->result();
    }
}
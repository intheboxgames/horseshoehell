<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."models/base_model.php";

/*
 *  Model Class for the Team -> Climber relationship
 *
 *  Change Log:
 *      
 *      2014-10-14  Luke Stuff   Creation
 */

class Team_Climber extends Base_model {

    public function __construct() {
        $this->table_name = 'team_climber';
        parent::__construct();
    }
}
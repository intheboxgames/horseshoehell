<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."models/base_model.php";

/*
 *  Model Class for sends
 *
 *  Change Log:
 *      
 *      2014-10-15  Luke Stuff   Creation
 */

class Send extends Base_model {

    public function __construct() {
        $this->table_name = 'send';
        parent::__construct();
    }

    /**
     * Gets a list of sends for the event climber
     */
    public function get_for_event_climber($event_climber_id) {
    	$sql = 'SELECT send.*, route.name, route.rating, route.trad, route.height, route.wall FROM send JOIN route ON send.route_id = route.id WHERE send.deleted IS NULL AND send.event_climber_id = ' . $this->db->escape($event_climber_id) . ' ORDER BY send_time ASC';
        $result = $this->db->query($sql);
        return $result->result();
    }

    /**
     * Delets all of user's routes
     */
    public function delete_for_event_climber($event_climber_id) {

        // do the delete as a transation to make sure all parts go through before commiting them
        $this->db->trans_begin();

        $sql = 'DELETE FROM send WHERE send.deleted IS NULL AND send.event_climber_id = ' . $this->db->escape($event_climber_id);
        $this->db->query($sql);

        if ($this->db->trans_status() === false)
        {
            return false;
        }

        log_message('debug','Successfully deleted from '.$this->table_name);

        $this->db->trans_commit(); // commit the deletion to the database

        return true;
    }

    /**
     * Gets the number of laps on the specified route for the specified event climber
     */
    public function get_count_for_event_climber_route($event_climber_id, $route_id) {
    	$sql = 'SELECT count(*) as count FROM send WHERE deleted IS NULL AND event_climber_id = ' . $this->db->escape($event_climber_id) . ' AND route_id = ' . $this->db->escape($route_id);
        $result = $this->db->query($sql);
        return $result->row()->count;
    }

    /**
     * gets the specific send object for the specified event climber and route
     */
    public function get_for_event_climber_route($event_climber_id, $route_id) {
    	$sql = 'SELECT * FROM send WHERE deleted IS NULL AND event_climber_id = ' . $this->db->escape($event_climber_id) . ' AND route_id = ' . $this->db->escape($route_id);
        $result = $this->db->query($sql);
        return $result->result();
    }


    /**
     * gets the specific send object for the specified event climber, route, and lap
     */
    public function get_specific($event_climber_id, $route_id, $pink_point, $lap) {
        $sql = 'SELECT * FROM send WHERE deleted IS NULL AND event_climber_id = ' . $this->db->escape($event_climber_id) . ' AND route_id = ' . $this->db->escape($route_id) . ' AND pink_point = ' . $this->db->escape($pink_point) . ' AND lap = ' . $this->db->escape($lap);
        $result = $this->db->query($sql);
        return $result ? $result->row() : false;
    }

    /*public function delete($id) {
        $this->db->query('DELETE FROM send WHERE id='.$this->db->escape($id));
        return $this->db->affected_rows() > 0;
    }*/
}
<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 *  Base Model Class
 *  Handles basic CRUD operations on all database tables
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff      Creation
 *
 */
class Base_model extends CI_Model {
	
	public $table_name = null; // the name of the current table

	public function __construct() {
		parent::__construct();
		if($this->table_name == NULL) {
			log_message('error', "Table name not set for class ".get_class($this));
		}
        $this->load->database(); // load the database
	}

    /**
     * Get all database entries in this table
     *
     * @return  array   All table entries
     */
	public function get_all() {
        $result = $this->db->query('SELECT * FROM '.$this->table_name);
        return $result ? $result->result() : false;
    }

    /**
     * Get the database entry with the specified id
     *
     * @param int $id       The id of the requested entry
     *
     * @return object   The requested entry from this table
     */
    public function get($id) {
        $result = $this->db->query('SELECT * FROM '.$this->table_name.' WHERE id = ?', $id);
        return $result ? $result->row() : false;
    }

    /**
     * Get all database entries in the provided array of ids
     *
     * @param array $ids    An array of ids to search for
     *
     * @return array   an array of all the requested entries
     */
    public function get_in($ids) {
    	if(!is_array($ids)) {
    		$ids = array($ids);
    	}
        $this->db->where_in('id', $ids);
    	$result = $this->db->get($this->table_name);
    	return $result ? $result->result() : false;
    }

    /**
     * All database entries that meet the specified criteria
     *
     * @param array $criteria   An associative array of all the specified criteria in the form -> array( '%param% %comparison% ?' => '%value%' ), ie  array('id IN (?)' => '1,5,7', 'level >= ?', '5')
     * @param string $op        The combining operand to use for multiple criteria (AND, OR)
     *
     * @return array   an array of all entried that meet the provided criteria
     */
    public function get_with_criteria($criteria, $op = 'AND') {
    	if(!is_array($criteria) || count($criteria) == 0) {
        	log_message('error','Criteria provided must be an array with at least one value. Is it currently - '.$criteria);
        	return false;
    	}

    	$params = array();
    	$values = array();
        // create the criteria and value lists
    	foreach($criteria as $param => $value){
    		if($param){
                // don't need this at the moment so I removed it
    			/*$operator_check = strpos($param, 'AND');
    			if($operator_check != 0 && $operator_check != 1){
    				$operator_check = strpos($param, 'OR');
    				if($operator_check != 0 && $operator_check != 1){
    					// No starting operator so add the default
    					$param = $default_op.' '.$param;
    				}
    			}*/
    			$params[] = $param;
    			if($value){
    				$values[] = $value;
    			}
    		}
    	}
        // create a where statment from the criteria
    	$param_list = implode(' '.$op.' ', $params);
    	$sql = 'SELECT * FROM '.$this->table_name.' WHERE '.$param_list;
    	log_message('debug', 'Executing get_with_criteria - '.sql); // logging for debug puposes

    	$result = $this->db->query($sql, $values);
    	return $result ? $result->result() : false;
    }

    /**
     * Creates an entry in the current table with the specified data
     *
     * @param object,array $data    An object or associative array containing the fields of a single new object to create in the current table
     *
     * @return int   the id of the newly created entry or null if the creation failed
     */
    public function create($data) {
        global $user;
        // make sure that the data does not contain an id yet
        if(isset($data->id)){
            unset($data->id);
        }
        // filter the data to send only the fields that are actually in the table
    	$data = $this->_filter_data($this->table_name, $data);


        // a create date to the fields
    	if(!isset($data['created'])){
    		$data['created'] = date('Y-m-d H:i:s');
    	}

        // insert the new entry and get the resulting id
        $this->db->insert($this->table_name, $data);
        $id = $this->db->insert_id();

        if($id){ 
        // if successful and autherized user is the caller, create and action entry to track the database change
        	log_message('debug','Successfully created '.$this->table_name);
            $data['id'] = $id;

            if($user) {
                $action = [
                    'account_id' => $user->id,
                    'status' => 'success',
                    'status_message' => '',
                    'table_effected' => $this->table_name,
                    'action_type' => 'create',
                    'entry_id' => $id,
                    'pre_data' => '',
                    'post_data' => json_encode($data),
                ];
                $this->db->insert('admin_action_log', $action);
            }
        }
        else {
            // if the creation failed, log the failure
        	log_message('warning','Failed to create in '.$this->table_name.' with Error: '.$this->db->_error_message());

            if($user) {
                $action = [
                    'account_id' => $user->id,
                    'status' => 'failed',
                    'status_message' => $this->db->_error_message(),
                    'table_effected' => $this->table_name,
                    'action_type' => 'create',
                    'entry_id' => '-1',
                    'pre_data' => '',
                    'post_data' => json_encode($data),
                ];
                $this->db->insert('admin_action_log', $action);
            }
        }
        return $id;
    }

    /**
     * Updates an entry in the current table with the specified data
     *
     * @param object,array $data    An object or associative array containing the new fields of the entry to update, must contain an id
     *
     * @return bool   True if the update was successful, false otherwise
     */
    public function update($data)
    {
        global $user;
        $data = $this->_filter_data($this->table_name, $data);

        if(!isset($data['id']) || empty($data['id']) || !is_numeric($data['id'])){
        	// No id so we can't update, throw an error
        	log_message('error','Failed to update entry for table '.$this->table_name.' because no ID value was present - '.var_export($data, TRUE));
        	return false;
        }

        // update the object
    	$id = $data['id'];
    	unset($data['id']);
        $pre_data = $this->get($id);
    	$this->db->update($this->table_name, $data, array('id' => $id));

        // log the results of the update
        if($this->db->_error_number() && $this->db->affected_rows() == 0){
        	log_message('error','Failed to update on \''.$this->table_name.'\' with Error: '.$this->db->_error_message());

            if($user) {
                $action = [
                    'account_id' => $user->id,
                    'status' => 'failed',
                    'status_message' => $this->db->_error_message(),
                    'table_effected' => $this->table_name,
                    'action_type' => 'update',
                    'entry_id' => $id,
                    'pre_data' => json_encode($pre_data),
                    'post_data' => json_encode($data),
                ];
                $this->db->insert('admin_action_log', $action);
            }
        	return false;
        }

        log_message('debug','Successfully updated on \''.$this->table_name.'\'');

        if($user) {
            $action = [
                'account_id' => $user->id,
                'status' => 'success',
                'status_message' => '',
                'table_effected' => $this->table_name,
                'action_type' => 'update',
                'entry_id' => $id,
                'pre_data' => json_encode($pre_data),
                'post_data' => json_encode($data),
            ];
            $this->db->insert('admin_action_log', $action);
        }
        return true;
    }

    /**
     * Removed an entry from the current table
     *
     * @param int $id    The id of the object to delete
     *
     * @return bool     true if the delete succeeded, false otherwise
     */
    public function delete($id)
    {
        global $user;
        // make sure the id is valid
        if(!$id || empty($id) || !is_numeric($id)){
        	log_message('error','Failed to update entry for table '.$this->table_name.' because no ID value was present - '.var_export($id, TRUE));
            return false;
        }

        $pre_data = $this->get($id); // get the object
        $this->db->query('DELETE FROM '.$this->table_name.' WHERE id='.$this->db->escape($id)); // and delete it from the database

        if($this->db->_error_number() && $this->db->affected_rows() == 0)
        {
        	log_message('error','Failed to delete '.$id.' from '.$this->table_name.' with Error: '.$this->db->_error_message());

            // log the transaction
            if($user) {
                $action = [
                    'account_id' => $user->id,
                    'status' => 'failed',
                    'status_message' => $this->db->_error_message(),
                    'table_effected' => $this->table_name,
                    'action_type' => 'update',
                    'entry_id' => $id,
                    'pre_data' => json_encode($pre_data),
                    'post_data' => '',
                ];
                $this->db->insert('admin_action_log', $action);
            }
            return false;
        }

        log_message('debug','Successfully deleted from '.$this->table_name);

        // log the deletion
        if($user) {
            $action = [
                'account_id' => $user->id,
                'status' => 'delete',
                'status_message' => '',
                'table_effected' => $this->table_name,
                'action_type' => 'update',
                'entry_id' => $id,
                'pre_data' => json_encode($pre_data),
                'post_data' => '',
            ];
            $this->db->insert('admin_action_log', $action);
        }
        return true;
    }

    /**
     * Filters data to match the fields in the database
     *
     * @param string $table             The name of the table this data is for
     * @param object,array $data        The data to filter
     *
     * @return array   an associative array of the filtered data
     */
    protected function _filter_data($table, $data){
    	
        $filtered_data = array();
        // get the available fields for the table
        $columns = $this->db->list_fields($table);

        // if the provided data is an object, convert it to an array
        if(is_object($data)){
        	$data_array = array();
        	foreach($data as $key => $value){
        		$data_array[$key] = $value;
        	}
        	$data = $data_array;
        }
        if(!is_array($data)){
        	return false;
        }
        // copy all fields that exist in the database
        foreach ($columns as $column){
            if (array_key_exists($column, $data)){
                $filtered_data[$column] = $data[$column];
            }
        }

        return $filtered_data;
    }
}
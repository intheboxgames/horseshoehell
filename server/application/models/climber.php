<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."models/base_model.php";

/*
 *  Model Class for Climbers
 *
 *  Change Log:
 *      
 *      2014-10-14  Luke Stuff   Creation
 */

class Climber extends Base_model {

    public function __construct() {
        $this->table_name = 'climber';
        parent::__construct();
    }

    /**
     * Get a list of climbers whose name matches the search term
     *
     * @param string $term    The first name, last name, or combined name or the climber to search for
     *
     * @return array   an array of all climbers that match the search term
     */
    public function search($term) {
    	$term = preg_replace('/[^A-Za-z0-9\- ]/', '', $term);
    	$sql = 'SELECT climber.*, account.username, account.last_login, account.role FROM '.$this->table_name.' JOIN account ON climber.account_id = account.id WHERE climber.first_name LIKE "%' . $term . '%" OR climber.last_name LIKE "%' . $term . '%" OR CONCAT(climber.first_name, " ", climber.last_name) LIKE "%' . $term . '%"';
        $result = $this->db->query($sql);
        return $result ? $result->result() : array();
    }

    /**
     * Get the climber associated with an account
     *
     * @param string $account_id    The id of the account
     *
     * @return climber   the climber associated with the account or null
     */
    public function get_for_account($account_id) {
        $sql = 'SELECT climber.*, account.username, account.last_login, account.role FROM '.$this->table_name.' JOIN account ON climber.account_id = account.id WHERE account_id = ' . $this->db->escape($account_id);
        $result = $this->db->query($sql);
        return $result ? $result->row() : null;
    }
}
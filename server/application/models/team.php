<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."models/base_model.php";

/*
 *  Model Class for Teams
 *
 *  Change Log:
 *      
 *      2014-10-14  Luke Stuff   Creation
 */

class Team extends Base_model {

    public function __construct() {
        $this->table_name = 'team';
        parent::__construct();
    }

    /**
     * Get a list of team for the specified event
     *
     * @param string $event_id    The id of the event
     *
     * @return array   an array of all teams in the specified event
     */
    public function get_for_event($event_id) {
    	$sql = 'SELECT * FROM '.$this->table_name.' WHERE event_id = ' . $this->db->escape($event_id);
        $result = $this->db->query($sql);
        return $result->result();
    }

    /**
     * Gets the team for the climber and event provided
     *
     * @param string $climber_id    The id of the climber
     * @param string $event_id    The id of the event
     *
     * @return team   A team object or null
     */
    public function get_for_climber($climber_id, $event_id) {
        $sql = 'SELECT team.* FROM '.$this->table_name.' JOIN team_climber ON team.id = team_climber.team_id WHERE team.event_id = ' . $this->db->escape($event_id) . ' AND team_climber.climber_id = ' . $this->db->escape($climber_id);
        $result = $this->db->query($sql);
        return $result->row();
    }

    /**
     * Get a climber's partner 
     *
     * @param string $climber_id    The id of the climber
     * @param string $event_id    The id of the event
     *
     * @return climber   A climber object representing the other climber in this team
     */
    public function get_partner($climber_id, $event_id) {
        $sql = 'SELECT climber.* FROM '.$this->table_name.' JOIN team_climber ON team.id = team_climber.team_id JOIN climber on team_climber.climber_id = climber.id WHERE team.event_id = ' . $this->db->escape($event_id) . ' AND team_climber.climber_id != ' . $this->db->escape($climber_id);
        $result = $this->db->query($sql);
        return $result ? $result->row() : false;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Login Controller Class
 *  Controls the login page for the website
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff      Creation
 *
 *
 *  TODO: Add forgot password functionality
 *  TODO: Add remember me checkbox (currently always remembers)
 */
class Login extends Base_Controller {

    function __construct(){
        $this->controller_name = 'login';
        $this->requires_auth = false;
        parent::__construct();
    }

    /**
     * Show the login page
     *
     */
	public function index() {
        $this->_show_view('login', "Login");
	}

    /**
     * Autherize the user with the provided username and password
     *
     * @param string username   The username to autherize (typically this is the user's email address)
     * @param string password   An sha1 encrypted password to be tested against the user's password
     */
	public function auth() {

        if ($this->auth->login($this->input->post('identity'), $this->input->post('password'), true)) {
            // if the login is successful, redirect them back to the home page
            redirect('/', 'refresh');
        }
        else {
            // if the login was unsuccessful, redirect them back to the login page
            redirect('/login', 'refresh');
        }
	}

    /**
     * Clear the currently logged in user and return to the home page
     *
     */
	public function logout() {
		$this->auth->logout();
        redirect('/', 'refresh');
	}
}
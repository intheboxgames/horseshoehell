<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Mobile Controller Class
 *  Handles sending html page views to the mobile app
 *  These pages do not contain any data, just html. The data is received through
 *  calls to the client api then processed and displayed using javascript.
 *
 *  Change Log:
 *      
 *      2014-11-07  Luke Stuff      Creation
 *      2014-11-25  Luke Stuff      Switched to page parameter instead of url locations
 *
 *  TODO: Get device statistics and device login
 *  TODO: Handle checking for updated content on device
 */
class Mobile extends Base_Controller {

    function __construct(){
        $this->controller_name = 'mobile';
        $this->requires_auth = false;

        parent::__construct();
    }

    /**
     * Send the mobile index page and javascript information
     *
     */
    function index() {
        if(!$this->agent->is_mobile()) {
            redirect('/');
        }
        $this->_show_view('mobile/index', "");
    }

    /**
     * Send the HTML for the requested page to the mobile device
     *
     * @param string $page   The name of the page to send
     */
    function page($page) {
        if(!$this->agent->is_mobile()) {
            redirect('/');
        }
        $this->_show_view('mobile/'.$page, "");
    }
}
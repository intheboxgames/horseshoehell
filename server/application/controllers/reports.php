<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Reports Controller Class
 *  Handles displaying reports and event results
 *
 *  Change Log:
 *      
 *      2014-10-15  Luke Stuff      Creation
 *
 *  TODO: Team search
 *  TODO: a lot, this control is not nearly done
 */
class Reports extends Base_Controller {

    function __construct(){
        $this->controller_name = 'reports';
        $this->requires_auth = false; // no auth required

        parent::__construct();
    }

    /**
     * Show the default report search page
     *
     */
	public function index(){
        $this->_show_view('reports/scores', "Scores");
	}

    /**
     * Search for or view information on a specific climber
     *
     * @param string $action   The action to perform on the climber (search, view, event_details)
     */
	public function climber($action) {
        // load the needed models
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("send");
        $this->load->model("route");
        $this->load->model("rating");
        $this->load->model("wall");

        switch($action) {
        	case 'search': // search for a climber using the provided term
                
				$term = $this->input->get('term');
				$climbers = $this->climber->search($term);

                // this action is always called using ajax, send the results as a JSON string
				$result = new stdClass();
				$result->climbers = $climbers;
				$result->status = 'success';
				$this->output->set_output(json_encode($result));
        		break;
        	case 'view': // view basic information about the climber

        		$climber_id = $this->input->get('climber');
        		$climber = $this->climber->get($climber_id);
        		$event_id = $this->input->get('event');

        		$event_list = $this->event->get_for_climber($climber_id);
        		$current_events = array();
        		$past_official = array();
        		$past_practice = array();

                // Get all events, current, past, and future, that this climber is a participant in
        		foreach($event_list as $event) {
                    // don't show non-public events
        			if(!$event->is_public) {
        				return;
        			}
        			$event->name = $this->_event_name($event);
        			if($event->end_time > gmdate('Y-m-d H:i:s')) {
        				$current_events[] = $event;
        			}
        			else if($event->is_official) {
        				$past_official[] = $event;
        			}
        			else {
        				$past_practice[] = $event;
        			}
        		}

                // Are they looking for a secific event?
        		if(!empty($event_id) && is_numeric($event_id) && $event_id > 0) {
                    // TODO: fill in this function
        		}
        		else {
                    // if not, then pick the event to show as the default, current events first then the more recent past event
        			if(count($current_events) > 0) {
        				$event_id = $current_events[count($current_events) - 1]->id;
        			}
        			else if(count($past_official) > 0) {
        				$event_id = $past_official[count($past_official) - 1]->id;
        			}
        			else if(count($past_practice) > 0) {
        				$event_id = $past_practice[count($past_practice) - 1]->id;
        			}
        		}

                // show the view
        		$this->data['climber'] = $climber;
        		$this->data['current_events'] = $current_events;
        		$this->data['past_official'] = $past_official;
        		$this->data['past_practice'] = $past_practice;
        		$this->data['selected_event'] = $event_id;
		        $this->_show_view('reports/climber', 'Scores - ' . $climber->first_name . ' ' . $climber->last_name);
        		break;
        	case 'event_details': // Get detailed results about a climber's score in a particular event
        		$climber_id = $this->input->get('climber');
        		$event_id = $this->input->get('event');

                // get the required information from the database
        		$climber = $this->climber->get($climber_id);
        		$event = $this->event->get($event_id);
    			$event->name = $this->_event_name($event);
    			$event->is_current = $event->end_time > gmdate('Y-m-d H:i:s');
        		$event_climber = $this->event_climber->get_for_climber($climber_id, $event_id);
        		$sends = $this->send->get_for_event_climber($event_climber->id);

                log_message("error", var_export($climber_id, true));
                log_message("error", var_export($event_id, true));

                $rating_list = $this->rating->get_all();
                $wall_list = $this->wall->get_all();

                // create required id maps
                $rating_map = array();
                foreach($rating_list as $rating) {
                    $rating_map[$rating->id] = $rating;
                }
                $wall_map = array();
                foreach($wall_list as $wall) {
                    $wall_map[$wall->id] = $wall;
                }

                // set the wall, rating and timestamp of each send
                foreach($sends as $send) {
                    $send->wall_name = $wall_map[$send->wall]->name;
                    $send->rating_name = $rating_map[$send->rating]->name;
                }

        		$event_climber->category_name = $this->_category_name($event_climber->category);

        		$this->data['climber'] = $climber;
        		$this->data['event'] = $event;
        		$this->data['event_climber'] = $event_climber;
        		$this->data['sends'] = $sends;
        		//$this->data['hourly_data'] = $hourly_data;
		        $this->_show_view('reports/climber_event_scores', 'Event Scores');
        		break;
        }
	}

    /**
     * Create a string name for the event
     *
     * @param   Event event         An event object
     *
     * @return  string              A readable string representation of the event, ie. its name
     */
    protected function _event_name($event) {
        if($event->is_official) {
            return $event->year . ' ' . $event->event_length . ' Hour Event';
        }
        else {
            return date('m/d/Y', strtotime($event->start_time)) . ' Practice Event';
        }
    }

    /**
     * Get the full catagory name for the category abbreviation
     *
     * @param   string $category    The abbreviation string for the category
     *
     * @return  string              The full name of the category
     */
    protected function _category_name($category) {
        switch($category) {
            case 'rec':
                return "Recreational";
                break;
            case 'int':
                return "Intermediate";
                break;
            case 'adv':
                return "Advanced";
                break;
            case 'eli':
                return "Elite";
                break;
        }
    }
}
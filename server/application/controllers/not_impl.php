<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Displays a Not Implemented page
 *
 *  Change Log:
 *      
 *      2015-02-25  Luke Stuff      Creation
 *
 */
class Not_impl extends Base_Controller {

    function __construct(){
        $this->controller_name = 'not_impl';
        $this->requires_auth = false;
        parent::__construct();
    }

    /**
     * Show the default routes page
     *
     */
	public function index(){
        $this->_show_view('not_impl', "Not Implemented");
	}
}
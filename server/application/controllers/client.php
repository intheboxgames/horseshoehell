<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Client Controller Class
 *  A faux REST controller that handles sending and receiving JSON data from the client
 *  Responsible for handling all CRUD and search operations from the client
 *
 *  TODO: Need to add a device lock and extra security
 *  TODO: Combine sending results into a global function
 *
 *  Change Log:
 *      
 *      2014-10-20  Luke Stuff      Creation
 */

class Client extends Base_Controller {

    function __construct(){
        $this->controller_name = 'client';
        $this->requires_auth = false; // anyone can access these functions

        parent::__construct();
    }


    /**
     * Gets route information
     *
     *
     * @return  JSON string             status = success and a list of all routes, ratings, and walls  
     */
    public function static_data() {
        $this->load->model("rating");
        $this->load->model("route");
        $this->load->model("wall");
        
        $result = new stdClass();
        $result->routes = $this->route->get_all_client();
        $result->ratings = $this->rating->get_all();
        $result->walls = $this->wall->get_all();
        $result->status = 'success';

        $this->output->set_output(json_encode($result));
    }

    /**
     * Autherize the user with the provided username and password
     *
     * @param string username   The username to autherize (typically this is the user's email address)
     * @param string password   An sha1 encrypted password to be tested against the user's password
     */
    public function auth() {

        $result = new stdClass();
        $result->status = 'success';
        if ($this->auth->login($this->input->get('username'), $this->input->get('password'), true)) {
            
            $this->auth->set_global_user(); // Set the current user to the session

            global $user;

            if(!$user && $this->requires_auth){
                $result->status = 'failed';
            }
            else {
                $this->load->model("climber");
                $result->climber = $this->climber->get_for_account($user->id);
            }
        }
        else {
            $result->status = 'failed';
        }
        $this->output->set_output(json_encode($result));
    }

    /**
     * Clear the currently logged in user and return to the home page
     *
     */
    public function logout() {
        $this->auth->logout();
    }


    /**
     * Search for a climber in the database by name
     *
     * @param   string GET.first_name      The first name of the climber to search for
     * @param   string GET.last_name       The last name of the climber to search for
     *
     * @return  JSON string             status = success, single, no_match, failed and a list of climbers that match the search   
     */
    public function find_climber() {
        $this->load->model("climber");
        $this->load->model("account");

        $first_name = $this->input->get('first_name');
        $last_name = $this->input->get('last_name');
        $birth_date = $this->input->get('birth_date');

        $result = new stdClass();
        $result->status = 'success';

        // send the params to the model
        $climbers = $this->climber->search($first_name . ' ' . $last_name);
        /*if($birth_date && strlen($birth_date) > 0) {
            $climbers_temp = $climbers;
            $climbers = [];
            foreach($climbers_temp as $climber) {
                if($climber->birth_date == $birth_date) {
                    $climbers[] = $climber;
                }
            }
            if(count($climbers) == 1) {
                $account = $this->account->get($climbers[0]->account_id);
                if(strlen($account->password) > 0) {
                    $result->status = 'password';
                }
            }
        }*/

        $result->climbers = $climbers;
        if(count($climbers) == 0) {
            $result->status = 'no_match'; // no climbers for found!
        }

        $this->output->set_output(json_encode($result));
    }

    /**
     * Create an entry in the database for the climber
     *
     * @param   string GET.first_name      The first name of the new climber
     * @param   string GET.last_name       The last name of the new climber
     * @param   string GET.birth_Date      The birth date of the new climber in the form YYYY-MM-DD
     * @param   string GET.category        The category the new climber should be in
     *
     * @return  JSON string             status = success, failed and the newly created climber   
     */
    public function register_climber() {
        $this->load->model("climber");

        $climber = new stdClass();
        $climber->first_name = $this->input->get('first_name');
        $climber->last_name = $this->input->get('last_name');
        $climber->birth_date = $this->input->get('birth_date');
        $climber->category = $this->input->get('category');

        $username = $this->input->get('username');
        $password = $this->input->get('password');

        $result = new stdClass();
        $result->status = 'success';

        if($this->auth->username_check($username)) {
            $result->status = 'username';
        }
        else {
            $account_id = $this->auth->register($username, $password, $climber->first_name, $climber->last_name, 'climber');

            if(!$account_id) {
                $result->status = 'failed';
            }
            else {
                $climber->account_id = $account_id;
                $climber->id = $this->climber->create($climber); // create the climber
                $climber->username = $username;
                $climber->role = 'climber';

                $result->climber = $climber;
                $result->status = $climber->id ? 'success' : 'failed';
            }
        }

        $this->output->set_output(json_encode($result));
    }

    /**
     * Get all events for the specified climber
     *
     * @param   int GET.climber         The id of the climber to search for
     *
     * @return  JSON string             status = success, none and a list of the events found   
     */
    public function get_events() {

        $this->load->model("event");
        $this->load->model("team");

        $climber_id = $this->input->get('climber');

        $events = $this->event->get_for_climber($climber_id);

        // Set the event name properly
        foreach($events as $event) {
            $event->name = $this->_event_name($event);
            $event->team = $this->team->get_for_climber($climber_id, $event->id);
            $event->partner = $this->team->get_partner($climber_id, $event->id);
        }

        $result = new stdClass();
        $result->events = $events;
        $result->status = count($events) > 0 ? 'success' : 'none';

        $this->output->set_output(json_encode($result));
    }

    /**
     * Get any events currently active for the specified climber
     *
     * @param   int GET.climber         The id of the climber to search for
     *
     * @return  JSON string             status = success, many, none and a list of the events found   
     */
    public function get_current_event() {

        $this->load->model("event");

        $climber_id = $this->input->get('climber');

        $events = $this->event->get_current_for_climber($climber_id);

        // Set the event name properly
        foreach($events as $event) {
            $event->name = $this->_event_name($event);
        }

        $result = new stdClass();
        $result->events = $events;
        $result->status = count($events) > 0 ? 'success' : 'none';

        $this->output->set_output(json_encode($result));
    }

    /**
     * Create a new event for the climber
     *
     * @param   int GET.climber             The id of the climber to create the event for
     * @param   int GET.event_length        The number of hours this event should last (6-36)
     * @param   bool GET.public             Should this event be viewable to the public or only the participant
     * @param   datetime GET.start_time     The date and time this event should begin
     *
     * @return  JSON string             status = success, failed and the newly created event object   
     */
    public function register_event() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");

        $climber_id = $this->input->get('climber');
        $partner_id = $this->input->get('partner');
        $event_length = $this->input->get('event_length');
        $public = $this->input->get('is_public');
        $start_time = $this->input->get('start_time');

        $climber = $this->climber->get($climber_id); // get the climber this event is being created for
        $partner = $partner_id >= 0 ? $this->climber->get($partner_id) : null; 

        $result = new stdClass();
        $result->climber = $climber;
        $result->partner = $partner;
        $result->status = 'success';

        // Setup the event object
        $event = new stdClass();
        $event->start_time = $start_time == 'now' ? gmdate('c') : $start_time;
        $event->end_time = date('c', strtotime($event->start_time) + ($event_length * 60 * 60));
        $event->year = date('Y', strtotime($start_time));
        $event->event_length = $event_length;
        $event->editable = 1;
        $event->is_official = 0; // not an official event, those are created through the web admin tool
        $event->is_public = $public;

        $event->id = $this->event->create($event); // create the event
        // make sure the creation succeeded before continuing
        if(!$event->id) {
            $result->status = 'failed';
        }
        else {
            // associate the event with the climber
            $event_climber = new stdClass();
            $event_climber->climber_id = $climber->id;
            $event_climber->event_id = $event->id;
            $event_climber->category = $climber->category;

            $event_climber->id = $this->event_climber->create($event_climber); // create the relationship

            // associate the event with the partner
            if(!is_null($partner)) {
                $event_partner = new stdClass();
                $event_partner->climber_id = $partner->id;
                $event_partner->event_id = $event->id;
                $event_partner->category = $partner->category;

                $event_partner->id = $this->event_climber->create($event_partner); // create the relationship
                $result->event_partner = $event_partner;
            }

            if(!$event_climber->id) {
                $result->status = 'failed';
            }
            else {
                $event->name = $this->_event_name($event);
                $result->event = $event;
                $result->event_climber = $event_climber;
            }
        }

        $this->output->set_output(json_encode($result));
    }

    /**
     * Updates the event information
     *
     * @return  JSON string             status = success, failed and the updated event object   
     */
    public function update_event() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("team");

        $event_id = $this->input->get('id');
        $climber_id = $this->input->get('climber');
        $partner_id = $this->input->get('partner');
        $event_length = $this->input->get('event_length');
        $public = $this->input->get('is_public');
        $start_time = $this->input->get('start_time');


        $result = new stdClass();
        $result->status = 'success';

        // Setup the event object
        $event = $this->event->get($event_id);
        if(!$event || $event->is_official == 1 /*|| strtotime($event->start_time) < time()*/) { // can't update an official event or an event that has already started
            $result->status = 'failed';
        }
        else {
            $event->start_time = $start_time == 'now' ? gmdate('c') : $start_time;
            $event->end_time = date('c', strtotime($event->start_time) + ($event_length * 60 * 60));
            $event->year = date('Y', strtotime($start_time));
            $event->event_length = $event_length;
            $event->editable = 1;
            $event->is_official = 0; // not an official event, those are created through the web admin tool
            $event->is_public = $public;

            if(!$this->event->update($event)) {
                $result->status = 'failed';
            }
            else {
                // update the partner information
                $partner = $this->team->get_partner($climber_id, $event_id);

                if($partner) {
                    $event_partner = $this->event_climber->get_for_climber($partner->id, $event->id);
                    if($partner->id != $partner_id) {
                        $this->event_climber->delete($event_partner->id);
                        $partner = null;
                    }
                    else {
                         $result->event_partner = $event_partner;
                    }
                }

                if(is_null($partner) && $partner_id > 0) {

                    $partner = $this->climber->get($partner_id);
                    if(!is_null($partner)) {
                        $event_partner = new stdClass();
                        $event_partner->climber_id = $partner->id;
                        $event_partner->event_id = $event->id;
                        $event_partner->category = $partner->category;
                        $event_partner->id = $this->event_climber->create($event_partner); // create the relationship
                        $result->event_partner = $event_partner;
                    }
                    else {
                        $result->event_partner = null;
                    }
                }

                $result->event_climber = $this->event_climber->get_for_climber($climber_id, $event->id);
            }
        }

        $result->event = $event;
        $this->output->set_output(json_encode($result));
    }

    /**
     * Create a new team for the climber(s) and event
     *
     * @param   int GET.climber             The id of the climber to create the event for
     * @param   int GET.partner             The id of the other team member, can be -1 for no partner
     * @param   string GET.name             The name of the team to create
     * @param   int GET.event               The id of the event this team is for
     *
     * @return  JSON string             status = success, failed and the newly created team object   
     */
    public function register_team() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("team");
        $this->load->model("team_climber");

        $climber_id = $this->input->get('climber');
        $partner_id = $this->input->get('partner');
        $event_id = $this->input->get('event');
        $team_name = $this->input->get('name'); 

        $result = new stdClass();
        $result->status = 'success';

        // Setup the event object
        $team = new stdClass();
        $team->name = $team_name;
        $team->event_id = $event_id;

        $team->id = $this->team->create($team); // create the team
        // make sure the creation succeeded before continuing
        if(!$team->id) {
            $result->status = 'failed';
        }
        else {
            // associate the climbers with the team
            $team_climber = new stdClass();
            $team_climber->climber_id = $climber_id;
            $team_climber->team_id = $team->id;

            $team_climber->id = $this->team_climber->create($team_climber); // create the relationship

            // associate the event with the partner
            if($partner_id > 0) {
                $partner_climber = new stdClass();
                $partner_climber->climber_id = $partner_id;
                $partner_climber->team_id = $team->id;

                $partner_climber->id = $this->team_climber->create($partner_climber); // create the relationship

                $partner = $this->climber->get($partner_id);
                $team->partner = $partner;
                $team->partner->valid = true;
            }
            else {
                $team->partner = new stdClass();
                $team->partner->valid = false;
            }

            if(!$team_climber->id) {
                $result->status = 'failed';
            }
            else {
                $result->team = $team;
            }
        }

        $this->output->set_output(json_encode($result));
    }

    /**
     * Updates a team
     *
     * @return  JSON string             status = success, failed and the updated team object   
     */
    public function update_team() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("team");
        $this->load->model("team_climber");

        $team_id = $this->input->get('id');
        $climber_id = $this->input->get('climber');
        $partner_id = $this->input->get('partner');
        $event_id = $this->input->get('event');
        $team_name = $this->input->get('name'); 

        $result = new stdClass();
        $result->status = 'success';

        // Setup the team object
        $team = $this->team->get($team_id);
        $event = $this->event->get($event_id);

        if(!$team || !$event || $event->is_official == 1 || strtotime($event->start_time) < time()) {
            $result->status = false;
        }
        else {
            $team->name = $team_name;

            // make sure the creation succeeded before continuing
            if(!$this->team->update($team)) {
                $result->status = 'failed';
            }
            else {

                $partner = $this->team->get_partner($climber_id, $event_id);
                // associate the event with the partner
                if($partner && $partner->id != $partner_id) {
                    $this->team_climber->delete($partner->id);
                }

                if($partner_id > 0 && (!$partner || $partner->id != $partner_id)) {
                    $partner_climber = new stdClass();
                    $partner_climber->climber_id = $partner_id;
                    $partner_climber->team_id = $team->id;

                    $partner_climber->id = $this->team_climber->create($partner_climber); // create the relationship

                    $partner = $this->climber->get($partner_id);
                    $team->partner = $partner;
                    $team->partner->valid = true;
                }
                else if($partner_id <= 0) {
                    $team->partner = new stdClass();
                    $team->partner->valid = false;
                }
                else {
                    $team->partner = $partner;
                    $team->partner->valid = true;
                }
            }
        }
        $result->team = $team;

        $this->output->set_output(json_encode($result));
    }

    /**
     * Get the routes available for a climber
     *
     * @param   int GET.climber         The id of the climber
     * @param   int GET.event           The id of the event to access
     *
     * @return  JSON string             status = success, failed and the list of climbs found   
     */
    public function get_climbs() {
        // load the many needed models
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("send");
        $this->load->model("route");
        $this->load->model("rating");
        $this->load->model("wall");

        $climber_id = $this->input->get('climber');
        $event_id = $this->input->get('event');

        // get the climber, the event, the relationship, and the list of current ascents 
        $climber = $this->climber->get($climber_id);
        $event = $this->event->get($event_id);
        $event_climber = $this->event_climber->get_for_climber($climber_id, $event_id);
        $sends = $this->send->get_for_event_climber($event_climber->id);

        // get all routes, ratings, and walls
        // This isn't the best way to do this but it will work for now
        // and there will never more than 400 routes, 20 ratings, and 26 walls
        $route_list = $this->route->get_all();
        $rating_list = $this->rating->get_all();
        $wall_list = $this->wall->get_all();

        // create id maps for the ratings, walls, and sends
        $rating_map = array();
        foreach($rating_list as $rating) {
            $rating_map[$rating->id] = $rating;
        }
        $wall_map = array();
        foreach($wall_list as $wall) {
            $wall_map[$wall->id] = $wall;
        }

        $send_map = array();
        foreach($sends as $send) {
            if(empty($send_map[$send->route_id])) {
                $send_map[$send->route_id] = 1;
            }
            else {
                $send_map[$send->route_id] = 2;
            }
        }

        // create the final routes list to send
        $routes = array();
        foreach($route_list as $route) {
            // filter the routes by category and event length
            if($event_climber->category == 'rec' && $route->rating > 12) {
                continue;
            }
            else if($event_climber->category == 'int' && $route->rating > 18) {
                continue;
            }
            else if($event_climber->category == 'adv' && $route->rating > 23) {
                continue;
            }
            if($event->event_length == '12' && !($route->wall >= 3 || $route->wall <= 13)) {
                continue;
            }
            // find the wall, rating, and number of ascents for this route
            $route->wall_name = $wall_map[$route->wall]->name;
            $route->rating_name = $rating_map[$route->rating]->name;
            $route->laps = empty($send_map[$route->id]) ? 0 : $send_map[$route->id];
            $routes[] = $route;
        }

        $result = new stdClass();
        $result->routes = $routes;
        $result->status = 'success';

        $this->output->set_output(json_encode($result)); // send the routes
    }

    /**
     * Add a send to a climber's score
     *
     * @param   int GET.climber         The id of the climber
     * @param   int GET.event           The id of the event
     * @param   int GET.route           The id of the route sent
     * @param   bool GET.pink_point     Was this trad route sent pink point or red point
     *
     * @return  JSON string             status = success, uneditable, too_many, wrong_category, failed and the new lap number for the climb   
     */
    public function add_lap() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("send");
        $this->load->model("route");
        $this->load->model("rating");

        $climber_id = $this->input->get('climber');
        $event_id = $this->input->get('event');
        $route_id = $this->input->get('route');

        // get the event and the routes
        $event = $this->event->get($event_id);
        $route = $this->route->get($route_id);

        // make sure this event is still active and can be edited
        if($event->editable == 0) {
            $result = new stdClass();
            $result->status = 'uneditable';
            $this->output->set_output(json_encode($result));
            return;
        }

        // get the event-climber relationship and the number of laps so far on this route
        $event_climber = $this->event_climber->get_for_climber($climber_id, $event_id);
        /*
        // make sure the route is in the climber's category and is available for this event
        if(($event_climber->category == 'rec' && $route->rating > 12) ||
           ($event_climber->category == 'int' && $route->rating > 18) ||
           ($event_climber->category == 'adv' && $route->rating > 23) ||
           ($event->event_length == '12' && !($route->wall >= 3 || $route->wall <= 13))) {
            $result = new stdClass();
            $result->status = 'wrong_category';
            $this->output->set_output(json_encode($result));
            return;
        }*/

        // create the send record
        $send = new stdClass();
        $send->climber_id = $climber_id;
        $send->event_id = $event_id;
        $send->event_climber_id = $event_climber->id;
        $send->route_id = $route_id;
        $send->team_id = $event_climber->team_id;
        $send->pink_point = $route->trad == 1 ? $this->input->get('pink_point') : 0;
        $send->send_time = $this->input->get('send_time');
        $send->lap = $this->input->get('lap');

        $existing = $this->send->get_specific($send->event_climber_id, $send->route_id, $send->pink_point, $send->lap);
        if($existing) {
            $result = new stdClass();
            $result->status = 'too_many';
            $this->output->set_output(json_encode($result));
            return;
        }

        // adjust the score earned for trad and height
        $adjusted_rating = $route->rating;
        if($route->trad == 1 && $send->pink_point == 0) {
            $adjusted_rating += 1;
        }
        if($route->height >= 60) {
            $adjusted_rating += 1;
        }
        $rating = $this->rating->get($adjusted_rating);

        $send->score = $rating->points;

        $send->id = $this->send->create($send); // create the send

        // send the results to the client
        $result = new stdClass();
        $result->send = $send;
        $result->status = $send->id ? 'success' : 'failed';
        $this->output->set_output(json_encode($result));
    }

    /**
     * Remove a send from a climber's score
     *
     * @param   int GET.climber         The id of the climber
     * @param   int GET.event           The id of the event
     * @param   int GET.route           The id of the route sent
     * @param   bool GET.pink_point     Was this trad route sent pink point or red point
     *
     * @return  JSON string             status = success, uneditable, too_few, failed   
     */
    public function remove_lap() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("send");
        $this->load->model("route");
        $this->load->model("rating");

        $climber_id = $this->input->get('climber');
        $event_id = $this->input->get('event');
        $route_id = $this->input->get('route');
        $pink_point = $this->input->get('pink_point');
        $lap_number = $this->input->get('lap');

        // get the event and route in question
        $event = $this->event->get($event_id);
        $route = $this->route->get($route_id);

        // make sure the event is still active and can be edited
        if($event->editable == 0) {
            $result = new stdClass();
            $result->status = 'uneditable';
            $this->output->set_output(json_encode($result));
            return;
        }

        // get the climber-event relation and the list of sends on this route
        $event_climber = $this->event_climber->get_for_climber($climber_id, $event_id);

        $existing = $this->send->get_specific($event_climber->id, $route_id, $pink_point, $lap_number);
        if(!$existing) {
            $result = new stdClass();
            $result->status = 'too_few';
            $this->output->set_output(json_encode($result));
            return;
        }
        $result = new stdClass();
        $result->send = $existing;
        $result->status = 'success';

        // If this is an official event, just remove the send from the score but keep a record of it
        /*if($event->is_official == '1') {
            $existing->deleted = gmdate('Y-m-d H:i:s');

            $this->send->update($existing);
        }
        else {*/
            // otherwise, completely remove the send
            if(!$this->send->delete($existing->id)) {
                $result->status = 'failed';
            }
        //}

        // send the results to the client
        $this->output->set_output(json_encode($result));
    }

    /**
     * Update a send from a climber's score
     *
     * @param   int GET.climber         The id of the climber
     * @param   int GET.event           The id of the event
     * @param   int GET.route           The id of the route sent
     * @param   bool GET.pink_point     Was this trad route sent pink point or red point
     *
     * @return  JSON string             status = success, uneditable, too_few, failed   
     */
    public function update_lap() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("send");
        $this->load->model("route");
        $this->load->model("rating");

        $climber_id = $this->input->get('climber');
        $event_id = $this->input->get('event');
        $route_id = $this->input->get('route');
        $pink_point = $this->input->get('pink_point');
        $send_time = $this->input->get('send_time');
        $lap_number = $this->input->get('lap');

        // get the event and route in question
        $event = $this->event->get($event_id);
        $route = $this->route->get($route_id);

        // make sure the event is still active and can be edited
        if($event->editable == 0) {
            $result = new stdClass();
            $result->status = 'uneditable';
            $this->output->set_output(json_encode($result));
            return;
        }

        // get the climber-event relation and the list of sends on this route
        $event_climber = $this->event_climber->get_for_climber($climber_id, $event_id);

        $existing = $this->send->get_specific($event_climber->id, $route_id, $pink_point, $lap_number);
        if(!$existing) {
            $result = new stdClass();
            $result->status = 'too_few';
            $this->output->set_output(json_encode($result));
            return;
        }

        $existing->send_time = $send_time;
        $this->send->update($existing);

        // send the results to the client
        $result = new stdClass();
        $result->send = $existing;
        $result->status = 'success';
        $this->output->set_output(json_encode($result));
    }

    /**
     * Sync all laps from the client
     *
     * @param   int GET.climber         The id of the climber
     * @param   int GET.event           The id of the event
     * @param   int GET.laps            The lap information
     *
     * @return  JSON string             status = success, uneditable, too_many, wrong_category, failed and the new lap number for the climb   
     */
    public function sync_laps() {
        $this->load->model("climber");
        $this->load->model("event");
        $this->load->model("event_climber");
        $this->load->model("send");
        $this->load->model("route");
        $this->load->model("rating");

        $climber_id = $this->input->get('climber');
        $event_id = $this->input->get('event');
        $laps = json_decode($this->input->get('laps'));

        // get the event and the routes
        $event = $this->event->get($event_id);

        // make sure this event is still active and can be edited
        if($event->editable == 0) {
            $result = new stdClass();
            $result->status = 'uneditable';
            $this->output->set_output(json_encode($result));
            return;
        }

        $result = new stdClass();
        $result->status = 'success';

        // get the event-climber relationship and the number of laps so far on this route
        $event_climber = $this->event_climber->get_for_climber($climber_id, $event_id);
        $unsynced = [];

        foreach($laps as $key => $lap) {
            if(!is_null($lap)) {
                $route = $this->route->get($lap->route);
                $existing = $this->send->get_specific($event_climber->id, $lap->route, $lap->pink_point, $lap->lap);
                switch($lap->action) {
                    case 'add':
                        $send = new stdClass();
                        $send->climber_id = $climber_id;
                        $send->event_id = $event_id;
                        $send->event_climber_id = $event_climber->id;
                        $send->route_id = $lap->route;
                        $send->team_id = $event_climber->team_id;
                        $send->pink_point = $lap->pink_point;
                        $send->send_time = gmdate('Y-m-d H:i:s', strtotime($lap->send_time));
                        $send->lap = $lap->lap;

                        if($existing) {
                            $unsynced[] = $lap;
                            break;
                        }

                        // adjust the score earned for trad and height
                        $adjusted_rating = $route->rating;
                        if($route->trad == 1 && $send->pink_point == 0) {
                            $adjusted_rating += 1;
                        }
                        if($route->height >= 60) {
                            $adjusted_rating += 1;
                        }
                        $rating = $this->rating->get($adjusted_rating);

                        $send->score = $rating->points;

                        $send->id = $this->send->create($send); // create the send
                        if(!$send->id) {
                            $unsynced[] = $lap;
                        }
                        break;
                    case 'remove':       
                        if(!$existing) {
                            $unsynced[] = $lap;
                            break;
                        }

                        if(!$this->send->delete($existing->id)) {
                            $unsynced[] = $lap;
                        }
                        break;
                    case 'update':        
                        if(!$existing) {
                            $unsynced[] = $lap;
                        }

                        $existing->send_time = gmdate('Y-m-d H:i:s', strtotime($lap->send_time));
                        if(!$this->send->update($existing)) {
                            $unsynced[] = $lap;
                        }
                        break;
                }
            }
        }



        // send the results to the client
        $result->unsynced = $unsynced;
        $this->output->set_output(json_encode($result));
    }

    /**
     * Create a string name for the event
     *
     * @param   Event event         An event object
     *
     * @return  string              A readable string representation of the event, ie. its name
     */
	protected function _event_name($event) {
		if($event->is_official) {
			return $event->year . ' ' . $event->event_length . ' Hour Event';
		}
		else {
			return date('m/d/Y', strtotime($event->start_time)) . ' Practice Event';
		}
	}

    /**
     * Get the full catagory name for the category abbreviation
     *
     * @param   string $category    The abbreviation string for the category
     *
     * @return  string              The full name of the category
     */
	protected function _category_name($category) {
		switch($category) {
			case 'rec':
				return "Recreational";
				break;
			case 'int':
				return "Intermediate";
				break;
			case 'adv':
				return "Advanced";
				break;
			case 'eli':
				return "Elite";
				break;
		}
	}
}
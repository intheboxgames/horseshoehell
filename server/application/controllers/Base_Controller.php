<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 *  Base Controller Class
 *  Contains general functions for handling all server calls
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff      Creation
 *      2014-11-20  Luke Stuff      Added mobile functionality
 */

class Base_Controller extends CI_Controller{

    public $controller_name = null;
    public $requires_auth = false;
    public $sidebar = null;

    public function __construct(){
        parent::__construct();

        // Make sure that the globally used libraries are loaded
        $this->load->library('user_agent');
        $this->load->library('Auth_library', null, 'auth');
        $this->load->library('Message_library', null, 'message');

        $this->init();
    }

    /**
     * Initialize this controller
     * Make sure the user is autherized to view this page
     *
     */
    public function init(){

        log_message('debug', 'Initializing '.$this->controller_name);

        // See if the user is logged in and autherized to view this page
        if(!$this->auth->logged_in() && $this->requires_auth){
            log_message('info', 'Redirecting to Login from '.$this->controller_name);
            redirect('/login');
        }

        $this->auth->set_global_user(); // Set the current user to the session

        global $user;
        if(!$user && $this->requires_auth){
            $this->message->add_error('Invalid User!');
            redirect('login');
        }
        else if($user) {
            $this->data['is_authed'] = true;
            $this->data['user'] = $user;
        }
        else {
            $this->data['is_authed'] = false;
            $this->data['user'] = null;
        }
    }

    /**
     * Search for a view with the specified name and send it to the client
     *
     * @param   string $view    The name of the view to show
     * @param   string $title   The name to display in the browser tab and in the page header
     */
    protected function _show_view($view, $title){
        if(empty($this->data)){
            $this->data = array();
        }
        $this->data['title'] = $title;

        // Get the resulting html from the requested view
        $content = $this->load->view($view, $this->data, true);

        // If this is an ajax call then return just the page content
        // otherwise return the full page with headers, footers, and sidebars
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $output = $content;
        }
        else {
            $mainFolder = 'common';
            // if this is coming from the mobile app then make sure to show the app version
            if($this->agent->is_mobile() && $this->controller_name == 'mobile') {
                $mainFolder = 'mobile';
            }
            // If this page has a sidebar, get it and add it to the view data
            if($this->sidebar != null && is_array($this->sidebar) && count($this->sidebar) > 0) {
                $this->data['has_sidebar'] = true;
                $this->data['sidebar'] = $this->load->view($mainFolder."/sidebar", array('sidebar' => $this->sidebar), true);
            }
            else {
                $this->data['has_sidebar'] = false;
            }
            $this->data['content'] = $content;
            // get the processed, combined html for the entire page
            $output = $this->load->view($mainFolder.'/main', $this->data, true);
        }

        $this->message->clear_messages('all'); // clear messages so they don't appear twice

        $this->output->set_output($output); // send the output to the browser
    }
}
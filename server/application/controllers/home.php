<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Home Controller Class
 *  The index page or home page for the website, just displays the home page
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff      Creation
 */
class Home extends Base_Controller {

    function __construct(){
        $this->controller_name = 'home';
        $this->requires_auth = false; // no auth required
        parent::__construct();
    }

    /**
     * Show the home page
     *
     */
	public function index(){
        $this->_show_view('home', "Home");
	}
}
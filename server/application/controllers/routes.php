<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Routes Controller Class
 *  Handles displaying route information on the website
 *	Also handles user route pictures, commenting, and route discussions
 *
 *  Change Log:
 *      
 *      2014-10-15  Luke Stuff      Creation
 *
 *  TODO: Everything. This is currently just a stub and needs to be filled in
 */
class Routes extends Base_Controller {

    function __construct(){
        $this->controller_name = 'routes';
        $this->requires_auth = false;
        parent::__construct();
    }

    /**
     * Show the default routes page
     *
     */
	public function index(){
        $this->_show_view('routes/routes', "Routes");
	}
}
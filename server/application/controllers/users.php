<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  User Managment Controller Class
 *  Helps with website admin page controller.
 *  Handles user management, creation, and deletions
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff      Creation
 *
 *
 *  TODO: Add role assignment
 */
class Users extends Base_Controller {

    function __construct(){
        $this->controller_name = 'users';
        $this->requires_auth = true;

        parent::__construct();
    }

    /**
     * Pass the data along to the account controller
     *
     */
    public function index() {
        $this->account('view');
    }

    /**
     * Manage website users
     *
     * @param string $action   The CRUD action to perform on the user (view, add, edit)
     */
    public function account($action = 'view') {
        global $user;

        $this->load->model("account");

        switch($action) {
            case 'add': // create a new user
                $new_user = new stdClass();
                $new_user->username = $this->input->post('username');
                $new_user->first_name = $this->input->post('first_name');
                $new_user->last_name = $this->input->post('last_name');
                $new_user->role = $this->input->post('role');
                $new_user->password = $this->input->post('password');
                $new_user->password_confirm = $this->input->post('password_confirm');

                // make sure the password is long enough
                if(strlen($new_user->password) < 3) {
                    $this->message->add_error("Your password must be at least 3 characters long");
                    redirect(base_url("users"));
                    return;
                }
                // make sure the passwords match
                if($new_user->password != $new_user->password_confirm) {
                    $this->message->add_error("Your password and password confirm do not match");
                    redirect(base_url("users"));
                    return;
                }

                // create the user
                $new_user->id = $this->account->register($new_user->username, $new_user->password, $new_user->first_name, $new_user->last_name, $new_user->role);

                if(!$new_user->id) {
                    $this->message->add_error("There was a problem creating the user. Please try again.");
                }
                else {
                    $this->message->add_success("User successfully added");
                }

                redirect(base_url("users"));
                break;
            case 'edit': // update the specified user
                $edit_user = new stdClass();
                $edit_user->username = $this->input->post('username');
                $edit_user->first_name = $this->input->post('first_name');
                $edit_user->last_name = $this->input->post('last_name');

                // only admin users can edit other users
                if($user->role != "admin") {
                    // if you are not an admin, you can only edit your self
                    if($user->id != $this->input->post('id')) {
                        $this->message->add_error("Oh No! There was a problem saving your account changes. Please try again.");
                        redirect(base_url("users"));
                        return;
                    }
                    $edit_user->role = $user->role;
                }
                else {
                    // admin users can edit anyone
                    $user = $this->account->get($this->input->post('id'));
                    $edit_user->role = $this->input->post('role');
                }

                // Save new username and name
                if($user->username != $edit_user->username || $user->first_name != $edit_user->first_name || $user->last_name != $edit_user->last_name || $edit_user->role != $user->role) {
                    // make sure the user doesn't already exist
                    if($user->username != $edit_user->username && $this->auth->username_check($edit_user->username)) {
                        $this->message->add_error("That username already exists. Please try a different one.");
                        redirect(base_url("users"));
                        return;
                    }
                    // update the user and update the session with the yodated user
                    if($this->account->update($user->id, $edit_user)){
                        $user = $this->account->get($user->id);
                        $this->account->set_session($user);
                        $this->account->remember_user($user->id);
                    }
                    else {
                        $this->message->add_error("There was a problem saving your changes. Please try again");
                        redirect(base_url("users"));
                        return;
                    }
                }

                // check to see if the password changed
                $password = $this->input->post('password');
                $password_confirm = $this->input->post('password_confirm');

                // Password changed
                if($password != '**************') {
                    // make sure it is a valid password
                    if(strlen($password) < 3) {
                        $this->message->add_error("Your password must be at least 3 characters long");
                        redirect(base_url("users"));
                        return;
                    }
                    if($password != $password_confirm) {
                        $this->message->add_error("Your password and password confirm do not match");
                        redirect(base_url("users"));
                        return;
                    }
                    // change the password
                    if($this->account->change_password($user->username, $password)) {
                        $this->message->add_success("Account successfully updated");
                    }
                    else {
                        $this->message->add_error("There was a problem saving your changes. Please try again");
                    }
                }
                else {
                    $this->message->add_success("Account successfully updated");
                }

                redirect(base_url("users"));
                break;
            case 'view': // view the users list
            default:
                // if the current user is an admin then display all users
                if($user->role != "admin") {
                    $this->data['user'] = $this->account->get($user->id);
                    $this->_show_view('manage/account', "Manage Account");
                }
                else {
                    // if not an admin, only display the current user
                    $this->data['user_list'] = $this->account->get_all();
                    $this->_show_view('manage/users', "Manage Users");
                }
                break;
        }
    }
}
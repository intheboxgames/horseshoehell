<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."controllers/Base_Controller.php";

/*
 *  Manage Controller Class
 *  The website admin page controller.
 *  Handles CRUD operations for most admin editable database objects
 *
 *  Change Log:
 *      
 *      2014-10-06  Luke Stuff      Creation
 *      2014-10-07  Luke Stuff      Added routes management
 *      2014-10-14  Luke Stuff      Added user, wall, and team management
 *
 *
 *  TODO: Add climber and climber-event route management
 *  TODO: Add event management
 *  TODO: Add lottery and registration management
 */
class Manage extends Base_Controller {

    function __construct(){
        $this->controller_name = 'manage';
        $this->requires_auth = true; // This page does require auth

        // This page has a sidebar for easier navagation
        $this->sidebar = array(
            'ratings' => array('text' => "Ratings", 'link' => 'manage/ratings', 'active' => false),
            'walls' => array('text' => "Walls", 'link' => 'manage/walls', 'active' => false),
            'routes' => array('text' => "Routes", 'link' => 'manage/routes', 'active' => false),
            'teams' => array('text' => "Teams", 'link' => 'manage/teams', 'active' => false),
        );
        parent::__construct();

        if(!$this->auth->is_admin()) {
            $this->message->add_error("You are not autherized to view that page");
            redirect(base_url(""));
        }
    }

    /**
     * Show the main admin page
     *
     */
    public function index() {
        $this->_show_view('manage/manage', "Manage");
    }

    /**
     * Manage the ratings information
     *
     * @param string $action   The CRUD action to perform on ratings (view, add, edit)
     */
    public function ratings($action = 'view') {

        $this->load->model("rating");

        $this->sidebar['ratings']['active'] = true; // set this as the active sidebar page

        // what are we doing to the ratings?
        switch($action) {
           /* case 'add':
                // create a new rating object
                $rating = new stdClass();
                $rating->name = $this->input->post('name');
                $rating->points = $this->input->post('points');

                $rating->id = $this->rating->create($rating);
                if(!$rating->id) {
                    $this->message->add_error("There was a problem saving the rating. Please try again.");
                }
                else {
                    $this->message->add_success("Wall successfully added");
                }
                redirect(base_url("manage/rating"));
                break;*/
            case 'edit':
                // update an existing rating object
                $rating = new stdClass();
                $rating->id = $this->input->post('id');
                //$rating->name = $this->input->post('name');
                $rating->points = $this->input->post('points');

                if(!$this->rating->update($rating)) {
                    $this->message->add_error("There was a problem saving the rating. Please try again.");
                }
                else {
                    $this->message->add_success("Rating successfully updated");
                }
                redirect(base_url("manage/ratings"));
                break;
            case 'view':
            default:
                // get a list of all walls and the number of routes on each wall
                $this->load->model("route");

                $rating_list = $this->rating->get_all();
                $route_count = $this->route->get_route_count_by_rating();

                // create a rating id map of the route count
                $route_count_map = array();
                foreach($route_count as $route) {
                    $route_count_map[$route->rating] = $route->routes;
                }

                // set the route count
                foreach($rating_list as $rating) {
                    $rating->route_count = !empty($route_count_map[$rating->id]) ? $route_count_map[$rating->id] : 0;
                }

                $this->data['rating_list'] = $rating_list;
                $this->_show_view('manage/ratings', "Manage Ratings");
                break;
        }
    }

    /**
     * Manage the walls and areas information
     *
     * @param string $action   The CRUD action to perform on walls (view, add, edit)
     */
    public function walls($action = 'view') {

        $this->load->model("wall");

        $this->sidebar['walls']['active'] = true; // set this as the active sidebar page

        // what are we doing to the walls?
        switch($action) {
            case 'add':
                // create a new wall object
                $wall = new stdClass();
                $wall->name = $this->input->post('name');
                $wall->side = $this->input->post('side');
                $wall->crag = $this->input->post('crag');

                $wall->id = $this->wall->create($wall);
                if(!$wall->id) {
                    $this->message->add_error("There was a problem saving the wall. Please try again.");
                }
                else {
                    $this->message->add_success("Wall successfully added");
                }
                redirect(base_url("manage/walls"));
                break;
            case 'edit':
                // update an existing wall object
                $wall = new stdClass();
                $wall->id = $this->input->post('id');
                $wall->name = $this->input->post('name');
                $wall->side = $this->input->post('side');
                $wall->crag = $this->input->post('crag');

                if(!$this->wall->update($wall)) {
                    $this->message->add_error("There was a problem saving the wall. Please try again.");
                }
                else {
                    $this->message->add_success("Wall successfully updated");
                }
                redirect(base_url("manage/walls"));
                break;
            case 'view':
            default:
                // get a list of all walls and the number of routes on each wall
                $this->load->model("route");

                $wall_list = $this->wall->get_all();
                $route_count = $this->route->get_route_count_by_wall();

                // create a wall id map of the route count
                $route_count_map = array();
                foreach($route_count as $route) {
                    $route_count_map[$route->wall] = $route->routes;
                }

                // set the route count
                foreach($wall_list as $wall) {
                    $wall->route_count = !empty($route_count_map[$wall->id]) ? $route_count_map[$wall->id] : 0;
                }

                $this->data['wall_list'] = $wall_list;
                $this->_show_view('manage/walls', "Manage Walls");
                break;
        }
    }

    /**
     * Manage route information
     *
     * @param string $action   The CRUD action to perform on routes (view, add, edit)
     */
    public function routes($action = 'view') {

        $this->load->model("route");
        $this->load->model("rating");
        $this->load->model("wall");

        $this->sidebar['routes']['active'] = true; // set this as the active sidebar page

        switch($action) {
            case 'add':
                // create the route object with the provided values
                $route = new stdClass();
                $route->number = $this->input->post('number');
                $route->page = $this->input->post('page');
                $route->position = $this->input->post('position');
                $route->name = $this->input->post('name');
                $route->wall = $this->input->post('wall');
                $route->rating = $this->input->post('rating');
                $route->trad = $this->input->post('trad');
                $route->height = $this->input->post('height');
                $route->safety_rating = $this->input->post('safety_rating');
                $route->year = $this->input->post('year');

                $route->id = $this->route->create($route);
                if(!$route->id) {
                    $this->message->add_error("There was a problem saving the route. Please try again.");
                }
                else {
                    $this->message->add_success("Route successfully added");
                }
                redirect(base_url("manage/routes"));
                break;
            case 'edit':
                // update an existing route with the provided values
                $route = new stdClass();
                $route->id = $this->input->post('id');
                $route->number = $this->input->post('number');
                $route->page = $this->input->post('page');
                $route->position = $this->input->post('position');
                $route->name = $this->input->post('name');
                $route->wall = $this->input->post('wall');
                $route->rating = $this->input->post('rating');
                $route->trad = $this->input->post('trad');
                $route->height = $this->input->post('height');
                $route->safety_rating = $this->input->post('safety_rating');
                $route->year = $this->input->post('year');

                if(!$this->route->update($route)) {
                    $this->message->add_error("There was a problem saving the route. Please try again.");
                }
                else {
                    $this->message->add_success("Route successfully updated");
                }
                redirect(base_url("manage/routes"));
                break;
            case 'view':
            default:
                // get a list of all routes
                $route_list = $this->route->get_all();
                $rating_list = $this->rating->get_all();
                $wall_list = $this->wall->get_all();

                // create rating and wall maps
                $rating_map = array();
                foreach($rating_list as $rating) {
                    $rating_map[$rating->id] = $rating;
                }
                $wall_map = array();
                foreach($wall_list as $wall) {
                    $wall_map[$wall->id] = $wall;
                }

                // set the rating and wall names for each route
                foreach($route_list as $route) {
                    $route->wall_name = $wall_map[$route->wall]->name;
                    $route->rating_name = $rating_map[$route->rating]->name;
                }

                $this->data['route_list'] = $route_list;
                $this->data['rating_list'] = $rating_list;
                $this->data['wall_list'] = $wall_list;
                $this->_show_view('manage/routes', "Manage Routes");
                break;
        }
    }
}
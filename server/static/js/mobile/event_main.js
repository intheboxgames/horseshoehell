var EventMainModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'EventMainModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var EventMainView = {
	link: 'mobile/page/event_main',
	viewLevel: 1,
	timer: null,

	init: function() {
		this.name = 'event_main';
		this.initBase();
	},

	events: [
		{selector: '#routes-button', event: 'click', handler: "onRoutes"},
		{selector: '#progress-button', event: 'click', handler: "onProgress"},
		{selector: '#goals-button', event: 'click', handler: "onGoals"},
		{selector: '#compare-button', event: 'click', handler: "onCompare"},
		{selector: '#settings-button', event: 'click', handler: "onSettings"},
	],
	onPreShow: function() {
		var t = this;
		window.application.closeView('splash');

		var climber = window.application.data.climber;
		$('.event-header .climber-name').html(climber.first_name + ' ' + climber.last_name);
		$('.event-header .climber-category').remove();//html(climber.category == 'rec' ? 'Recreational' : (climber.category == 'int' ? 'Intermediate' : (climber.category == 'adv' ? 'Advanced' : 'Elite')));
		$('.event-header .team-name').html(window.application.data.team.name);


		var endTime = new Date(window.application.data.event.end_time).getTime();

		function eventEnded() {
			if(typeof RoutesView !== 'undefined') {
				RoutesView.updateRoutesList();
			}
			var e = $('.event-header .countdown').parent();
			e.html('<div class="ending-text">Event has ended<br/>All scores are final.</div>');
		};

		function startEndCountdown() {
			endTime += (15 * 60 * 1000); // allow changes for up to 15 minutes after the end of the event
			if(endTime <= new Date()) {
				eventEnded();
			}
			else {
				var e = $('.event-header .countdown').parent();
				e.html('<span class="ending-text">Event ended. Time left to make changes: </span><span class="countdown">00:10:14</span>');
				$('.event-header .header-column').css({width: '48%'});
				$('.event-header .countdown').css({'font-size': '1em'});
				t.setCountdownText(endTime);
				t.timer = setInterval(function() {
					t.setCountdownText(endTime);
					if(endTime <= new Date()) {
						eventEnded();
					}
				}, 1000); // every second
			}
		};

		if(endTime <= new Date()) {
			startEndCountdown();
		}
		else {

			t.setCountdownText(endTime);
			t.timer = setInterval(function() {
				t.setCountdownText(endTime);
				if(endTime <= new Date()) {
					startEndCountdown();
				}
			}, 1000); // every second
		}

		window.application.data.updateGoalsProgress();
		window.application.showView('routes');
	},
	onHide: function() {
		var t = this;
		clearInterval(t.timer);
	},
	onRoutes: function() {
		var t = EventMainView;
		window.application.data.next_view_data = 'event';
		window.application.showView('routes');
	},
	onProgress: function() {
		var t = EventMainView;
		window.application.data.next_view_data = 'event';
		window.application.showView('progress');
	},
	onGoals: function() {
		var t = EventMainView;
		window.application.data.next_view_data = 'event';
		window.application.showView('goals');
	},
	onCompare: function() {
		var t = EventMainView;
		window.application.data.next_view_data = 'event';
		window.application.showView('compare_settings');
	},
	onSettings: function() {
		var t = EventMainView;
		window.application.data.next_view_data = 'event';
		window.application.showView('settings');
	},
	setCountdownText: function(time) {

		var timeLeft = time - new Date().getTime();
		var seconds = Math.floor(timeLeft / 1000);
		var minutes = Math.floor(seconds / 60);
		var hours = Math.floor(minutes / 60);
		seconds -= minutes * 60;
		minutes -= hours * 60;
		var s = (hours < 10 ? '0' : '') + hours;
		s += (minutes < 10 ? ':0' : ':') + minutes;
		s += (seconds < 10 ? ':0' : ':') + seconds;
		$('.event-header .countdown').html(s);
	}
};

EventMainView = mergeObjects(View, EventMainView);
EventMainModel = mergeObjects(Model, EventMainModel);

window.application.loadedViews['event_main'] = EventMainView;
window.application.loadedModels['event_main'] = EventMainModel;
var ClimberSelectModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'ClimberSelectModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var ClimberSelectView = {
	link: 'mobile/page/climber_select',

	init: function() {
		this.name = 'climber_select';
		this.initBase();
	},

	events: [
		{selector: '.climber-summary', event: 'click', handler: "onSelected"},
		{selector: '#climber_select-submit', event: 'click', handler: "onSubmit"},
		{selector: '#climber_select-back', event: 'click', handler: "onBack"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.hasOwnProperty('climbers')) {
			var climbers = window.application.data.climbers;
			
			var template = t.element.find('.climber-summary');
			var base = t.element.find('.form-wrapper');
			for(var i = climbers.length - 1; i >= 0; --i)
			{
				var e;
				if(i == climbers.length - 1) {
					e = template;
				}
				else {
					e = template.clone();
				}
				e.data('climber', climbers[i].id);
				e.find('.climber-name').html(climbers[i].first_name + " " + climbers[i].last_name);
				e.find('.climber-username').html(climbers[i].username);
				e.find('.climber-date').html('Created: ' + climbers[i].created.substring(0, 10));
				if(i != climbers.length - 1) {
					base.prepend(e);
				}
			}

			if(window.application.data.next_view_data == 'main') {
				$('#climber_select-wrapper .subtitle').html('It looks like there are several climbers with that name. Please select your account from the list below.');
			}
			else if(window.application.data.next_view_data == 'partner') {
				$('#climber_select-wrapper .subtitle').html('There are several climbers with your partner\'s name. Please select your partner from the list below');
			}
		}
		else {
			alert('Something has gone terribly, horribly wrong!');
		}
	},
	onSelected: function(e) {
		var t = ClimberSelectView;
		t.element.find('.climber-summary').removeClass('selected');
		$(e.currentTarget).addClass('selected');
	},
	onSubmit: function() {
		var t = ClimberSelectView;

		// validate the data
		var selected = t.element.find('.climber-summary.selected');
		var climber = null;
		var errors = "";
		if(selected.length == 0) {
			errors = "You must select a climber to continue.";
		}
		else {
			climber = selected.data('climber');
			if(climber == null || climber <= 0) {
				errors = "You selected an invalid climber, please pick a different one.";
			}
		}
		if(errors.length > 0) {
			showError(errors);
			return;
		}

		for(var i = 0; i < window.application.data.climbers.length; ++i) {
			if(window.application.data.climbers[i].id == climber) {
				climber = window.application.data.climbers[i];
				climber.valid = true;
				break;
			}
		}

		if(typeof(climber) != 'object' || !climber.hasOwnProperty('valid') || !climber.valid) {
			showError("The climber you selected doesn't exist!");
			return;
		}

		if(window.application.data.next_view_data == 'main') {
			window.application.data.makeServerCall('/client/auth', {climber: climber.username, password: ''}, function(result) {
				if(result && result.status == 'success') {
					window.application.data.climber = result.climber;
					window.application.data.climber.valid = true;
					if(window.application.data.hasOwnProperty('climbers')) { delete window.application.data.climbers; }
					if(window.application.data.hasOwnProperty('event')) { delete window.application.data.event; }
					if(window.application.data.hasOwnProperty('team')) { delete window.application.data.team; }
					window.application.data.saveLocal();
					window.application.data.checkForEvents();
				}
				else {
					hideLoading();
					showError("The account you selected is protected by a password. Please enter your password below to login.");
					window.application.data.climber = climber;
					window.application.data.climber.valid = false;
					window.application.showView("welcome");
				}
			});
		}
		else if(window.application.data.next_view_data == 'partner') {
			window.application.data.team.partner = climber;
			window.application.data.team.partner.valid = true;
			if(window.application.data.hasOwnProperty('climbers')) { delete window.application.data.climbers; }
			window.application.data.saveLocal();
			window.application.data.createEventServer();
		}
		else {
			showError("You selected a climber but nothing told you to. How did you get here?");
		}
	},
	onBack: function() {
		var t = ClimberSelectView;
		if(window.application.data.next_view_data == 'main') {
			window.application.showView("welcome");
		}
		else if(window.application.data.next_view_data == 'partner') {
			window.application.showView("event_setup");
		}
	}
};

ClimberSelectView = mergeObjects(View, ClimberSelectView);
ClimberSelectModel = mergeObjects(Model, ClimberSelectModel);

window.application.loadedViews['climber_select'] = ClimberSelectView;
window.application.loadedModels['climber_select'] = ClimberSelectModel;
var RegisterModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'RegisterModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var RegisterView = {
	link: 'mobile/page/register',

	init: function() {
		this.name = 'RegisterView';
		this.initBase();
	},

	events: [
		{selector: '#register-submit', event: 'click', handler: "onSubmit"},
		{selector: 'body', event: 'keypress', handler: "onSubmit"},
	],
	onSubmit: function() {
		var t = this;
		window.application.showView("");
	},
	onBack: function() {
		var t = this;
		window.application.showView("welcome");
	}
};
RegisterView = mergeObjects(View, RegisterView);
RegisterModel = mergeObjects(Model, RegisterModel);

window.application.loadedViews['register'] = RegisterView;
window.application.loadedModels['register'] = RegisterModel;
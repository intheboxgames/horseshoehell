window.application.data = {
	version: 0,
	isLoaded: false,
	isLoading: false,

	settings: {
		theme: 'light',
		autoLock: true,
		progress: 'goals',
		lowPower: false,
		preventScoreChanges: true,
	},

	makeServerCall: function(url, data, callback) {
		var query = "";
		if(data) {
			query += '?';
			for(var key in data) {
				query += key + '=' + data[key] + '&';
			}
			query = query.substring(0, query.length - 1);
		}
		$.get(url + query, function(result) {
			if(result) {
				result = JSON.parse(result);
			}
			if(callback) {
				callback(result);
			}
		}).fail(function( jqxhr, settings, exception ) {
			if(callback) {
				callback(null);
			}
			alert("There was error contacting the server. Please make sure you are connected to the internet and try again.")
		    debugger;
		});
	},

	hasStaticData: false,
	loadingStaticData: false,
	loadStaticData: function(callback, force) {
		var t = this;
		if(t.loadingStaticData) {
			return;
		}
		if(t.hasStaticData && !force) {
			if(callback) callback();
			return;
		}
		t.loadingStaticData = true;
		t.hasStaticData = false;
		t.makeServerCall('/client/static_data', null, function(result) {
			if(result && result.status == 'success') {
				t.routes = result.routes;
				t.ratings = result.ratings;
				t.walls = result.walls;

				// create the connections
				var ratingMap = [];
				var wallMap = [];
				for(var i = 0; i < t.ratings.length; ++i) {
					ratingMap[t.ratings[i].id] = t.ratings[i];
				}
				for(var i = 0; i < t.walls.length; ++i) {
					wallMap[t.walls[i].id] = t.walls[i];
				}

				for(var i = 0; i < t.routes.length; ++i) {
					t.routes[i].wall = wallMap[t.routes[i].wall];
					t.routes[i].rating = ratingMap[t.routes[i].rating];
					t.routes[i].height = t.routes[i].height;
					t.routes[i].tall = t.routes[i].height >= 60;
					t.routes[i].trad = t.routes[i].trad == '1';
				}

				t.loadingStaticData = false;
				t.hasStaticData = true;
				if(callback){ callback(true); }
			}
			else {
				alert("Oh No! There was a problem with the app. Please close the app and reopen it.");
				if(callback){ callback(false); }
			}
		});
	},

	saveLocal: function() {
		var saveData = {};

		if(this.hasStaticData && !this.loadingStaticData) {
			saveData.hasStaticData = true;
			saveData.loadingStaticData = false;
			saveData.routes = this.routes;
			saveData.ratings = this.ratings;
			saveData.walls = this.walls;
		}

		if(this.hasOwnProperty('climber')) {
			saveData.climber = this.climber;
		}
		if(this.hasOwnProperty('event')) {
			saveData.event = this.event;
		}
		if(this.hasOwnProperty('team')) {
			saveData.team = this.team;
		}
		if(this.hasOwnProperty('events')) {
			saveData.events = this.events;
		}
		if(this.hasOwnProperty('goals')) {
			saveData.goals = this.goals;
		}
		if(this.hasOwnProperty('goalsList')) {
			saveData.goalsList = this.goalsList;
		}
		if(this.hasOwnProperty('filters')) {
			saveData.filters = this.filters;
		}
		if(this.hasOwnProperty('filtersList')) {
			saveData.filtersList = this.filtersList;
		}
		if(this.hasOwnProperty('lapSyncQueue')) {
			saveData.lapSyncQueue = this.lapSyncQueue;
		}
		if(this.hasOwnProperty('settings')) {
			saveData.settings = this.settings;
		}

		localStorage['mainData'] = JSON.stringify(saveData);
	},
	loadLocal: function() {
		var savedValues = localStorage['mainData'];
		if(savedValues) {
			savedValues = $.parseJSON(savedValues);
			for(var attr in savedValues) {
				this[attr] = savedValues[attr];
			}
		}

		$('body').removeClass('light').removeClass('dark').addClass(window.application.data.settings.theme);
	},

	// These functions are used on multiple pages and I don't want to write them multiple times
	checkForEvents: function() {

		if(!window.application.data.hasOwnProperty('event') || !window.application.data.event.hasOwnProperty('valid') || !window.application.data.event.valid) {
			window.application.data.makeServerCall('/client/get_events', {climber: window.application.data.climber.id}, function(result) {
				if(result && result.status == 'success') {
					var eventsList = result.events;
					window.application.data.events = eventsList;
					window.application.showView("event_select");
				}
				else {
					window.application.showView("event_setup");
				}
			});
		}
		else {
			if(!this.hasOwnProperty('events')) {
				window.application.data.events = [window.application.data.event];
			}
			this.openEvent();
		}
	},
	createEventServer: function() {
		var t = this;
		showLoading();
		var data = window.application.data.event;
		var hasPartner = window.application.data.team.partner.valid && window.application.data.team.partner.id > 0;
		data.partner = hasPartner ? window.application.data.team.partner.id : -1;
		data.climber = window.application.data.climber.id;

		if(!window.application.data.hasOwnProperty('event_setup_view_data') || window.application.data.event_setup_view_data != 'waiting' || window.application.data.event.valid == false) {
			var url = window.application.data.hasOwnProperty('event_setup_view_data') && window.application.data.event_setup_view_data == 'waiting' ? '/client/update_event' : '/client/register_event';
			window.application.data.makeServerCall(url, data, function(result) {
				if(result) {
					if(result.status == 'success') {
						window.application.data.event = result.event;
						window.application.data.event.valid = true;
						window.application.data.event.eventClimber = result.event_climber;
						window.application.data.event.eventClimber.valid = true;
						if(hasPartner) {
							window.application.data.event.eventPartner = result.event_partner;
							window.application.data.event.eventPartner.valid = true;
						}
						else {
							window.application.data.event.eventPartner = {};
							window.application.data.event.eventPartner.valid = false;
						}
						t.createTeamServer();
					}
					else {
						hideLoading();
						alert("There was error contacting the server. Please make sure you are connected to the internet and try again.")
					    debugger;
					}
				}
				else {
					hideLoading();
					alert("There was error contacting the server. Please make sure you are connected to the internet and try again.")
				    debugger;
				}
			});
		}
		else {
			t.createTeamServer();
		}
	},
	createTeamServer: function() {
		var t = this;
		showLoading();

		var data = window.application.data.team;
		var hasPartner = window.application.data.team.partner.valid && window.application.data.team.partner.id > 0;

		data.climber = window.application.data.climber.id;
		data.event = window.application.data.event.id;
		data.partner = hasPartner ? window.application.data.team.partner.id : -1;
		data.name = window.application.data.team.name

		if(!window.application.data.hasOwnProperty('event_setup_view_data') || window.application.data.event_setup_view_data != 'waiting' || window.application.data.team.valid == false) {
			var url = window.application.data.hasOwnProperty('event_setup_view_data') && window.application.data.event_setup_view_data == 'waiting' ? '/client/update_team' : '/client/register_team';
			window.application.data.makeServerCall(url, data, function(result) {
				if(result) {
					if(result.status == 'success') {
						window.application.data.team = result.team;
						window.application.data.team.valid = true;
						window.application.data.saveLocal();

						if(window.application.data.next_view_data == 'event') {
							window.application.closeView('event_setup');
						}
						else {
							window.application.data.openEvent();
						}
					}
					else {
						hideLoading();
						alert("There was error contacting the server. Please make sure you are connected to the internet and try again.")
					    debugger;
					}
				}
				else {
					hideLoading();
				}
			});
		}
		else {
			window.application.data.saveLocal();
			window.application.data.openEvent();
		}
	},

	openEvent: function() {
		var t = this;
		var event = window.application.data.event;
		if(!event) {
			alert("There was a unexpected error. Please try again.");
		    debugger;
		    return;
		}
		var start = new Date(event.start_time);
		var end = new Date(event.end_time);

		if(end < new Date()) {
			// past
			window.application.data.event_state = 'past';
			window.application.showView("event_main");
		}
		else if(start > new Date()) {
			// future
			window.application.data.event_state = 'future';
			window.application.showView("event_waiting");
		}
		else {
			// current
			window.application.data.event_state = 'current';
			window.application.showView("event_main");
		}
	},

	lapSyncQueue: [],
	addLap: function(route, pinkpoint, lap, time) {
		var data = {
			climber: window.application.data.climber.id,
			event: window.application.data.event.id,
			route: route.id,
			pink_point: pinkpoint ? 1 : 0,
			send_time: time,
			lap: lap,
		};
		window.application.data.makeServerCall('/client/add_lap', data, function(result) {
			if(result && result.status == 'success') {
				
			}
			else {
				// failed so add the lap to the unsynced queue
				data.action = 'add';
				window.application.data.lapSyncQueue[window.application.data.lapSyncQueue.length] = data;
				window.application.data.saveLocal();
				hideLoading();
			}
		});
	},
	removeLap: function(route, pinkpoint, lap) {
		var data = {
			climber: window.application.data.climber.id,
			event: window.application.data.event.id,
			route: route.id,
			pink_point: pinkpoint ? 1 : 0,
			lap: lap,
		};
		window.application.data.makeServerCall('/client/remove_lap', data, function(result) {
			if(result && result.status == 'success') {
				
			}
			else {
				// failed so remove the lap to the unsynced queue
				data.action = 'remove';
				// first, check to see if this route has been synced yet, if not then just remove it from here
				var found = false;
				for(var i in window.application.data.lapSyncQueue) {
					if(	window.application.data.lapSyncQueue[i].route == data.route &&
						window.application.data.lapSyncQueue[i].pink_point == data.pink_point &&
						window.application.data.lapSyncQueue[i].lap == data.lap &&
						window.application.data.lapSyncQueue[i].action == 'add') {
						window.application.data.lapSyncQueue[i] = null;
						found = true;
					}
				}
				if(!found) {
					window.application.data.lapSyncQueue[window.application.data.lapSyncQueue.length] = data;
				}
				window.application.data.saveLocal();
				hideLoading();
			}
		});
	},
	updateLap: function(route, pinkpoint, lap, time) {
		var data = {
			climber: window.application.data.climber.id,
			event: window.application.data.event.id,
			route: route.id,
			pink_point: pinkpoint ? 1 : 0,
			send_time: time,
			lap: lap,
		};
		window.application.data.makeServerCall('/client/update_lap', data, function(result) {
			if(result && result.status == 'success') {
				
			}
			else {
				// failed so update the lap to the unsynced queue
				data.action = 'update';
				// first, check to see if this route has been synced yet, if not then just update it from here
				var found = false;
				for(var i in window.application.data.lapSyncQueue) {
					if(	window.application.data.lapSyncQueue[i].route == data.route &&
						window.application.data.lapSyncQueue[i].pink_point == data.pink_point &&
						window.application.data.lapSyncQueue[i].lap == data.lap &&
						window.application.data.lapSyncQueue[i].action == 'add') {
						window.application.data.lapSyncQueue[i].send_time = data.send_time;
						found = true;
					}
				}
				if(!found) {
					window.application.data.lapSyncQueue[window.application.data.lapSyncQueue.length] = data;
				}
				window.application.data.saveLocal();
				hideLoading();
			}
		});
	},

	syncLaps: function() {

		if(window.application.data.lapSyncQueue.length == 0) {
			return;
		}

		var data = {
			climber: window.application.data.climber.id,
			event: window.application.data.event.id,
			laps: JSON.stringify(window.application.data.lapSyncQueue),
		};

		window.application.data.makeServerCall('/client/sync_laps', data, function(result) {
			if(result) {
				if(result.status == 'success') {
					window.application.data.lapSyncQueue = result.unsynced;
					window.application.data.saveLocal();
				}
				else {
					alert("There was a problem syncing your laps. The server responded with " + result.status);
				    debugger;
				}
			}
		});
	},

	updateGoalsProgress: function() {
		var start_time = new Date(window.application.data.event.start_time);
        var end_time = new Date(window.application.data.event.end_time);
        if(new Date() < end_time) {
            end_time = new Date();
        }

        end_time.setMinutes(end_time.getMinutes() + 59);
        end_time.setSeconds(end_time.getSeconds() + 59);

        var hourly_data = [];

        var hour = 0;
        var laps = window.application.data.event.laps;
        var routes = window.application.data.routes;
        var ratings = window.application.data.ratings;
        var lastTime = new Date(0);
        for(var time = start_time; time <= end_time; time.setHours(time.getHours() + 1)) {

            hourly_data[hour] = {routes:0, total_routes:0, score:0, total_score:0, rating:0, total_rating:0, trad:0, total_trad:0, height:0, total_height:0};

            // copy previous data
            if(hour > 0)
            {
                hourly_data[hour].total_routes = hourly_data[hour - 1].total_routes;
                hourly_data[hour].total_score = hourly_data[hour - 1].total_score;
                hourly_data[hour].total_rating = hourly_data[hour - 1].total_rating;
                hourly_data[hour].total_trad = hourly_data[hour - 1].total_trad;
                hourly_data[hour].total_height = hourly_data[hour - 1].total_height;
            }

            for(var i in laps) {
                if(typeof laps[i] === 'undefined' || !laps[i]) {
                    continue;
                }

                var route = null;
                for(var r in routes) {
                    if(routes[r].id == i) {
                        route = routes[r];
                        break;
                    }
                }
                if(!route) {
                    continue;
                }


                for(var l = 0; l < 2; l++) {
                    var lap = l == 0 ? laps[i].first : laps[i].second;
                    if(!lap.sent) {
                        continue;
                    }
                    var sent_time = new Date(lap.time.replace("T", " "));
                    // only include things from this hour but be sure to include thins before and after the event in the first and last hours
                    if(sent_time <= lastTime || (hour < parseInt(window.application.data.event.event_length) && sent_time > time)) {
                        continue
                    }
                    var originalRating = parseInt(route.rating.id);
                    var pointsRating = originalRating;
                    var points = parseInt(route.rating.points);
                    if(route.height){
                        pointsRating += 1;
                    }
                    if((route.trad === true || route.trad == '1') && !lap.pink_point) {
                        pointsRating += 1;
                    }
                    if(originalRating != pointsRating) {
                        for(var r in ratings) {
                            if(ratings[r].id == pointsRating) {
                                points = parseInt(ratings[r].points);
                                break;
                            }
                        }
                    }

                    hourly_data[hour].routes += 1;
                    hourly_data[hour].score += points;
                    hourly_data[hour].rating += originalRating;
                    hourly_data[hour].trad += route.trad ? 1 : 0;
                    hourly_data[hour].height += route.height;

                    hourly_data[hour].total_routes += 1;
                    hourly_data[hour].total_score += points;
                    hourly_data[hour].total_rating += originalRating;
                    hourly_data[hour].total_trad += route.trad ? 1 : 0;
                    hourly_data[hour].total_height += route.height;
                }
            }

            ++hour;
            lastTime = new Date(time);
        }

        // calculate composite stats
        for(var i in hourly_data) {
            if(hourly_data[i].routes > 0) {
                hourly_data[i].rating = Math.round(hourly_data[i].rating / hourly_data[i].routes);
                hourly_data[i].points_per_route = hourly_data[i].score / hourly_data[i].routes;
            }
            else {
                hourly_data[i].rating = 0;
                hourly_data[i].points_per_route = 0;
            }
            if(hourly_data[i].total_routes > 0) {
                hourly_data[i].total_rating = Math.round(hourly_data[i].total_rating / hourly_data[i].total_routes);
                hourly_data[i].total_points_per_route = hourly_data[i].total_score / hourly_data[i].total_routes;
            }
            else {
                hourly_data[i].total_rating = 0;
                hourly_data[i].total_points_per_route = 0;
            }
        }
        window.application.data.hourlyData = hourly_data;

        // get the lap data
        var lap_data = [];

        for(var i in laps) {
            if(typeof laps[i] === 'undefined' || !laps[i]) {
                continue;
            }

            var route = null;
            for(var r in routes) {
                if(routes[r].id == i) {
                    route = routes[r];
                    break;
                }
            }
            if(!route) {
                continue;
            }


            for(var l = 0; l < 2; l++) {
                var lap = l == 0 ? laps[i].first : laps[i].second;
                if(!lap.sent) {
                    continue;
                }

                var sent_time = new Date(lap.time.replace("T", " "));
                var originalRating = parseInt(route.rating.id);
                var pointsRating = originalRating;
                var points = parseInt(route.rating.points);
                if(route.height){
                    pointsRating += 1;
                }
                if((route.trad === true || route.trad == '1') && !lap.pink_point) {
                    pointsRating += 1;
                }
                if(originalRating != pointsRating) {
                    for(var r in ratings) {
                        if(ratings[r].id == pointsRating) {
                            points = parseInt(ratings[r].points);
                            break;
                        }
                    }
                }

                lap_data[lap_data.length] = {
                    lap: lap_data.length,
                    name: route.name + (lap.pink_point ? ' PP' : '') + (route.tall ? '*' : ''),
                    rating: route.rating.name,
                    score: points,
                    sent_time: sent_time,
                };
            }
        }

        lap_data.sort(function(a, b) {
            if(a.sent_time < b.sent_time) return -1;
            if(a.sent_time > b.sent_time) return 1;
            return 0;
        });

        window.application.data.lapData = lap_data;


        // update the event ui
		var goalsSet = 0;
		var goalsProgress = 0;
		if(window.application.data.hasOwnProperty('goals') && window.application.data.settings.progress != 'text') {
	        if(window.application.data.settings.progress == 'goals') {
				var goals = window.application.data.goals;
				var data = window.application.data.hourlyData[window.application.data.hourlyData.length - 1];
				if(goals.personal.score > 0) { 
					var percent = ((data.total_score / goals.personal.score) * 100);
					goalsSet += 1;
					goalsProgress += percent;
				}
				if(goals.personal.laps > 0) { 
					var percent = ((data.total_routes / goals.personal.laps) * 100);
					goalsSet += 1;
					goalsProgress += percent;
				}
				if(goals.personal.height > 0) { 
					var percent = ((data.total_height / goals.personal.height) * 100);
					goalsSet += 1;
					goalsProgress += percent;
				}
				if(goalsSet == 0) {
					if(goals.personal.rating > 0) { 
						var percent = ((data.total_rating / goals.personal.rating) * 100);
						goalsSet += 1;
						goalsProgress += percent;
					}
					if(goals.personal.lapsPerHour > 0) { 
						var value = data.total_routes / (window.application.data.hourlyData.length > 1 ? window.application.data.hourlyData.length - 1 : window.application.data.hourlyData.length);
						var percent = ((value / goals.personal.lapsPerHour) * 100);
						goalsSet += 1;
						goalsProgress += percent;
					}
					if(goals.personal.scorePerHour > 0) { 
						var value = data.total_score / (window.application.data.hourlyData.length > 1 ? window.application.data.hourlyData.length - 1 : window.application.data.hourlyData.length);
						var percent = ((value / goals.personal.scorePerHour) * 100);
						goalsSet += 1;
						goalsProgress += percent;
					}
				}
			}
			else if(window.application.data.settings.progress == 'score') {
				var goals = window.application.data.goals;
				var data = window.application.data.hourlyData[window.application.data.hourlyData.length - 1];
				if(goals.personal.score > 0) { 
					var percent = ((data.total_score / goals.personal.score) * 100);
					goalsSet += 1;
					goalsProgress = percent;
				}
			}
			else if(window.application.data.settings.progress == 'laps') {
				var goals = window.application.data.goals;
				var data = window.application.data.hourlyData[window.application.data.hourlyData.length - 1];
				if(goals.personal.laps > 0) { 
					var percent = ((data.total_routes / goals.personal.laps) * 100);
					goalsSet += 1;
					goalsProgress = percent;
				}
			}
		}
			
		if(goalsSet == 0) {

			var data = window.application.data.hourlyData[window.application.data.hourlyData.length - 1];

			$('.event-header .goal-progress').css({'border': 'none'});
			$('.event-header .goal-progress .goal-progress-bar').css({'display': 'none'});
			$('.event-header .goal-progress .goal-progress-text').html("<div style='display:inline-block;width:30%;'>Score: " + data.total_score + "</div><div style='display:inline-block;width:30%;'>Laps: " + data.total_routes + "</div>");
		}
		else  {
			$('.event-header .goal-progress').show();
			$('.event-header .goal-progress').css({'border': ''});
			$('.event-header .goal-progress .goal-progress-bar').css({'display': 'block'});
			goalsProgress /= goalsSet;
			if(goalsProgress >= 99) {
				$('.event-header .goal-progress .goal-progress-bar').css({'border-bottom-right-radius': '.5em', 'border-top-right-radius': '.5em'});
			}
			else {
				$('.event-header .goal-progress .goal-progress-bar').css({'border-bottom-right-radius': '0', 'border-top-right-radius': '0'});
			}

			if(goalsProgress <= 100) {
				goalsProgress = goalsProgress.toFixed(2) + '%';
				$('.event-header .goal-progress .goal-progress-bar').css('width', goalsProgress);
				$('.event-header .goal-progress .goal-progress-text').html('Goal Progress: ' + goalsProgress);
			}
			else {
				goalsProgress = goalsProgress.toFixed(2) + '%';
				$('.event-header .goal-progress .goal-progress-bar').css('width', '100%');
				$('.event-header .goal-progress .goal-progress-text').html('Goal Progress: ' + goalsProgress);
			}
			$('.event-header .goal-progress .goal-progress-text').html('Goal Progress: ' + goalsProgress);
		} 
	},
};
var EventSetupModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'EventSetupModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var EventSetupView = {
	link: 'mobile/page/event_setup',

	init: function() {
		this.name = 'event_setup';
		this.initBase();
	},

	events: [
		{selector: '#event_setup-submit', event: 'click', handler: "onSubmit"},
		{selector: '#event_setup-back', event: 'click', handler: "onBack"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.next_view_data == 'event') {
			t.element.css({'z-index': 9000});
			showOverlay();
		}
	},
	onShow: function() {
		if(window.application.data.hasOwnProperty('event')) {
			var event = window.application.data.event;
			this.element.find('#length').val(event.event_length);
			var d = new Date(event.start_time);
			this.element.find('#start_date').val(d.getFullYear() + "-" + (d.getMonth() < 9 ? '0' : '') + (d.getMonth() + 1) + "-" + (d.getDate() < 10 ? '0' : '') + d.getDate());
			var l = d.getHours();
			this.element.find('#start_time').val(((l < 10 ? '0' : '') + l) + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes());
			this.element.find('#is_public').val(event.is_public == '1' || event.is_public == 'true' ? 'yes' : 'no');

			if(d <= new Date() || event.is_official == '1' || event.is_official == 'true') {
				this.element.find('#length').prop('disabled', true);
				this.element.find('#start_date').prop('disabled', true);
				this.element.find('#start_time').prop('disabled', true);
				if(event.is_official == '1' || event.is_official == 'true') {
					this.element.find('#is_public').prop('disabled', true);
				}
				this.element.find('#team_name').prop('disabled', true);
				this.element.find('#partner_first_name').prop('disabled', true);
				this.element.find('#partner_last_name').prop('disabled', true);
			}
		}
		if(window.application.data.hasOwnProperty('team')) {
			var team = window.application.data.team;
			this.element.find('#team_name').val(team.name);
			this.element.find('#partner_first_name').val(team.partner.first_name);
			this.element.find('#partner_last_name').val(team.partner.last_name);
		}

		if(window.application.data.next_view_data == 'waiting' || window.application.data.next_view_data == 'event') {
			this.element.find('.subtitle').text('You can change your event settings below.');
		}
		else {
			// This is not the first time loading the app but they don't have any events in the database
			if( (!window.application.data.climber.hasOwnProperty('brand_new') || !window.application.data.climber.brand_new) &&
				(!window.application.data.hasOwnProperty('events') || window.application.data.climber.events.length == 0)) {
				this.element.find('.subtitle').text('We couldn\'t find and events you are a part of. Please fill out the information below to create a practice event.');
			}

			// This climber has events but wants to create a new one
			if( window.application.data.hasOwnProperty('events') && window.application.data.climber.events.length >= 0) {
				this.element.find('.subtitle').text('Please fill in the information below to create your new practice event.');
			}
		}
	},
	onHide: function() {
		hideOverlay();
	},
	onSubmit: function() {
		var t = EventSetupView;
		if(window.application.data.hasOwnProperty('event')) {
			var event = window.application.data.event;
			var d = new Date(event.start_time);

			if(d <= new Date() || event.is_official == '1' || event.is_official == 'true') {
				if(window.application.data.next_view_data == 'event') {
					window.application.closeView('event_setup');
				}
				else {
					window.application.data.openEvent();
				}
				return;
			}
		}

		// validate the data
		var length = t.element.find('#length').val().trim();
		var start_date = t.element.find('#start_date').val().trim(); // yyyy-mm-dd
		var start_time = t.element.find('#start_time').val().trim();
		var team_name = t.element.find('#team_name').val().trim();
		var partner_first_name = t.element.find('#partner_first_name').val().trim();
		var partner_last_name = t.element.find('#partner_last_name').val().trim();
		var is_public = t.element.find('#is_public').val() == 'yes' ? '1' : '0';
		var errors = "";
		if(length == null || length < 4 || length > 48) {
			errors = "Event length between 4 and 48 hours.";
			t.element.find('#length').addClass('error');
			t.element.find('#length').on('focus', function() {
				t.element.find('#length').off('focus');
				t.element.find('#length').removeClass('error');
			});
		}
		if(start_date.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide an event start date.";
			t.element.find('#start_date').addClass('error');
			t.element.find('#start_date').on('focus', function() {
				t.element.find('#start_date').off('focus');
				t.element.find('#start_date').removeClass('error');
			});
		}
		if(start_time.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide an event start time.";
			t.element.find('#start_time').addClass('error');
			t.element.find('#start_time').on('focus', function() {
				t.element.find('#start_time').off('focus');
				t.element.find('#start_time').removeClass('error');
			});
		}

		if(start_date.length > 0 && start_time.length > 0 && new Date(start_date + ' ' + start_time) < new Date(new Date().setHours(new Date().getHours() - 1))) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Event start must be in the future.";
			t.element.find('#start_date').addClass('error');
			t.element.find('#start_date').on('focus', function() {
				t.element.find('#start_date').off('focus');
				t.element.find('#start_date').removeClass('error');
			});
			t.element.find('#start_time').addClass('error');
			t.element.find('#start_time').on('focus', function() {
				t.element.find('#start_time').off('focus');
				t.element.find('#start_time').removeClass('error');
			});
		}

		if(team_name.length == 0) {
			if(partner_last_name.length == 0) {
				team_name = "Team " + window.application.data.climber.first_name + " " + window.application.data.climber.last_name;
			}
			else {
				team_name = "Team " + window.application.data.climber.last_name + " " + partner_last_name;
			}
			if(!confirm("You did not provide a team name. Your team name has been set to \"" + team_name + "\". Is that OK?")) {
				return;
			}
		}

		if(partner_first_name.length == 0 && partner_last_name.length == 0) {
			if(!confirm("You did not provide a partner name. Are you sure you want to participate in this event without a partner?")) {
				return;
			}
		}
		else if(partner_first_name.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide your partner's first and last names";
			t.element.find('#partner_first_name').addClass('error');
			t.element.find('#partner_first_name').on('focus', function() {
				t.element.find('#partner_first_name').off('focus');
				t.element.find('#partner_first_name').removeClass('error');
			});
		}
		else if(partner_last_name.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide your partner's first and last names";
			t.element.find('#partner_last_name').addClass('error');
			t.element.find('#partner_last_name').on('focus', function() {
				t.element.find('#partner_last_name').off('focus');
				t.element.find('#partner_last_name').removeClass('error');
			});
		}

		if(errors.length > 0) {
			showError(errors);
			return;
		}

		var start = new Date(new Date(start_date + 'T' + start_time).getTime() + (new Date().getTimezoneOffset() * 60 * 1000));
		var end_date = new Date(start);
		end_date.setHours(start.getHours() + parseInt(length));
		if(window.application.data.next_view_data == 'waiting') {

			// see if anything changed
			if(	window.application.data.event.start_time != start.toISOString() ||
				window.application.data.event.event_length != length ||
				window.application.data.event.is_public != is_public) {

				window.application.data.event.valid = false;
				window.application.data.event.start_time = start.toISOString();
				window.application.data.event.end_time = end_date.toISOString();
				window.application.data.event.year = start_date.substring(0, 4);
				window.application.data.event.event_length = length;
				window.application.data.event.is_public = is_public;
			}
			else {
				window.application.data.event.valid = true;
			}

			if(	window.application.data.team.name != team_name ||
				window.application.data.team.partner.first_name != partner_first_name ||
				window.application.data.team.partner.last_name != partner_last_name) {

				window.application.data.team.valid = false;
				window.application.data.team.name = team_name;
				window.application.data.team.partner.valid = false;
				window.application.data.team.partner.first_name = partner_first_name;
				window.application.data.team.partner.last_name = partner_last_name;
			}
			else {
				window.application.data.team.valid = true;
			}

			// nothing changed
			if(window.application.data.event.valid && window.application.data.team.valid) {
				window.application.data.openEvent();
			}
		}
		else {
			window.application.data.event = {
				valid: false,
				start_time: start.toISOString(),
				end_time: end_date.toISOString(),
				year: start_date.substring(0, 4),
				event_length: length,
				editable: true,
				is_official: false,
				is_public: is_public,
			};
			window.application.data.team = {
				valid: false,
				name: team_name,
				partner: {
					valid: false,
					first_name: partner_first_name,
					last_name: partner_last_name,
				}
			};
		}

		window.application.data.event_setup_view_data = window.application.data.next_view_data;

		// if there is a partner then make sure they exist on the server
		if((window.application.data.team.partner.first_name.length > 0 || window.application.data.team.partner.last_name.length > 0) && window.application.data.team.valid == false) {
			window.application.data.makeServerCall('/client/find_climber', {first_name: partner_first_name, last_name: partner_last_name}, function(result) {
				if(result) {
					if(result.status == 'no_match') {
						if(confirm("We couldn't find your partner's account. Do you want to create one for them? (If you say no, your partner's score won't be tracked.)")) {
							window.application.data.next_view_data = 'partner';
							window.application.showView("create_user");
						}
						else {
							window.application.data.createEventServer();
						}
					}
					else if(result.status == 'success' && result.climbers.length == 1) {
						// Save the event and team
						window.application.data.team.partner = result.climbers[0];
						window.application.data.team.partner.valid = true;
						window.application.data.createEventServer();
					}
					else if(result.status == 'success') {
						window.application.data.next_view_data = 'partner';
						window.application.data.climbers = result.climbers;
						window.application.showView("climber_select");
					}
				}
				else {
					hideLoading();
				}
			});
		}
		else {
			window.application.data.createEventServer();
		}
	},
	onBack: function() {
		var t = this;
		if(window.application.data.next_view_data == 'waiting') {
			window.application.data.openEvent();
		}
		else if(window.application.data.next_view_data == 'event') {
			window.application.closeView('event_setup');
		}
		else if(window.application.data.hasOwnProperty('events')) {
			window.application.showView("event_select");
		}
		else {
			window.application.showView("welcome");
		}
	}
};
EventSetupView = mergeObjects(View, EventSetupView);
EventSetupModel = mergeObjects(Model, EventSetupModel);

window.application.loadedViews['event_setup'] = EventSetupView;
window.application.loadedModels['event_setup'] = EventSetupModel;
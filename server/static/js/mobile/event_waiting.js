var EventWaitingModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'EventWaitingModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var EventWaitingView = {
	link: 'mobile/page/event_waiting',
	timer: null,

	init: function() {
		this.name = 'event_waiting';
		this.initBase();
	},

	events: [
		{selector: '#event_waiting-goals', event: 'click', handler: "onGoals"},
		{selector: '#event_waiting-routes', event: 'click', handler: "onRoutes"},
		{selector: '#event_waiting-settings', event: 'click', handler: "onSettings"},
		{selector: '#event_waiting-back', event: 'click', handler: "onBack"},
		{selector: '#event_waiting-logout', event: 'click', handler: "onLogout"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.hasOwnProperty('event')) {
			var event = window.application.data.event;
			var start = new Date(event.start_time);
			var startTime = start.getTime();
			t.element.find('.countdown').html(timeUntil(startTime));
			t.timer = setInterval(function() {
				if(startTime <= new Date().getTime()) {
					clearInterval(t.timer);
					t.timer = null;
					t.element.find('.countdown-wrapper').empty();
					window.application.data.event_state = 'current';
					var startButton = $("<div id='event_waiting-start' class='button button-large'>Start Event</div>");
					startButton.on('click', function() {
						window.application.data.openEvent();
						window.application.closeView("event_waiting");
					});
					t.element.find('.countdown-wrapper').append(startButton);
				}
				else {
					t.element.find('.countdown').html(timeUntil(startTime));
				}
			}, 1000); // every second

			t.element.find('.title').html(event.name);
			t.element.find('.team .value').html(window.application.data.team.name);
			if(window.application.data.team.hasOwnProperty('partner') && window.application.data.team.partner.hasOwnProperty('first_name')) {
				t.element.find('.partner .value').html(window.application.data.team.partner.first_name + " " + window.application.data.team.partner.last_name);
			}
			else {
				t.element.find('.partner .value').html('None');
			}

			t.element.find('.category .value').html(event.category == 'rec' ? 'Recreational' : event.category == 'int' ? 'Intermediate' : event.category == 'adv' ? 'Advanced' : 'Elite');

			t.element.find('.length .value').html(event.event_length);
			t.element.find('.climbers .value').html(event.participants);

			if(event.is_official == '1') {
				t.element.find('#event_waiting-settings').addClass('disabled');
			}
		}
		else {
			alert('Something has gone terribly, horribly wrong!');
		}
	},
	onHide: function() {
		if(this.timer) {
			clearInterval(this.timer);
			this.timer = null;
		}
	},
	onGoals: function() {
		window.application.showView("goals_settings");
	},
	onRoutes: function() {
		window.application.showView("filter_settings");
	},
	onSettings: function() {
		if(window.application.data.event.is_official != '1') {
			window.application.data.next_view_data = 'waiting';
			window.application.showView("event_setup");
		}
		else {
			showError('This is an official event and the event settings cannot be changed. Please contact the event coordinators if you would like to change your team name or partner.');
		}
	},
	onAppSettings: function() {

	},
	onBack: function() {
		window.application.showView("event_select");
	},
	onLogout: function() {
		var t = EventWaitingView;
		///if(window.application.dsata.next_view_data == 'main') {
			delete window.application.data.climber;
			delete window.application.data.events;
			window.application.data.saveLocal();
			window.application.showView("welcome");
		//}
	}
};

EventWaitingView = mergeObjects(View, EventWaitingView);
EventWaitingModel = mergeObjects(Model, EventWaitingModel);

window.application.loadedViews['event_waiting'] = EventWaitingView;
window.application.loadedModels['event_waiting'] = EventWaitingModel;
var GoalSettingsModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'GoalSettingsModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var GoalSettingsView = {
	link: 'mobile/page/goals_settings',

	init: function() {
		this.name = 'goals_settings';
		this.initBase();
	},

	events: [
		{selector: '#goals_settings-submit', event: 'click', handler: "onSubmit"},
		{selector: '#goals_settings-cancel', event: 'click', handler: "onBack"},
		{selector: '#goals_settings-wrapper .tab-button', event: 'click', handler: "onTabSelect"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.next_view_data == 'event') {
			t.element.css({'z-index': 9000});
			showOverlay();
		}

		if(window.application.data.hasOwnProperty('goals')) {
			var goals = window.application.data.goals;

			if(goals.personal.score > 0) { t.element.find('#personal-score').val(goals.personal.score); }
			if(goals.personal.laps > 0) { t.element.find('#personal-laps').val(goals.personal.laps); }
			if(goals.personal.height > 0) { t.element.find('#personal-height').val(goals.personal.height); }
			if(goals.personal.rating > 0) { t.element.find('#personal-rating').val(goals.personal.rating); }
			if(goals.personal.lapsPerHour > 0) { t.element.find('#personal-laps-per-hour').val(goals.personal.lapsPerHour); }
			if(goals.personal.scorePerHour > 0) { t.element.find('#personal-score-per-hour').val(goals.personal.scorePerHour); }

			if(goals.team.score > 0) { t.element.find('#team-score').val(goals.team.score); }
			if(goals.team.laps > 0) { t.element.find('#team-laps').val(goals.team.laps); }
			if(goals.team.height > 0) { t.element.find('#team-height').val(goals.team.height); }
			if(goals.team.rating > 0) { t.element.find('#team-rating').val(goals.team.rating); }
			if(goals.team.lapsPerHour > 0) { t.element.find('#team-laps-per-hour').val(goals.team.lapsPerHour); }
			if(goals.team.scorePerHour > 0) { t.element.find('#team-score-per-hour').val(goals.team.scorePerHour); }
		}

		var ratings = window.application.data.ratings;
		for(var i in ratings) {
			var o = $('<option value="' + ratings[i].id + '"></option>').html(ratings[i].name);
			$('#personal-rating').append(o);
			$('#team-rating').append(o.clone());
		}
	},
	onHide: function() {
		hideOverlay();
	},
	onTabSelect: function(e) {
		var s = $(e.currentTarget);
		$('#goals_settings-wrapper .tab-button').removeClass('selected');
		$('#goals_settings-wrapper .form-tab').removeClass('selected');

		s.addClass('selected');
		$('#goals_settings-wrapper #' + s.data('tab')).addClass('selected');
	},
	onSubmit: function() {
		var t = GoalSettingsView;

		var goals = {
			personal: {},
			team: {},
		};
		
		goals.personal.score = t.element.find('#personal-score').val().length == 0 ? 0 : parseInt(t.element.find('#personal-score').val());
		goals.personal.laps = t.element.find('#personal-laps').val().length == 0 ? 0 : parseInt(t.element.find('#personal-laps').val());
		goals.personal.height = t.element.find('#personal-height').val().length == 0 ? 0 : parseInt(t.element.find('#personal-height').val());
		goals.personal.rating = t.element.find('#personal-rating').val();
		goals.personal.lapsPerHour = t.element.find('#personal-laps-per-hour').val().length == 0 ? 0 : parseInt(t.element.find('#personal-laps-per-hour').val());
		goals.personal.scorePerHour = t.element.find('#personal-score-per-hour').val().length == 0 ? 0 : parseInt(t.element.find('#personal-score-per-hour').val());
		goals.team.score = t.element.find('#team-score').val().length == 0 ? 0 : parseInt(t.element.find('#team-score').val());
		goals.team.laps = t.element.find('#team-laps').val().length == 0 ? 0 : parseInt(t.element.find('#team-laps').val());
		goals.team.height = t.element.find('#team-height').val().length == 0 ? 0 : parseInt(t.element.find('#team-height').val());
		goals.team.rating = t.element.find('#team-rating').val();
		goals.team.lapsPerHour = t.element.find('#team-laps-per-hour').val().length == 0 ? 0 : parseInt(t.element.find('#team-laps-per-hour').val());
		goals.team.scorePerHour = t.element.find('#team-score-per-hour').val().length == 0 ? 0 : parseInt(t.element.find('#team-score-per-hour').val());

		if(!window.application.data.hasOwnProperty('goalsList')) {
			window.application.data.goalsList = [];
		}
		window.application.data.goalsList[window.application.data.event.id] = goals;
		window.application.data.goals = goals;
		window.application.data.saveLocal();

		if(window.application.data.next_view_data == 'event') {
			window.application.data.updateGoalsProgress();
			window.application.closeView('goals_settings');
		}
		else {
			window.application.data.openEvent();
		}
	},
	onBack: function() {
		if(window.application.data.next_view_data == 'event') {
			window.application.closeView('goals_settings');
		}
		else {
			window.application.data.openEvent();
		}
	},
};

GoalSettingsView = mergeObjects(View, GoalSettingsView);
GoalSettingsModel = mergeObjects(Model, GoalSettingsModel);

window.application.loadedViews['goals_settings'] = GoalSettingsView;
window.application.loadedModels['goals_settings'] = GoalSettingsModel;
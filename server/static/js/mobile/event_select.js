var EventSelectModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'EventSelectModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var EventSelectView = {
	link: 'mobile/page/event_select',
	timers: [],

	init: function() {
		this.name = 'event_select';
		this.initBase();
	},

	events: [
		{selector: '.event-summary', event: 'click', handler: "onSelected"},
		{selector: '#event_select-submit', event: 'click', handler: "onSubmit"},
		{selector: '#event_select-new', event: 'click', handler: "onNew"},
		{selector: '#event_select-logout', event: 'click', handler: "onBack"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.hasOwnProperty('events')) {
			var events = window.application.data.events;
			
			var template = t.element.find('.event-summary');
			for(var i = events.length - 1; i >= 0; --i)
			{
				var e = template.clone();
				var start = new Date(events[i].start_time.replace(/\s/, 'T'));
				var end = new Date(events[i].end_time.replace(/\s/, 'T'));

				e.data('event', events[i].id);
				e.find('.event-name').html(events[i].name);
				e.find('.event-date').html('');

				// check for event times
				var official = events[i].is_official == 1 ? 'official' : 'practice';
				if(end < new Date()) {
					e.find('.event-date').html(start.toDateString() + ' ' + start.toLocaleTimeString());
					t.element.find('#event-past-' + official).append(e);
				}
				else if(start > new Date()) {
					var startTime = start.getTime();
					e.find('.event-date').html('Starts in ' + timeUntil(startTime));
					if((startTime - new Date().getTime()) < (1000 * 60 * 60 * 24 * 2)) // if it's less than 2 days then update the timer
					{
						var timer = setInterval(function() {
							if(startTime <= (new Date().getTime())) {
								e.find('.event-date').html('Started');
							}
							else {
								e.find('.event-date').html('Starts in ' + timeUntil(startTime));
							}
						}, 1000); // every second
						t.timers.push(timer);
					}
					t.element.find('#event-future-' + official).append(e);
				}
				else {
					var endTime = end.getTime();
					e.find('.event-date').html('Time Left: ' + timeUntil(endTime));
					if((endTime - new Date().getTime()) < (1000 * 60 * 60 * 24 * 2)) // if it's less than 2 days then update the timer
					{
						var timer = setInterval(function() {
							if(endTime <= (new Date().getTime())) {
								e.find('.event-date').html('Ended');
							}
							else {
								e.find('.event-date').html('Time Left: ' + timeUntil(endTime));
							}
						}, 1000); // every second
						t.timers.push(timer);
					}
					t.element.find('#event-current-' + official).append(e);
				}
			}
			template.remove()

			if(t.element.find('#event-past-official').children().length == 1) t.element.find('#event-past-official').remove();
			if(t.element.find('#event-past-practice').children().length == 1) t.element.find('#event-past-practice').remove();
			if(t.element.find('#event-future-official').children().length == 1) t.element.find('#event-future-official').remove();
			if(t.element.find('#event-future-practice').children().length == 1) t.element.find('#event-future-practice').remove();
			if(t.element.find('#event-current-official').children().length == 1) t.element.find('#event-current-official').remove();
			if(t.element.find('#event-current-practice').children().length == 1) t.element.find('#event-current-practice').remove();
		}
		else {
			alert('Something has gone terribly, horribly wrong!');
		}
	},
	onHide: function() {
		for(var timer in this.timers) {
			clearInterval(timer);
		}
		timers = [];
	},
	onSelected: function(e) {
		var t = EventSelectView;
		t.element.find('.event-summary').removeClass('selected');
		$(e.currentTarget).addClass('selected');
	},
	onSubmit: function() {
		var t = EventSelectView;

		// validate the data
		var selected = t.element.find('.event-summary.selected');
		var event = null;
		var errors = "";
		if(selected.length == 0) {
			errors = "You must select an event to continue.";
		}
		else {
			event = selected.data('event');
			if(event == null || event <= 0) {
				errors = "You selected an invalid event, please pick a different one.";
			}
		}
		if(errors.length > 0) {
			showError(errors);
			return;
		}

		// make sure to save the current event
		if(window.application.data.hasOwnProperty('event')) {
			for(var i = 0; i < window.application.data.events.length; ++i) {
				if(window.application.data.events[i].id == window.application.data.event.id) {
					window.application.data.events[i] = window.application.data.event;
					delete window.application.data.event;
					break;
				}
			}
		}
		if(window.application.data.hasOwnProperty('event')) {
			window.application.data.events[window.application.data.events.length] = window.application.data.event;
			delete window.application.data.event;
		}

		for(var i = 0; i < window.application.data.events.length; ++i) {
			if(window.application.data.events[i].id == event) {
				event = window.application.data.events[i];
				event.valid = true;
				break;
			}
		}

		if(typeof(event) != 'object' || !event.hasOwnProperty('valid') || !event.valid) {
			showError("The event you selected doesn't exist!");
			return;
		}

		//if(window.application.data.next_view_data == 'main') {
			window.application.data.event = event;
			window.application.data.team = event.team;
			window.application.data.team.partner = event.partner;
			if(window.application.data.hasOwnProperty('goalsList')) {
				if(window.application.data.goalsList[event.id]) {
					window.application.data.goals = window.application.data.goalsList[event.id];
				}
				else {
					delete window.application.data.goals;
				}
			}
			else {
				delete window.application.data.goals;
			}
			if(window.application.data.hasOwnProperty('filtersList')) {
				if(window.application.data.filtersList[event.id]) {
					window.application.data.filters = window.application.data.filtersList[event.id];
				}
				else {
					delete window.application.data.filters;
				}
			}
			else {
				delete window.application.data.filters;
			}

			window.application.data.saveLocal();
			window.application.data.openEvent();
			window.application.closeView("event_select");
			//window.application.data.saveLocal();
		//}
		//else {
		//	showError("You selected a event but nothing told you to. How did you get here?");
		//}
	},
	onNew: function() {
		// make sure to save the current event
		if(window.application.data.hasOwnProperty('event')) {
			for(var i = 0; i < window.application.data.events.length; ++i) {
				if(window.application.data.events[i].id == window.application.data.event.id) {
					window.application.data.events[i] = window.application.data.event;
					break;
				}
			}
		}
		if(window.application.data.hasOwnProperty('event')) {
			window.application.data.events[window.application.data.events.length] = window.application.data.event;
		}
		delete window.application.data.event;
		delete window.application.data.team;
		delete window.application.data.goals;
		delete window.application.data.filters;

		window.application.showView("event_setup");
	},
	onBack: function() {
		var t = EventSelectView;
		if(window.application.data.next_view_data == 'event') {
			window.application.data.openEvent();
			window.application.closeView("event_select");
		}
		else {
			//delete window.application.data.climber;
			//delete window.application.data.events;
			window.application.data.saveLocal();
			window.application.showView("welcome");
		}
	}
};

EventSelectView = mergeObjects(View, EventSelectView);
EventSelectModel = mergeObjects(Model, EventSelectModel);

window.application.loadedViews['event_select'] = EventSelectView;
window.application.loadedModels['event_select'] = EventSelectModel;
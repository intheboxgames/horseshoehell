var AlertModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'AlertModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var AlertView = {
	link: 'mobile/page/alert',
	viewLevel: 4,

	init: function() {
		this.name = 'alert';
		this.initBase();
	},

	events: [
		{selector: '#close-button', event: 'click', handler: "onBack"},
	],
	onPreShow: function() {
		this.element.find('.title').html(window.application.data.hasOwnProperty('alertTitle') ? window.application.data.alertTitle : "");
		this.element.find('.subtitle').html(window.application.data.hasOwnProperty('alertMessage') ? window.application.data.alertMessage : "");
		showOverlay();
	},
	onBack: function() {
		var t = AlertView;
		window.application.closeView("alert");
		hideOverlay();
	}
};

AlertView = mergeObjects(View, AlertView);
AlertModel = mergeObjects(Model, AlertModel);

window.application.loadedViews['alert'] = AlertView;
window.application.loadedModels['alert'] = AlertModel;
var CompareModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'CompareModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var CompareView = {
	link: 'mobile/page/compare',

	init: function() {
		this.name = 'CompareView';
		this.initBase();
	},

	events: [
		//{selector: 'body', event: 'click', handler: "onSubmit"},
		//{selector: 'body', event: 'keypress', handler: "onSubmit"},
	],
	onSubmit: function() {
		var t = this;
		window.application.showView("welcome");
	}
};
CompareView = mergeObjects(View, CompareView);
CompareModel = mergeObjects(Model, CompareModel);

window.application.loadedViews['compare'] = CompareView;
window.application.loadedModels['compare'] = CompareModel;

var hourly_data = [];
hourly_data[0] = {routes:"0", total_routes:"0", score:"0", total_score:"0", rating:"0", total_rating:"0", points_per_route:"0", total_points_per_routes:"0", trad:"0", total_trad:"0", height:"0", total_height:"0"};

var chart_titles = {
    routes: "Routes Each Hour",
    total_routes: "Total Routes Over Time",
    score: "Score Each Hour",
    total_score: "Total Score Over Time",
    rating: "Average Rating Each Hour",
    total_rating: "Overall Average Rating",
    points_per_route: "Average Points Per Route Each Hour",
    total_points_per_routes: "Total Average Points Per Route",
    trad: "Trad Climbs Each Hour",
    total_trad: "Total Trad Climbs Over Time",
    height: "Height Climbed Each Hour",
    total_height: "Total Height Climbed Over Time",
};
var chart_labels = {
    routes: "Routes",
    total_routes: "Total Routes",
    score: "Score",
    total_score: "Total Score",
    rating: "Average Rating",
    total_rating: "Overall Average Rating",
    points_per_route: "Average Points Per Route",
    total_points_per_routes: "Average Points Per Route",
    trad: "Trad Climbs",
    total_trad: "Total Trad Climbs",
    height: "Height Climbed",
    total_height: "Total Height Climbed",
};

function show_chart(type) { 
    chart_data = [];
    for(var i = 0; i < hourly_data.length; ++i) {
        chart_data[i] = hourly_data[i].total_score;
    }
    $('#score-container').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: "Total Score over Time",
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            allowDecimals: false,
            title: {
                text: 'Hour'
            },
            labels: {
                formatter: function () {
                    return 'Hour ' + this.value;
                }
            }
        },
        yAxis: {
            title: {
                text: "Total total_score"
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
        plotOptions: {
            area: {
                pointStart: 0,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: "Your Score",
            data: [0,1000,1500,2000,3500,4500,5000,6500,7000,7500,9000,10000,10500,11000,11500,12500,13000,14500,15500,15700,17000,19000,22000],
        },{
            name: "Luke's Score",
            data: [0,1000,1500,2500,5000,6000,9000,10000,11000,12000,13000,14000,15000,15500,16500,16000,16500,17500,18500,19000,20000,20500,21000],
        }]
    });
}
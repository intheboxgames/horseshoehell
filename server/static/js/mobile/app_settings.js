var AppSettingsModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'AppSettingsModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var AppSettingsView = {
	link: 'mobile/page/app_settings',

	init: function() {
		this.name = 'app_settings';
		this.initBase();
	},

	events: [
		{selector: '#app_settings-submit', event: 'click', handler: "onSubmit"},
		{selector: '#app_settings-cancel', event: 'click', handler: "onBack"},
		{selector: '#theme', event: 'change', handler: "onTheme"},
		{selector: '#lock', event: 'change', handler: "onLock"},
		{selector: '#power', event: 'change', handler: "onPower"},
		{selector: '#progress', event: 'change', handler: "onProgress"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.next_view_data == 'event') {
			t.element.css({'z-index': 9000});
			showOverlay();
		}

		t.element.find('#theme option[value="' + window.application.data.settings.theme + '"]').prop('selected', true);
		t.element.find('#lock option[value="' + (window.application.data.settings.autoLock ? 'on' : 'off') + '"]').prop('selected', true);
		t.element.find('#power option[value="' + (window.application.data.settings.lowPower ? 'on' : 'off') + '"]').prop('selected', true);
		t.element.find('#progress option[value="' + window.application.data.settings.progress + '"]').prop('selected', true);
		t.element.find('#changeScores option[value="' + (window.application.data.settings.preventScoreChanges ? 'no' : 'yes') + '"]').prop('selected', true);
	},
	onHide: function() {
		hideOverlay();
		$('body').removeClass('light').removeClass('dark').addClass(window.application.data.settings.theme);
	},
	onTheme: function() {
		var t = AppSettingsView;
		$('body').removeClass('light').removeClass('dark').addClass(t.element.find('#theme').val());
	},
	onLock: function() {
	},
	onPower: function() {
	},
	onProgress: function() {
	},
	onSubmit: function() {
		var t = AppSettingsView;

		window.application.data.settings.theme = t.element.find('#theme').val();
		window.application.data.settings.autoLock = t.element.find('#lock').val() == 'on';
		window.application.data.settings.lowPower = t.element.find('#power').val() == 'on';
		window.application.data.settings.progress = t.element.find('#progress').val();
		window.application.data.settings.preventScoreChanges = t.element.find('#changeScores').val() == 'no';

		window.application.data.saveLocal();

		if(window.application.data.next_view_data == 'event') {
			window.application.data.updateGoalsProgress();
			window.application.closeView('app_settings');
		}
		else {
			window.application.data.openEvent();
		}
	},
	onBack: function() {
		if(window.application.data.next_view_data == 'event') {
			window.application.closeView('app_settings');
		}
		else {
			window.application.data.openEvent();
		}
	},
};

AppSettingsView = mergeObjects(View, AppSettingsView);
AppSettingsModel = mergeObjects(Model, AppSettingsModel);

window.application.loadedViews['app_settings'] = AppSettingsView;
window.application.loadedModels['app_settings'] = AppSettingsModel;
var ProgressModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'ProgressModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}


var ProgressView = {
	link: 'mobile/page/progress',
    viewLevel: 2,

    init: function() {
        this.name = 'progress';
        this.initBase();

        HighchartsDefaultTheme = $.extend(true, {}, Highcharts.getOptions(), {});
    },
    events: [
        {selector: '#graph-type', event: 'change', handler: "onGraphChange"},
        //{selector: 'body', event: 'keypress', handler: "onSubmit"},
    ],
    onPreShow: function() {
        var t = this;
        $('.event-footer #progress-button').addClass('active');

        if(!window.application.data.hasOwnProperty('hourlyData') ||
           !window.application.data.hasOwnProperty('lapData')) {
            window.application.data.updateGoalsProgress();
        }

        var total_score = 0;
        for(var i = 0; i < window.application.data.lapData.length; ++i) {
            total_score += window.application.data.lapData[i].score;
            var entry = $(
            '<tr>' +
                '<th>' + (i + 1) + '</th>' +
                '<th>' + window.application.data.lapData[i].name + '</th>' +
                '<th>' + window.application.data.lapData[i].rating + '</th>' +
                '<th>' + window.application.data.lapData[i].score + '</th>' +
                '<th>' + total_score + '</th>' +
            '</tr>');
            $('#route_list tbody').append(entry);
        }
    },
    onShow: function() {
        this.showChart('total_score');
        $('#route_list').dataTable( {
            "bAutoWidth": false
        });
    },
    onHide: function() {
        $('.event-footer #progress-button').removeClass('active');
    },
    onGraphChange: function(e) {
        var t = ProgressView;
        var type = $(e.currentTarget).val();
        t.showChart(type);
    },

    showChart: function(type) {
        var t = ProgressView;

        var chart_labels = {
            routes: "Routes",
            total_routes: "Total Routes",
            score: "Score",
            total_score: "Total Score",
            rating: "Average Rating",
            total_rating: "Overall Average Rating",
            points_per_route: "Average Points Per Route",
            total_points_per_route: "Average Points Per Route",
            trad: "Trad Climbs",
            total_trad: "Total Trad Climbs",
            height: "Height Climbed",
            total_height: "Total Height Climbed",
        };

        var chart_data = [];
        for(var i = 0; i < window.application.data.hourlyData.length; ++i) {
            chart_data[i] = window.application.data.hourlyData[i][type];
        }

        // set the theme
        if(window.application.data.settings.theme == 'light') {

            var defaultOptions = Highcharts.getOptions();
            for (var prop in defaultOptions) {
                if (typeof defaultOptions[prop] !== 'function') delete defaultOptions[prop];
            }
            Highcharts.setOptions(HighchartsDefaultTheme);
        }
        else {
            Highcharts.setOptions(HighchartsDarkTheme);
        }

        $('#score-container').highcharts({
            chart: {
                type: 'area'
            },
            title: {
                text: '',
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                allowDecimals: false,
                title: {
                    text: 'Hour'
                },
                labels: {
                    formatter: function () {
                        return 'Hour ' + this.value;
                    }
                }
            },
            yAxis: {
                title: {
                    text: chart_labels[type],
                },
                labels: {
                    formatter: function () {
                        if(type == 'rating' || type == 'total_rating') {
                            for(var r in window.application.data.ratings) {
                                if(this.value == window.application.data.ratings[r].id) {
                                    return window.application.data.ratings[r].name;
                                }
                            }
                        }
                        return this.value;
                    }
                }
            },
            plotOptions: {
                area: {
                    pointStart: 0,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                name: chart_labels[type],
                data: chart_data,
            }]
        });
    }


};
ProgressView = mergeObjects(View, ProgressView);
ProgressModel = mergeObjects(Model, ProgressModel);

window.application.loadedViews['progress'] = ProgressView;
window.application.loadedModels['progress'] = ProgressModel;












// Theme for highcharts
// Load the fonts

var HighchartsDefaultTheme;
var HighchartsDarkTheme = {
   colors: ["#3776fb", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
   chart: {
      backgroundColor: {
         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, 'rgba(33, 33, 33, 0.98)'],
            [1, 'rgba(44, 44, 44, 0.98)']
         ]
      },
      plotBorderColor: '#606063'
   },
   title: {
      style: {
         color: '#E0E0E3',
         fontSize: '20px'
      }
   },
   subtitle: {
      style: {
         color: '#E0E0E3',
      }
   },
   xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};

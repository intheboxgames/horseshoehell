var SplashModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'SplashModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var SplashView = {
	link: 'mobile/page/splash',

	init: function() {
		this.name = 'splash';
		this.initBase();
	},

	events: [
		{selector: 'body', event: 'click', handler: "onSubmit"},
	],

	onShow: function() {
		var t = this;
		setTimeout(function() {
			if(t.isActive()) {
				t.onSubmit();
			}
		}, 3000); // 3 seconds

		// preload all the other views
		window.application.preloadView('welcome');
		window.application.preloadView('alert');
		window.application.preloadView('app_settings');
		window.application.preloadView('climber_select');
		//window.application.preloadView('compare');
		window.application.preloadView('compare_settings');
		window.application.preloadView('create_user');
		//window.application.preloadView('event_end');
		window.application.preloadView('event_main');
		window.application.preloadView('event_select');
		window.application.preloadView('event_setup');
		window.application.preloadView('event_waiting');
		window.application.preloadView('filter_settings');
		window.application.preloadView('goals');
		window.application.preloadView('goals_settings');
		window.application.preloadView('progress');
		window.application.preloadView('routes');
		window.application.preloadView('settings');
	},
	onSubmit: function() {
		var t = this;

		// set to fade out
		$('#splash-wrapper').removeClass('instant').addClass('fade');

		// determine next
		// no climber so go to welcome
		if(!window.application.data.hasOwnProperty('climber') || !window.application.data.climber.hasOwnProperty('valid') || !window.application.data.climber.valid) {
			window.application.showView("welcome");
		}
		else {
			window.application.data.checkForEvents();
		}
	}
};
SplashView = mergeObjects(View, SplashView);
SplashModel = mergeObjects(Model, SplashModel);

window.application.loadedViews['splash'] = SplashView;
window.application.loadedModels['splash'] = SplashModel;
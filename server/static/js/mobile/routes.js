var RoutesModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'RoutesModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var routeTemplate;
var areaTemplate;
var RoutesView = {
	link: 'mobile/page/routes',
	viewLevel: 2,

	init: function() {
		this.name = 'routes';
		this.initBase();
	},

	events: [
		//{selector: 'body', event: 'click', handler: "onSubmit"},
		//{selector: 'body', event: 'keypress', handler: "onSubmit"},
	],
	onPreShow: function() {
		var t = this;

		areaTemplate = t.element.find('.area-wrapper');
		routeTemplate = t.element.find('.route-wrapper');

		t.updateRoutesList();
		$('.event-footer #routes-button').addClass('active');
	},
	onShow: function() {
	},
	onHide: function() {
		$('.event-footer #routes-button').removeClass('active');
	},
	updateRoutesList: function() {
		var t = this;
		var routes = [];

		var filters = {
			lowestRating: 1,
			highestRating: window.application.data.ratings.length - 1,
			walls: false,
			style: 'both',
			height: 'all',
		};
		if(window.application.data.hasOwnProperty(filters)) {
			filters = window.application.data.filters;
		}

		var category = window.application.data.climber.category;
		var categoryMax = category == 'rec' ? 12 : (category == 'int' ? 18 : (category == 'adv' ? 23 : (window.application.data.ratings.length - 1)));
		filters.highestRating = filters.highestRating > categoryMax ? categoryMax : filters.highestRating;

		for(var i in window.application.data.routes) {
			var route = window.application.data.routes[i];
			if(	route.rating.id >= filters.lowestRating &&
				route.rating.id <= filters.highestRating &&
				(!filters.walls || filters.walls.indexOf(route.wall.id) >= 0) &&
				(filters.style == 'both' || (filters.style == 'trad' && route.trad) || (filters.style == 'sport' && !route.trad)) &&
				(filters.height == 'all' || (filters.height == 'tall' && route.tall) || (filters.height == 'short' && !route.tall))
				) {
				routes[routes.length] = route; 
			}
		}

		if(window.application.data.event.hasOwnProperty('laps')) {
			for(var i in routes) {
				if(!window.application.data.event.laps[routes[i].id]) {
					window.application.data.event.laps[routes[i].id] = {first: {sent: false, time: '', pinkpoint: false}, second: {sent: false, time: '', pinkpoint: false}, locked: false};
				}
				routes[i].laps = window.application.data.event.laps[routes[i].id];
			}
		}
		else {
			window.application.data.event.laps = [];
			for(var i in routes) {
				window.application.data.event.laps[routes[i].id] = {first: {sent: false, time: '', pinkpoint: false}, second: {sent: false, time: '', pinkpoint: false}, locked: false};
				routes[i].laps = window.application.data.event.laps[routes[i].id];
			}
		}	

		var orderedRoutes = [];
		for(var i in routes) {
			orderedRoutes[routes[i].hasOwnProperty('position') ? routes[i].position : routes[i].id] = routes[i];
		}


		var endTime = new Date(window.application.data.event.end_time).getTime();
		endTime += (15 * 60 * 1000);
		t.eventEnded = endTime <= new Date();
		t.eventEnded = t.eventEnded && (window.application.data.settings.preventScoreChanges || window.application.data.event.is_official);

		var parent = t.element.find('.scroll-wrapper');
		parent.empty();
		var lastArea = '';
		var areaDiv = null;
		for(var i in orderedRoutes) {
			if(!orderedRoutes[i]) {
				continue;
			}
			var showRoute = function(route, pinkpoint) {

				// Check for a new area
				if(route.wall.name != lastArea) {
					var area = areaTemplate.clone();
					area.find('.area-title').html(route.wall.name);
					parent.append(area);
					lastArea = route.wall.name;
					areaDiv = area.find('.area-routes');
					(function(area) {
						area.find('.area-title').on('click', function() {
							if(area.find('.area-more-button > i').hasClass('fa-chevron-down')) {
								area.find('.area-routes').slideDown();
								area.find('.area-more-button > i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
							}
							else {
								area.find('.area-routes').slideUp();
								area.find('.area-more-button > i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
							}

						});
					})(area);
				}

				var e = routeTemplate.clone();
				e.attr('data-route', route.id);
				if(route.trad === true || route.trad == '1') {

					if(pinkpoint) {
						e.attr('data-trad', 'pinkpoint');
					}
					else {
						e.attr('data-trad', 'redpoint');
					}
					if(route.laps.first.sent) {
						if(route.laps.first.pinkpoint) {
							if(pinkpoint) {
								e.find('.lap1 > i').removeClass('fa-square-o').addClass('fa-check-square-o');
							}
							else {
								e.find('.lap1').addClass('disabled');
							}
						}
						else  {
							if(pinkpoint) {
								e.find('.lap1').addClass('disabled');
							}
							else {
								e.find('.lap1 > i').removeClass('fa-square-o').addClass('fa-check-square-o');
							}
						}
					}
					if(route.laps.second.sent) {
						if(route.laps.second.pinkpoint) {
							if(pinkpoint) {
								e.find('.lap2 > i').removeClass('fa-square-o').addClass('fa-check-square-o');
							}
							else {
								e.find('.lap2').addClass('disabled');
							}
						}
						else {
							if(pinkpoint) {
								e.find('.lap2').addClass('disabled');
							}
							else {
								e.find('.lap2 > i').removeClass('fa-square-o').addClass('fa-check-square-o');
							}
						}
					}
				}
				else {
					if(route.laps.first.sent) {
						e.find('.lap1 > i').removeClass('fa-square-o').addClass('fa-check-square-o');
					}
					if(route.laps.second.sent) {
						e.find('.lap2 > i').removeClass('fa-square-o').addClass('fa-check-square-o');
					}
				}

				e.find('.lap-time.lap1 input').prop('disabled', !route.laps.first.sent);
				e.find('.lap-time.lap2 input').prop('disabled', !route.laps.second.sent);
				e.find('.lap-time.lap1 input').val(route.laps.first.time);
				e.find('.lap-time.lap2 input').val(route.laps.second.time);

				if(route.laps.locked) {
					e.find('.lap > i').css({color: '#999'});
					e.find('.route-more .route-lock > i').removeClass('fa-toggle-off').addClass('fa-toggle-on').css({color: '#3776fb'});
					e.find('.route-more .route-lock .label').html('Route lock ON');
					e.find('.lap-time input').prop('disabled', true);
				}

				if(t.eventEnded) {
					e.find('.lap > i').css({color: '#999'});
				}
				else {
					e.find('.lap').on('click', function(el) {
						t.addLap($(el.currentTarget), route);
					});
				}

				e.find('.route-more-button').on('click', function(el) {
					el = $(el.currentTarget).parent();
					if(el.find('.route-more-button > i').hasClass('fa-chevron-down')) {
						el.find('.route-more').slideDown();
						el.find('.route-more-button > i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
					}
					else {
						el.find('.route-more').slideUp();
						el.find('.route-more-button > i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
					}
				});

				e.find('.route-more .route-lock > i').on('click', function(el) {
					t.lockRoute($(el.currentTarget), route);
				});

				e.find('.lap-time.lap1 input').on('change', function(el) {
					t.changeLapTime($(el.currentTarget), route, 1);
				});
				e.find('.lap-time.lap2 input').on('change', function(el) {
					t.changeLapTime($(el.currentTarget), route, 2);
				});

				e.find('.route-info .name').html(route.name + (pinkpoint ? ' PP' : '') + (route.tall ? ' *' : ''));
				e.find('.route-info .rating').html(route.rating.name);
				e.find('.route-info .style').html(route.trad === true || route.trad == '1' ? 'Trad' : 'Sport');
				e.find('.route-info .page').html('pg ' + route.page + ' #' + route.number);
				e.find('.route-info .points').html(route.rating.points);

				var pointsRating = parseInt(route.rating.id);
				if(route.tall){
					pointsRating += 1;
				}
				if((route.trad === true || route.trad == '1') && !pinkpoint) {
					pointsRating += 1
				}
				for(var r in window.application.data.ratings) {
					if(window.application.data.ratings[r].id == pointsRating) {
						e.find('.route-info .points').html(window.application.data.ratings[r].points);
						break;
					}
				}
				areaDiv.append(e);
			};
			if(orderedRoutes[i].trad === true || orderedRoutes[i].trad == '1') {
				showRoute(orderedRoutes[i], false);
				showRoute(orderedRoutes[i], true);
			}
			else {
				showRoute(orderedRoutes[i], false);
			}

		}

		parent.append($('<div style="height:10em"></div>'));
	},
	addLap: function(el, route) {
		var t = RoutesView;
		if(window.application.data.event.laps[route.id].locked || el.hasClass('disabled') || t.eventEnded) {
			return;
		}

		var parent = el.parent();
		var trad = parent.data('trad') == 'redpoint';
		var pinkpoint = parent.data('trad') == 'pinkpoint';
		var lap = el.hasClass('lap1') ? 1 : 2;
		var lapName = lap == 1 ? 'first' : 'second';
		var other = null;
		var selector = '';
		if(trad) {
			other = $('.route-wrapper[data-route=' + route.id + '][data-trad="pinkpoint"]');
			selector = '.' + el.attr('class').split(/\s+/).join('.');
		}
		else if(pinkpoint) {
			other = $('.route-wrapper[data-route=' + route.id + '][data-trad="redpoint"]');
			selector = '.' + el.attr('class').split(/\s+/).join('.');
		}

		if(el.find('i').hasClass('fa-square-o')) {
			// add a lap

			var d = new Date();
			var time = ((d.getHours() == 0 ? 24 : ((d.getHours() < 10 ? '0' : '') + d.getHours())) + ":" + (d.getMinutes() < 10 ? '0' : '') + d.getMinutes());
			var date = d.getFullYear() + '-' + (d.getMonth() < 9 ? '0' : '') + (d.getMonth() + 1) + '-' + (d.getDate() < 10 ? '0' : '') + d.getDate();

			window.application.data.event.laps[route.id][lapName] = {
				sent: true,
				time: date + 'T' + time,
				pink_point: pinkpoint,
			};

			el.parent().find('.lap-time.lap' + lap + ' input').val(window.application.data.event.laps[route.id][lapName].time);
			el.parent().find('.lap-time.lap' + lap + ' input').prop('disabled', false);
			
			if(trad || pinkpoint) {
				other.find(selector).addClass('disabled');
				other.find('.lap-time.lap' + lap + ' input').val(window.application.data.event.laps[route.id][lapName].time);
				other.find('.lap-time.lap' + lap + ' input').prop('disabled', false);
			}

			if(lap == 2 && window.application.data.settings.autoLock) {
				var lock = el.parent().find('.route-lock > i');
				t.lockRoute(lock, route);
			}

			window.application.data.addLap(route, pinkpoint, lap, window.application.data.event.laps[route.id][lapName].time);

			el.find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
		}
		else {
			// remove a lap

			window.application.data.event.laps[route.id][lapName] = {
				sent: false,
				time: '',
				pink_point: false,
			};

			el.parent().find('.lap-time.lap' + lap + ' input').val('');
			el.parent().find('.lap-time.lap' + lap + ' input').prop('disabled', true);
			
			if(trad || pinkpoint) {
				other.find(selector).removeClass('disabled');
				other.find('.lap-time.lap' + lap + ' input').val('');
				other.find('.lap-time.lap' + lap + ' input').prop('disabled', true);
			}

			window.application.data.removeLap(route, pinkpoint, lap);

			el.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
		}

		window.application.data.updateGoalsProgress();
		window.application.data.saveLocal();
	},
	lockRoute: function(el, route) {
		var t = RoutesView;
		var parent = $('.route-wrapper[data-route=' + route.id + ']');
		if(el.hasClass('fa-toggle-on')) {
			el = parent.find('.fa-toggle-on');
			el.removeClass('fa-toggle-on').addClass('fa-toggle-off');
			el.css({color: '#999'});
			el.parent().find('.label').html('Route lock OFF');
			parent.find('.lap-time.lap1 input').prop('disabled', !window.application.data.event.laps[route.id].first.sent);
			parent.find('.lap-time.lap2 input').prop('disabled', !window.application.data.event.laps[route.id].second.sent);
			if(!t.eventEnded) parent.find('.lap > i').css({color: 'inherit'});
			window.application.data.event.laps[route.id].locked = false;
		}
		else {
			el = parent.find('.fa-toggle-off');
			el.removeClass('fa-toggle-off').addClass('fa-toggle-on');
			el.css({color: '#3776fb'});
			el.parent().find('.label').html('Route lock ON');
			parent.find('.lap > i').css({color: '#999'});
			parent.find('.lap-time.lap1 input').prop('disabled', true);
			parent.find('.lap-time.lap2 input').prop('disabled', true);
			window.application.data.event.laps[route.id].locked = true;
		}
		window.application.data.saveLocal();
	},
	changeLapTime: function(el, route, lap) {
		var t = RoutesView;

		var parent = $('.route-wrapper[data-route=' + route.id + ']');
		var newVal = el.val();

		parent.find('.lap-time.lap' + lap + ' input').val(newVal);
		window.application.data.event.laps[route.id][lap == 1 ? 'first' : 'second'].time = newVal;

		window.application.data.updateLap(route, window.application.data.event.laps[route.id][lap == 1 ? 'first' : 'second'].pinkpoint, lap, newVal);

		window.application.data.updateGoalsProgress();
		window.application.data.saveLocal();
	},
};
RoutesView = mergeObjects(View, RoutesView);
RoutesModel = mergeObjects(Model, RoutesModel);

window.application.loadedViews['routes'] = RoutesView;
window.application.loadedModels['routes'] = RoutesModel;
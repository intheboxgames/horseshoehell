var WelcomeModel = {
	first_name: '',
	last_name: '',
	link: '/client/climbers',

	init: function() {
		this.name = 'WelcomeModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var WelcomeView = {
	link: 'mobile/page/welcome',

	init: function() {
		this.name = 'welcome';
		this.initBase();
	},

	events: [
		{selector: '#welcome-submit', event: 'click', handler: "onSubmit"},
		{selector: '#welcome-new', event: 'click', handler: "onCreateNew"},
	],
	onShow: function() {
		if(window.application.data.hasOwnProperty('climber')) {
			var climber = window.application.data.climber;
			this.element.find('#username').val(climber.username);
			this.element.find('#first_name').val(climber.first_name);
			this.element.find('#last_name').val(climber.last_name);
		}
	},
	onCreateNew: function() {
		window.application.showView("create_user");
	},
	onSubmit: function() {
		var t = WelcomeView;

		// validate the data
		var username = t.element.find('#username').val().trim();
		var password = t.element.find('#password').val().trim();

		var first_name = t.element.find('#first_name').val().trim();
		var last_name = t.element.find('#last_name').val().trim();
		var birth_date = t.element.find('#birth_date').val().trim();
		var errors = "";

		if(username.length > 0 || password.length > 0)
		{
			if(username.length == 0) {
				errors = "Please provide your username to continue.";
				t.element.find('#username').addClass('error');
				t.element.find('#username').on('focus', function() {
					t.element.find('#username').off('focus');
					t.element.find('#username').removeClass('error');
				});
			}
			/*if(password.length == 0) {
				errors = "Please provide your password to continue.";
				t.element.find('#password').addClass('error');
				t.element.find('#password').on('focus', function() {
					t.element.find('#password').off('focus');
					t.element.find('#password').removeClass('error');
				});
			}*/
		}
		else if(first_name.length > 0 || last_name.length > 0){
			if(first_name.length == 0) {
				errors = "Please provide your first name to continue.";
				t.element.find('#first_name').addClass('error');
				t.element.find('#first_name').on('focus', function() {
					t.element.find('#first_name').off('focus');
					t.element.find('#first_name').removeClass('error');
				});
			}
			if(last_name.length == 0) {
				if(errors.length > 0) {errors += "</br>";}
				errors += "Please provide your last name to continue.";
				t.element.find('#last_name').addClass('error');
				t.element.find('#last_name').on('focus', function() {
					t.element.find('#last_name').off('focus');
					t.element.find('#last_name').removeClass('error');
				});
			}
			if(birth_date.length == 0) {
				if(errors.length > 0) {errors += "</br>";}
				errors += "Please provide your birthday to continue.";
				t.element.find('#birth_date').addClass('error');
				t.element.find('#birth_date').on('focus', function() {
					t.element.find('#birth_date').off('focus');
					t.element.find('#birth_date').removeClass('error');
				});
			}
		}
		if(errors.length > 0) {
			showError(errors);
			return;
		}

		// Search for this user on the server
		showLoading();
		if(username.length > 0) {

			window.application.data.makeServerCall('/client/auth', {username: username, password: password}, function(result) {
				if(result && result.status == 'success') {
					window.application.data.climber = result.climber;
					window.application.data.climber.valid = true;
					if(window.application.data.hasOwnProperty('climbers')) { delete window.application.data.climbers; }
					if(window.application.data.hasOwnProperty('event')) { delete window.application.data.event; }
					if(window.application.data.hasOwnProperty('team')) { delete window.application.data.team; }
					window.application.data.saveLocal();
					window.application.data.checkForEvents();
				}
				else {
					hideLoading();
					showError("That username and password combination was not found. Please try again or create a new account.");

					t.element.find('#password').val("");
					t.element.find('#password').addClass('error');
					t.element.find('#password').on('focus', function() {
						t.element.find('#password').off('focus');
						t.element.find('#password').removeClass('error');
					});
				}
			});
		}
		else {
			window.application.data.makeServerCall('/client/find_climber', {first_name: first_name, last_name: last_name, birth_date: birth_date}, function(result) {
				if(result && result.status == 'success') {
					if(result.climbers.length == 1) {
						window.application.data.climber = result.climbers[0];
						window.application.data.climber.valid = true;
						if(window.application.data.hasOwnProperty('climbers')) { delete window.application.data.climbers; }
						if(window.application.data.hasOwnProperty('event')) { delete window.application.data.event; }
						if(window.application.data.hasOwnProperty('team')) { delete window.application.data.team; }
						window.application.data.saveLocal();
						window.application.data.checkForEvents();
					}
					else {
						window.application.data.climbers = result.climbers;
						window.application.data.next_view_data = 'main';
						window.application.showView("climber_select");
					}
				}
				else {
					hideLoading();
					if(result && result.status == 'password') {
						showError("The account you are trying to access is protected with a password. Please enter your username and password to login.");
					}
					else {
						showError("That name a birthday combination was not found. Please try again or create a new account.");

						t.element.find('#first_name').addClass('error');
						t.element.find('#first_name').on('focus', function() {
							t.element.find('#first_name').off('focus');
							t.element.find('#first_name').removeClass('error');
						});
						t.element.find('#last_name').addClass('error');
						t.element.find('#last_name').on('focus', function() {
							t.element.find('#last_name').off('focus');
							t.element.find('#last_name').removeClass('error');
						});
						t.element.find('#birth_date').addClass('error');
						t.element.find('#birth_date').on('focus', function() {
							t.element.find('#birth_date').off('focus');
							t.element.find('#birth_date').removeClass('error');
						});
					}
				}
			});
		}


	}
};
WelcomeView = mergeObjects(View, WelcomeView);
WelcomeModel = mergeObjects(Model, WelcomeModel);

window.application.loadedViews['welcome'] = WelcomeView;
window.application.loadedModels['welcome'] = WelcomeModel;
var SettingsModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'SettingsModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var SettingsView = {
	link: 'mobile/page/settings',
    viewLevel: 2,

	init: function() {
		this.name = 'settings';
		this.initBase();
	},

	events: [
		{selector: '#settings-app', event: 'click', handler: "onApp"},
		{selector: '#settings-goals', event: 'click', handler: "onGoals"},
		{selector: '#settings-routes', event: 'click', handler: "onRoutes"},
		{selector: '#settings-event', event: 'click', handler: "onEvent"},
		{selector: '#settings-select', event: 'click', handler: "onSelect"},
		{selector: '#settings-logout', event: 'click', handler: "onLogout"},
		{selector: '#settings-help', event: 'click', handler: "onHelp"},
		{selector: '#settings-import', event: 'click', handler: "onImport"},
		{selector: '#settings-export', event: 'click', handler: "onExport"},
	],
	onPreShow: function() {
		var t = this;
        $('.event-footer #settings-button').addClass('active');
		
		// TODO: get a list of event climbers and teams from the server. Fail if there is no connection
	},
	onHide: function() {
        $('.event-footer #settings-button').removeClass('active');
	},
	onApp: function() {
		window.application.data.next_view_data = 'event';
		window.application.showView('app_settings');
	},
	onGoals: function() {
		window.application.data.next_view_data = 'event';
		window.application.showView('goals_settings');
	},
	onRoutes: function() {
		window.application.data.next_view_data = 'event';
		window.application.showView('filter_settings');
	},
	onEvent: function() {
		window.application.data.next_view_data = 'event';
		window.application.showView('event_setup');
	},
	onImport: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "The Import and Export features are not implemented yet but they will be soon.<br/><br/>The Import page will allow the climber to use a blue tooth connection or scan a QR code from another climber's device and upload that data to the server. The importing climber will not see the other climber's data, only a confirmation message of whether or not the upload was successfull.";
		window.application.showView('alert');
	},
	onExport: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "The Import and Export features are not implemented yet but they will be soon.<br/><br/>The Export screen will allow a climber to share their event information with another device so it can be uploaded to the servers.";
		window.application.showView('alert');
	},
	onHelp: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "Help is not available at this time. The Help page will be the very last thing added, after all other features have been finalized and implemented. That way I don't have to update the page multiple times as functionality changes.";
		window.application.showView('alert');
	},
	onSelect: function() {
		window.application.closeView('settings');
		window.application.closeView('event_main');
		window.application.data.next_view_data = 'event';
		window.application.showView('event_select');
	},
	onLogout: function() {
		if(confirm("Are you sure you want to logout? An internet connection is required to log back in.")) {
			window.application.closeView('settings');
			window.application.closeView('event_main');
			delete window.application.data.climber;
			delete window.application.data.events;
			window.application.data.saveLocal();
			window.application.showView("welcome");
		}
	},
};

SettingsView = mergeObjects(View, SettingsView);
SettingsModel = mergeObjects(Model, SettingsModel);

window.application.loadedViews['settings'] = SettingsView;
window.application.loadedModels['settings'] = SettingsModel;
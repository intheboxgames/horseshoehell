var GoalsModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'GoalsModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var GoalsView = {
	link: 'mobile/page/goals',
    viewLevel: 2,

    init: function() {
        this.name = 'goals';
        this.initBase();
    },

    events: [
        //{selector: '#graph-type', event: 'change', handler: "onGraphChange"},
        //{selector: 'body', event: 'keypress', handler: "onSubmit"},
		{selector: '.tab-button', event: 'click', handler: "onTabSelect"},
		{selector: '#need-button', event: 'click', handler: "onNeedPressed"},
    ],
    onPreShow: function() {
        var t = this;
        $('.event-footer #goals-button').addClass('active');

        if(!window.application.data.hasOwnProperty('hourlyData') ||
           !window.application.data.hasOwnProperty('lapData')) {
            window.application.data.updateGoalsProgress();
        }

        var hasGoals = false;
		if(window.application.data.hasOwnProperty('goals')) {
			var goals = window.application.data.goals;
			var data = window.application.data.hourlyData[window.application.data.hourlyData.length - 1];

			if(goals.personal.score > 0) { 
				hasGoals = true;
				var percent = ((data.total_score / goals.personal.score) * 100);
				percent = (percent >= 100 ? Math.floor(percent) : percent.toFixed(2)) + '%';
				t.element.find('#tab-personal-goals table').append('<tr><th style="padding:0.5em 0;">Total Score</th><th>' + goals.personal.score + '</th><th>' + data.total_score + '</th><th>' + percent + '</th></tr>'); 
			}
			if(goals.personal.laps > 0) { 
				hasGoals = true;
				var percent = ((data.total_routes / goals.personal.laps) * 100);
				percent = (percent >= 100 ? Math.floor(percent) : percent.toFixed(2)) + '%';
				t.element.find('#tab-personal-goals table').append('<tr><th style="padding:0.5em 0;">Total Laps</th><th>' + goals.personal.laps + '</th><th>' + data.total_routes + '</th><th>' + percent + '</th></tr>'); 
			}
			if(goals.personal.height > 0) { 
				hasGoals = true;
				var percent = ((data.total_height / goals.personal.height) * 100);
				percent = (percent >= 100 ? Math.floor(percent) : percent.toFixed(2)) + '%';
				t.element.find('#tab-personal-goals table').append('<tr><th style="padding:0.5em 0;">Total Height</th><th>' + goals.personal.height + '</th><th>' + data.total_height + '</th><th>' + percent + '</th></tr>'); 
			}
			if(goals.personal.rating > 0) { 
				hasGoals = true;
				var percent = ((data.total_rating / goals.personal.rating) * 100);
				percent = (percent >= 100 ? Math.floor(percent) : percent.toFixed(2)) + '%';
				var goalRating = '';
				var actualRating = '';
                for(var r in window.application.data.ratings) {
                    if(Math.round(data.total_rating) == window.application.data.ratings[r].id) {
                        actualRating = window.application.data.ratings[r].name;
                    }
                    if(Math.round(goals.personal.rating) == window.application.data.ratings[r].id) {
                        goalRating = window.application.data.ratings[r].name;
                    }
                }
				t.element.find('#tab-personal-goals table').append('<tr><th style="padding:0.5em 0;">Average Rating</th><th>' + goalRating + '</th><th>' + actualRating + '</th><th>' + percent + '</th></tr>'); 
			}
			if(goals.personal.lapsPerHour > 0) { 
				hasGoals = true;
				var value = data.total_routes / (window.application.data.hourlyData.length > 1 ? window.application.data.hourlyData.length - 1 : window.application.data.hourlyData.length);
				var percent = ((value / goals.personal.lapsPerHour) * 100);
				percent = (percent >= 100 ? Math.floor(percent) : percent.toFixed(2)) + '%';
				t.element.find('#tab-personal-goals table').append('<tr><th style="padding:0.5em 0;">Laps Per Hour</th><th>' + goals.personal.lapsPerHour + '</th><th>' + value.toFixed(2) + '</th><th>' + percent + '</th></tr>'); 
			}
			if(goals.personal.scorePerHour > 0) { 
				hasGoals = true;
				var value = data.total_score / (window.application.data.hourlyData.length > 1 ? window.application.data.hourlyData.length - 1 : window.application.data.hourlyData.length);
				var percent = ((value / goals.personal.scorePerHour) * 100);
				percent = (percent >= 100 ? Math.floor(percent) : percent.toFixed(2)) + '%';
				t.element.find('#tab-personal-goals table').append('<tr><th style="padding:0.5em 0;">Score Per Hour</th><th>' + goals.personal.scorePerHour + '</th><th>' + value.toFixed(2) + '</th><th>' + percent + '</th></tr>'); 
			}

			/*if(goals.team.score > 0) { t.element.find('#team-score').val(goals.team.score); }
			if(goals.team.laps > 0) { t.element.find('#team-laps').val(goals.team.laps); }
			if(goals.team.height > 0) { t.element.find('#team-height').val(goals.team.height); }
			if(goals.team.rating > 0) { t.element.find('#team-rating').val(goals.team.rating); }
			if(goals.team.lapsPerHour > 0) { t.element.find('#team-laps-per-hour').val(goals.team.lapsPerHour); }
			if(goals.team.scorePerHour > 0) { t.element.find('#team-score-per-hour').val(goals.team.scorePerHour); }*/
		}
		
		if(!hasGoals) {
			t.element.find('.form-wrapper').html("<div style='margin-top:3em;font-size:1.4em'>No Goals Set</div><div style='margin-top:1em; width: 80%; display:inline-block;'>Go to Goals in the settings menu to set your goals for this event.</div>");
		}
    },
	onTabSelect: function(e) {
		var s = $(e.currentTarget);
		$('#goals-wrapper .tab-button').removeClass('selected');
		$('#goals-wrapper .form-tab').removeClass('selected');

		s.addClass('selected');
		$('#goals-wrapper #' + s.data('tab')).addClass('selected');
	},
	onNeedPressed: function() {
		window.application.data.alertTitle = "";
		window.application.data.alertMessage = "This feature is a work in progress and may be removed before the final release if it cannot be completed in time.<br/><br/>Eventually this popup will give you a brief explaination of what you need to do to meet all your goals. Example:<br/><br/>To meet your goals, you need to climb 11 more laps rated 5.10a or higher, 7 laps rated 5.9 or higher, and 2 laps rated 5.9- or higher. In addition, each route must be 48 feet in height or taller.<br/><br/>Look for more details on this coming soon.";
		window.application.showView('alert');
	},
    onShow: function() {
    },
    onHide: function() {
        $('.event-footer #goals-button').removeClass('active');
    },
};
GoalsView = mergeObjects(View, GoalsView);
GoalsModel = mergeObjects(Model, GoalsModel);

window.application.loadedViews['goals'] = GoalsView;
window.application.loadedModels['goals'] = GoalsModel;

var FilterSettingsModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'FilterSettingsModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var FilterSettingsView = {
	link: 'mobile/page/filter_settings',
	filters: {},

	init: function() {
		this.name = 'filter_settings';
		this.initBase();
	},

	events: [
		{selector: '.multiselect-button-all', event: 'click', handler: "onSelectAll"},
		{selector: '.multiselect-button-none', event: 'click', handler: "onSelectNone"},
		{selector: '.multiselect-group-header', event: 'click', handler: "onSelectGroup"},
		{selector: '#filter_settings-submit', event: 'click', handler: "onSubmit"},
		{selector: '#filter_settings-cancel', event: 'click', handler: "onBack"},
		{selector: '.multiselect-input > input', event: 'change', handler: "onFilterChanged"},
		{selector: '#lowest-rating', event: 'change', handler: "onFilterChanged"},
		{selector: '#highest-rating', event: 'change', handler: "onFilterChanged"},
		{selector: '#route-style', event: 'change', handler: "onFilterChanged"},
		{selector: '#route-height', event: 'change', handler: "onFilterChanged"},
	],
	onPreShow: function() {
		var t = this;
		if(window.application.data.next_view_data == 'event') {
			t.element.css({'z-index': 9000});
			showOverlay();
		}

		var ratings = window.application.data.ratings;
		for(var i in ratings) {
			var o = $('<option value="' + ratings[i].id + '"></option>').html(ratings[i].name);
			$('#lowest-rating').append(o);
			$('#highest-rating').append(o.clone());
		}

		if(window.application.data.hasOwnProperty('filters')) {
			var filters = window.application.data.filters;

			self.filters = filters;
			for(var i in filters.walls) {
				if(filters.walls[i]) {
					t.element.find('.multiselect-input > input[value="' + filters.walls[i] + '"]').prop('checked', true);
				}
			}

			$('#lowest-rating option[value="' + filters.lowestRating + '"]').prop('selected', true);
			$('#highest-rating option[value="' + filters.highestRating + '"]').prop('selected', true);
			$('#route-style option[value="' + filters.style + '"]').prop('selected', true);
			$('#route-height option[value="' + filters.height + '"]').prop('selected', true);
		}
		else {
			t.onSelectAll();
			$('#lowest-rating option[value="4"]').prop('selected', true);
			$('#highest-rating option[value="31"]').prop('selected', true);
		}
		t.onFilterChanged();
	},
	onHide: function() {
		hideOverlay();
	},
	onSelectAll: function() {
		var t = FilterSettingsView;
		t.element.find('.multiselect-input > input').prop('checked', true);
	},
	onSelectNone: function() {
		var t = FilterSettingsView;
		t.element.find('.multiselect-input > input').prop('checked', false);
	},
	onSelectGroup: function(e) {
		var t = FilterSettingsView;
		var parent = $(e.currentTarget).parent();
		var boxes = parent.find('.multiselect-input > input');
		var checks = parent.find('.multiselect-input > input:checked');

		if(boxes.length == checks.length) {
			// all are checked to uncheck
			parent.find('.multiselect-input > input').prop('checked', false);
		}
		else {
			// otherwise, check them all
			parent.find('.multiselect-input > input').prop('checked', true);
		}
	},
	onFilterChanged: function() {
		var t = FilterSettingsView;
		t.filters = {
			walls: [],
			lowestRating: 4,
			highestRating: 31,
			style: 'both',
			height: 'all',
		};

		var checks = t.element.find('.multiselect-input > input:checked');
		checks.each(function(i) {
			var v = $(checks[i]).val();
			t.filters.walls[t.filters.walls.length] = v;
		});
		t.filters.lowestRating = parseInt(t.element.find('#lowest-rating').val());
		t.filters.highestRating = parseInt(t.element.find('#highest-rating').val());
		t.filters.style = t.element.find('#route-style').val();
		t.filters.height = t.element.find('#route-height').val();

		var routesCount = 0;
		for(var i in window.application.data.routes) {
			var route = window.application.data.routes[i];
			if(	route.rating.id >= t.filters.lowestRating &&
				route.rating.id <= t.filters.highestRating &&
				t.filters.walls.indexOf(route.wall.id) >= 0 &&
				(t.filters.style == 'both' || (t.filters.style == 'trad' && route.trad) || (t.filters.style == 'sport' && !route.trad)) &&
				(t.filters.height == 'all' || (t.filters.height == 'tall' && route.tall) || (t.filters.height == 'short' && !route.tall))
				) {
				routesCount += 1;
			}
		}

		t.element.find('#route-count').html(routesCount);
	},
	onSubmit: function() {
		var t = FilterSettingsView;

		if(!window.application.data.hasOwnProperty('filtersList')) {
			window.application.data.filtersList = [];
		}
		window.application.data.filtersList[window.application.data.event.id] = t.filters;
		window.application.data.filters = t.filters;
		window.application.data.saveLocal();

		if(window.application.data.next_view_data == 'event') {
			window.application.closeView('filter_settings');
		}
		else {
			window.application.data.openEvent();
		}
	},
	onBack: function() {
		if(window.application.data.next_view_data == 'event') {
			window.application.closeView('filter_settings');
		}
		else {
			window.application.data.openEvent();
		}
	},
};

FilterSettingsView = mergeObjects(View, FilterSettingsView);
FilterSettingsModel = mergeObjects(Model, FilterSettingsModel);

window.application.loadedViews['filter_settings'] = FilterSettingsView;
window.application.loadedModels['filter_settings'] = FilterSettingsModel;
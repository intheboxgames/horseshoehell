var CreateUserModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'CreateUserModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var CreateUserView = {
	link: 'mobile/page/create_user',

	init: function() {
		this.name = 'create_user';
		this.initBase();
	},

	events: [
		{selector: '#create-user-submit', event: 'click', handler: "onSubmit"},
		{selector: '#create-user-back', event: 'click', handler: "onBack"},
	],
	onShow: function() {
		if(window.application.data.next_view_data == 'partner') {
			if(window.application.data.hasOwnProperty('team') && window.application.data.team.hasOwnProperty('partner')) {
				var climber = window.application.data.team.partner;
				this.element.find('#first_name').val(climber.first_name);
				this.element.find('#last_name').val(climber.last_name);
			}
			$('#create-user-wrapper .subtitle').html('Please enter your partner\'s information below to create an account for them.');
		}
		else {
			if(window.application.data.hasOwnProperty('climber')) {
				var climber = window.application.data.climber;
				this.element.find('#first_name').val(climber.first_name);
				this.element.find('#last_name').val(climber.last_name);
			}
			$('#create-user-wrapper .subtitle').html('Please enter your information below to create a new account.');
		}
	},
	onSubmit: function() {
		var t = CreateUserView;

		// validate the data
		var first_name = t.element.find('#first_name').val().trim();
		var last_name = t.element.find('#last_name').val().trim();
		var birth_date = t.element.find('#birth_date').val().trim(); // yyyy-mm-dd
		var category = t.element.find('#category').val().trim();
		var username = t.element.find('#username').val().trim();
		var password = t.element.find('#password').val().trim();

		var errors = "";
		if(first_name.length == 0) {
			errors = "Please provide your first name to continue.";
			t.element.find('#first_name').addClass('error');
			t.element.find('#first_name').on('focus', function() {
				t.element.find('#first_name').off('focus');
				t.element.find('#first_name').removeClass('error');
			});
		}
		if(last_name.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide your last name to continue.";
			t.element.find('#last_name').addClass('error');
			t.element.find('#last_name').on('focus', function() {
				t.element.find('#last_name').off('focus');
				t.element.find('#last_name').removeClass('error');
			});
		}
		if(birth_date.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide your birthday to continue.";
			t.element.find('#birth_date').addClass('error');
			t.element.find('#birth_date').on('focus', function() {
				t.element.find('#birth_date').off('focus');
				t.element.find('#birth_date').removeClass('error');
			});
		}
		if(username.length == 0) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Please provide your username to continue.";
			t.element.find('#username').addClass('error');
			t.element.find('#username').on('focus', function() {
				t.element.find('#username').off('focus');
				t.element.find('#username').removeClass('error');
			});
		}
		else if(username.match(/^\w+$/).length =! 1 || username.length < 6 || username.length > 36) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Username must be 6 to 36 characters long and contain only alphanumeric characters and underscores.";
			t.element.find('#username').addClass('error');
			t.element.find('#username').on('focus', function() {
				t.element.find('#username').off('focus');
				t.element.find('#username').removeClass('error');
			});
		}
		if(password.length > 0 && password.length < 4) {
			if(errors.length > 0) {errors += "</br>";}
			errors += "Your password must be empty or at least 4 characters long.";
			t.element.find('#password').addClass('error');
			t.element.find('#password').on('focus', function() {
				t.element.find('#password').off('focus');
				t.element.find('#password').removeClass('error');
			});
		}
		if(errors.length > 0) {
			showError(errors);
			return;
		}

		// Search for this user on the server
		showLoading();
		var climberData = {
			first_name: first_name,
			last_name: last_name,
			birth_date: birth_date,
			category: category,
			username: username,
			password: password,
		};
		window.application.data.makeServerCall('/client/register_climber', climberData, function(result) {
			if(result && result.status == 'success') {
				if(window.application.data.next_view_data == 'partner') {
					window.application.data.team.partner = result.climber;
					window.application.data.team.partner.valid = true;
					window.application.data.createEventServer();
				}
				else {
					window.application.data.climber = result.climber;
					window.application.data.climber.valid = true;
					window.application.data.climber.brand_new = true;
					if(window.application.data.hasOwnProperty('climbers')) { delete window.application.data.climbers; }
					if(window.application.data.hasOwnProperty('event')) { delete window.application.data.event; }
					if(window.application.data.hasOwnProperty('team')) { delete window.application.data.team; }
					window.application.data.saveLocal();
					window.application.data.checkForEvents();
				}
			}
			else {
				hideLoading();
				if(result && result.status == 'username') {
					showError("That username already exists. Please pick a different one.");
					t.element.find('#username').addClass('error');
					t.element.find('#username').on('focus', function() {
						t.element.find('#username').off('focus');
						t.element.find('#username').removeClass('error');
					});
				}
				else {
					showError("There was an error creating account. Please make sure you are connected to the internet and try again.");
				}
			}
		});
	},
	onBack: function() {
		var t = CreateUserView;
		if(window.application.data.next_view_data == 'partner') {
			window.application.showView("event_setup");
		}
		else {
			window.application.showView("welcome");
		}
	}
};

CreateUserView = mergeObjects(View, CreateUserView);
CreateUserModel = mergeObjects(Model, CreateUserModel);

window.application.loadedViews['create_user'] = CreateUserView;
window.application.loadedModels['create_user'] = CreateUserModel;
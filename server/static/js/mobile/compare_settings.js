var CompareSettingsModel = {
	link: '/client/climbers',

	init: function() {
		this.name = 'CompareSettingsModel';
		this.initBase();
	},

	onSubmit: function() {

	}
}

var CompareSettingsView = {
	link: 'mobile/page/compare_settings',
    viewLevel: 2,

	init: function() {
		this.name = 'compare_settings';
		this.initBase();
	},

	events: [
		{selector: '#compare_settings-rankings', event: 'click', handler: "onRankings"},
		{selector: '#compare_settings-routes', event: 'click', handler: "onRoutes"},
		{selector: '#compare_settings-event', event: 'click', handler: "onEvent"},
		{selector: '#compare_settings-compare', event: 'click', handler: "onCompare"},
	],
	onPreShow: function() {
		var t = this;
        $('.event-footer #compare-button').addClass('active');
		
		// TODO: get a list of event climbers and teams from the server. Fail if there is no connection
	},
	onHide: function() {
        $('.event-footer #compare-button').removeClass('active');
	},
	onRankings: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "The compare scores and ranking pages are not implemented yet but they will be soon.<br/><br/>The Ranking page will show a list of all climbers in the event and their current ranking according to a set of filters and sorting. Filters will include Category and Gender and Ranking can be calculated based on any of the award categories (score, laps, team or individual, trad laps, height, most 5.8s etc.) Accuracy or the ranking is dependant on how many climbers use the app and keep their score up to date.";
		window.application.showView('alert');
	},
	onRoutes: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "The compare scores and ranking pages are not implemented yet but they will be soon.<br/><br/>The Route Statistics page will show a list of all routes on the ranch and the number of times that have been climbed so far in the event. Selecting a route will provide a graph showing the number of ascents over time. This could be usefull in seeing what time of day certain routes see more traffic.";
		window.application.showView('alert');
	},
	onEvent: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "The compare scores and ranking pages are not implemented yet but they will be soon.<br/><br/>The Event Statistics page will show a set of interesting statistics about the event such as Total Laps for All Climbers, Total Score for all Climbers, Total Ascents by Rating, and other things like that.";
		window.application.showView('alert');
	},
	onCompare: function() {
		window.application.data.alertTitle = "Not Ready";
		window.application.data.alertMessage = "The compare scores and ranking pages are not implemented yet but they will be soon.<br/><br/>The Compare Scores page will show a screen almost identical to the Progress screen but the graph will show results for both you and the climber or team you have selected.";
		window.application.showView('alert');
	},
};

CompareSettingsView = mergeObjects(View, CompareSettingsView);
CompareSettingsModel = mergeObjects(Model, CompareSettingsModel);

window.application.loadedViews['compare_settings'] = CompareSettingsView;
window.application.loadedModels['compare_settings'] = CompareSettingsModel;
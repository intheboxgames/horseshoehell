window.application = {
	activeView: [],
	nextView: null,
	loadedViews: [],
	loadedModels: [],
	openViews: [],

	init: function() {
		if(this.data) {
			this.data.loadLocal();
			this.data.loadStaticData();
		}
	},
	closeAllViews: function() {
		for(var i in this.activeView) {
			if(this.activeView[i]) {
				this.activeView[level].save();
				this.activeView[level].hide();
				this.activeView[level] = null;
			}
		}
	},
	closeView: function(view) {
		if($.type(view) == "string") {
			for(var i in this.activeView) {
				if(this.activeView[i] && this.activeView[i].name == view) {
					this.activeView[i].save();
					this.activeView[i].hide();
					this.activeView[i] = null;
				}
			}
		}
		else if($.type(view) == 'object') {
			view.save();
			view.hide();
			for(var i in this.activeView) {
				if(this.activeView[i] && this.activeView[i].name == view.name) {
					this.activeView[i].save();
					this.activeView[i].hide();
					this.activeView[i] = null;
				}
			}
		}
	},
	preloadView: function(view) {
		var t = this;
		if(t.loadedViews[view] != null) {
			// already loaded
			if(!t.loadedViews[view].isInited) {
				t.loadedViews[view].init();
			}
			if(!t.loadedViews[view].isLoaded()) {
				t.loadedViews[view].update(function(r) {}, true);
			}
		}
		else {
			var script = window.location.protocol + "//" + window.location.host + "/static/js/mobile/" + view + ".js";
			$.getScript(script, function(){
				// success! but nothing to do now because we are just preloading
				if(!t.loadedViews[view].isInited) {
					t.loadedViews[view].init();
				}
				if(!t.loadedViews[view].isLoaded()) {
					t.loadedViews[view].update(function(r) {}, true);
				}
			}).fail(function( jqxhr, settings, exception ) {
				alert("There was error with the app. Please close and reopen it.")
			    debugger;
			});
		}
	},
	showView: function(view) {
		this.nextView = null;
		if(this.loadedViews[view] != null) {
			if(this.loadedViews[view] == this.activeView[this.loadedViews[view].viewLevel]) {
				return;
			}
			this.nextView = this.loadedViews[view];
			this.onViewAdded();
		}
		else {
			showLoading();
			var script = window.location.protocol + "//" + window.location.host + "/static/js/mobile/" + view + ".js";
			var t = this;
			$.getScript(script, function(){
				if(t.nextView == null) {
					t.showView(view);
				}
			}).fail(function( jqxhr, settings, exception ) {
				hideLoading();
				alert("There was error with the app. Please close and reopen it.")
			    debugger;
			});
		}
	},
	onViewAdded: function() {
		if(!this.nextView.isInited) {
			this.nextView.init();
		}
		if(this.nextView.isLoaded()) {
			this.onViewFinishedLoading();
		}
		else {
			showLoading();
			var t = this;
			this.nextView.update(function(r) {
				if(r) {
					t.onViewFinishedLoading();
				}
			}, true);
		}
	},
	onViewFinishedLoading: function() {
		var t = this;
		hideLoading();
		var level = this.nextView.viewLevel;
		if(this.activeView[level] != null) {
			this.activeView[level].save();
			this.activeView[level].hide();
			this.activeView[level] = null;
		}
		window.scrollTo(0,0);
		this.activeView[level] = this.nextView;
		this.activeView[level].show();
		this.openViews[this.activeView[level].name] = this.activeView[level];
		this.nextView = null;
	},
};

function mergeObjects(objBase, objSub) {
	var b = {};
	for(var attr in objBase) {
		b[attr] = objBase[attr];
	}
	for(var attr in objSub) {
		b[attr] = objSub[attr];
	}
	return b;
};

function saveObject(obj, name) {
	var savedValues = {};
	for(var attr in obj) {
		if(typeof(obj[attr]) != 'function' && attr != 'element' && attr != 'events') {
			savedValues[attr] = obj[attr];
		}
	}
	localStorage['modelStorage.' + name] = JSON.stringify(savedValues);
}

function loadObject(obj, name) {
	/*var savedValues = localStorage['modelStorage.' + name];
	if(savedValues) {
		savedValues = $.parseJSON(savedValues);
		for(var attr in savedValues) {
			obj[attr] = savedValues[attr];
		}
	}*/
}

function showLoading() {
	$('#loading-overlay').addClass('active');
}
function hideLoading() {
	$('#loading-overlay').removeClass('active');
}
function showOverlay() {
	$('#overlay').addClass('active');
}
function hideOverlay() {
	$('#overlay').removeClass('active');
}

function showMessage(msg, level) {
	var el = $('#message-header');
	if(el.hasClass('active') && level != 'error') {
		return;
	}
	$('#message-header .message').html(msg);
	el.removeClass('info').removeClass('warning').removeClass('error');
	el.addClass(level);
	if(!el.hasClass('active')) {
		el.addClass('active');
		el.on('click', function() {
			el.off('click');
			el.removeClass('active');
		})
		setTimeout(function() {
			el.off('click');
			el.removeClass('active');
		}, 3000); // 3 seconds
	}
}
function showError(msg) {
	showMessage(msg, 'error');
}
function showWarning(msg) {
	showMessage(msg, 'warning');
}
function showInfo(msg) {
	showMessage(msg, 'info');
}

var Model = {
	name: 'BaseModel',
	isInited: false,
	link: '/client/climbers',
	data: null,
	lastSync: 0,

	initBase: function() {
		this.isInited = true;
		loadObject(this, this.name);
	},
	isLoaded: function() {
		return this.data != null;
	},
	save: function() {
		saveObject(this, this.name);
	},
	update: function(callback, force) {
		if(this.isLoaded() && !force) {
			return;
		}

		var t = this;
		$.get(this.link, function(result) {
			if(result && result.status == 'success') {
				t.data = result.data;
				if(callback){ callback(true); }
			}
			else {
				alert("Oh No! There was a problem with the app. Please close the app and reopen it.");
				if(callback){ callback(false); }
			}
		}).fail(function( jqxhr, settings, exception ) {
			hideLoading();
			alert("There was error with the app. Please close and reopen it.")
		    debugger;
		});
	},
	submit: function(callback) {

	}
};

var View = {
	name: 'BaseView',
	isInited: false,
	link: 'mobile/page/welcome',
	element: null,
	source: null,
	lastSync: 0,
	events: [],
	viewLevel: 3,

	initBase: function() {
		this.isInited = true;
		loadObject(this, this.name);
	},
	update: function(callback, force) {
		if(this.isLoaded() && !force) {
			return;
		}

		var t = this;
		$.get(window.location.protocol + "//" + window.location.host + "/" + this.link, function(result) {
			if(result) {
				t.source = result;
				if(callback){ callback(true); }
			}
			else {
				alert("Oh No! There was a problem with the app. Please close the app and reopen it.");
				if(callback){ callback(false); }
			}
		}).fail(function( jqxhr, settings, exception ) {
			hideLoading();
			alert("There was error with the app. Please close and reopen it.")
		    debugger;
		});
	},
	isLoaded: function() {
		return this.source != null;
	},
	isActive: function() {
		return this.element != null && this.element.hasClass('active');
	},
	show: function(callback) {
		var t = this;
		if(this.element == null) {
			this.element = $(this.source).appendTo($('#content-wrapper'));
		}
		setTimeout(function() { 
			t.onPreShow();
			t.element.addClass('active'); 
			setTimeout(function() {
				t.createBindings();
				t.onShow();
				if(callback) {
					callback();
				}
			}, 500);
		}, 1);
	},
	hide: function(callback) {
		this.removeBindings();
		this.element.removeClass('active');
		this.save();
		var t = this;
		t.onHide();
		setTimeout(function() {
			t.element.remove();
			t.element = null;
			if(callback) {
				callback();
			}
		}, 500);
	},
	save: function() {
		saveObject(this, this.name);
	},

	createBindings: function() {
		var ev = this.events;
		var t = this;
		if($.type(ev) == 'function') {
			ev = ev();
		}

		for(var i in ev) {
			var el = this.element.find(ev[i].selector);
			if(el.length == 0) {
				el = $(ev[i].selector);
			}
			if(el.length > 0) {
				var handler = ev[i].handler;
				el.on(ev[i].event + "." + this.name, t[handler] );
			}
		}
		t.onCreateBindings();
	},
	removeBindings: function() {
		var ev = this.events;
		if($.type(ev) == 'function') {
			ev = ev();
		}

		for(var i in ev) {
			var el = this.element.find(ev[i].selector);
			if(el.length == 0) {
				el = $(ev[i].selector);
			}
			if(el.length > 0) {
				el.off(ev[i].event + "." + this.name);
			}
		}
		this.onRemoveBindings();
	},

	// functions to be overridden
	onPreShow: function() {

	},
	onShow: function() {

	},
	onHide: function() {

	},
	onCreateBindings: function() {

	},
	onRemoveBindings: function() {

	},
};

function timeUntil(time) {
	var s = '';
	var timeLeft = time - new Date().getTime();
	var seconds = Math.floor(timeLeft / 1000);
	var minutes = Math.floor(seconds / 60);
	var hours = Math.floor(minutes / 60);
	var days = Math.floor(hours / 24);
	seconds -= minutes * 60;
	minutes -= hours * 60;
	hours -= days * 24;
	if(days >= 100) {
		s += days + ' Days';
	}
	else if(days > 0) {
		s += days + ' Day' + (days == 1 ? ' ' : 's ') + hours + ' Hour' + (hours == 1 ? '' : 's');
	}
	else if(hours > 0) {
		s += hours + ' Hour' + (hours == 1 ? ' ' : 's ') + minutes + ' Minute' + (minutes == 1 ? '' : 's');
	}
	else {
		s += minutes + ' Minute' + (minutes == 1 ? ' ' : 's ') + seconds + ' Second' + (seconds == 1 ? ' ' : 's ');
	}
	return s;
};
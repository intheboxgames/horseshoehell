


var RegisterView = {
	first_name: '',
	last_name: '',
	birth_date: '',
	category: '',
	event_type: '',
	event_start: '',
	event_time: '',
	link: '/mobile/register',

	init: function() {
		this.name = 'RegisterView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$('#register-submit').on('click.register', function(){ t.onSubmit(); });
		$('#register-back').on('click.register', function(){ t.onBack(); });
		$(document).on('keypress.register', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$('#register-submit').off('click.register');
		$('#register-back').off('click.register');
		$(document).off('keypress.register');
	},

	onBack: function() {
		window.application.showView(WelcomeView);
	},
	onSubmit: function() {
		/*this.first_name = $('#register-wrapper #first_name').val();
		this.last_name = $('#register-wrapper #last_name').val();
		this.birth_date = $('#register-wrapper #birth_date').val();
		this.category = $('#register-wrapper #category').val();
		this.event_type = $('#register-wrapper #event_type').val();
		this.event_start = $('#register-wrapper #event_start').val();
		this.event_time = $('#register-wrapper #event_time').val();

		if(this.first_name == '' || this.last_name == '') {
			alert('Please enter both a first and last name.');
			return;
		}
		if(this.birth_date == '') {
			alert('Please enter your birthday. This is used to separate climbers that have the same or similar names.');
			return;
		}
		if(this.event_start == '') {
			alert('Please enter a start date for your practice run.');
			return;
		}
		if(this.event_time == '') {
			alert('Please enter a start time for your practice run.');
			return;
		}*/
		
		window.application.showView(CreateEventView);
	}
};
RegisterView = createClass(BaseView, RegisterView);



var CreateEventView = {
	link: '/mobile/create_event',

	init: function() {
		this.name = 'CreateEventView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.create_event', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.create_event');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(GoalsSettingsView);
	}
};
CreateEventView = createClass(BaseView, CreateEventView);



var GoalsSettingsView = {
	link: '/mobile/goals_settings',

	init: function() {
		this.name = 'GoalsSettingsView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.goals_settings', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.goals_settings');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(RoutesSettingsView);
	}
};
GoalsSettingsView = createClass(BaseView, GoalsSettingsView);



var RoutesSettingsView = {
	link: '/mobile/routes_settings',

	init: function() {
		this.name = 'RoutesSettingsView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.routes_settings', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.routes_settings');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(CountdownView);
	}
};
RoutesSettingsView = createClass(BaseView, RoutesSettingsView);



var CountdownView = {
	link: '/mobile/countdown',

	init: function() {
		this.name = 'CountdownView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.countdown', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.countdown');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(RoutesView);
	}
};
CountdownView = createClass(BaseView, CountdownView);



var RoutesView = {
	link: '/mobile/routes',

	init: function() {
		this.name = 'RoutesView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.routes', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.routes');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(ProgressView);
	}
};
RoutesView = createClass(BaseView, RoutesView);



var ProgressView = {
	link: '/mobile/progress',

	init: function() {
		this.name = 'ProgressView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.progress', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.progress');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(GoalsView);
	}
};
ProgressView = createClass(BaseView, ProgressView);



var GoalsView = {
	link: '/mobile/goals',

	init: function() {
		this.name = 'GoalsView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.goals', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.goals');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(ClimberSearchView);
	}
};
GoalsView = createClass(BaseView, GoalsView);



var ClimberSearchView = {
	link: '/mobile/climber_search',

	init: function() {
		this.name = 'ClimberSearchView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.climber_search', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.climber_search');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(SearchResultsView);
	}
};
ClimberSearchView = createClass(BaseView, ClimberSearchView);



var SearchResultsView = {
	link: '/mobile/search_results',

	init: function() {
		this.name = 'SearchResultsView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.search_results', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.search_results');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(CompareView);
	}
};
SearchResultsView = createClass(BaseView, SearchResultsView);



var CompareView = {
	link: '/mobile/compare',

	init: function() {
		this.name = 'CompareView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.compare', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.compare');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(SettingsView);
	}
};
CompareView = createClass(BaseView, CompareView);



var SettingsView = {
	link: '/mobile/settings',

	init: function() {
		this.name = 'SettingsView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.settings', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.settings');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(AppSettingsView);
	}
};
SettingsView = createClass(BaseView, SettingsView);



var AppSettingsView = {
	link: '/mobile/app_settings',

	init: function() {
		this.name = 'AppSettingsView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.app_settings', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.app_settings');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(EventEndView);
	}
};
AppSettingsView = createClass(BaseView, AppSettingsView);



var EventEndView = {
	link: '/mobile/event_end',

	init: function() {
		this.name = 'EventEndView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.event_end', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.event_end');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(ScoreSubmissionView);
	}
};
EventEndView = createClass(BaseView, EventEndView);



var ScoreSubmissionView = {
	link: '/mobile/submission',

	init: function() {
		this.name = 'ScoreSubmissionView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.submission', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.submission');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(PostEventView);
	}
};
ScoreSubmissionView = createClass(BaseView, ScoreSubmissionView);



var PostEventView = {
	link: '/mobile/post_event',

	init: function() {
		this.name = 'PostEventView';
		this.initBase();
	},

	// Methods to be overloaded
	createBindings: function() {
		var t = this;
		$(document).on('keypress.post_event', function(e){ if(e.which == 13) { t.onSubmit(); } } );
	},
	removeBindings: function() {
		var t = this;
		$(window).off('keypress.post_event');
	},

	onSubmit: function() {
		var t = this;
		window.application.showView(WelcomeView);
	}
};
PostEventView = createClass(BaseView, PostEventView);



$(document).ready(function() {
	window.application.init();
});